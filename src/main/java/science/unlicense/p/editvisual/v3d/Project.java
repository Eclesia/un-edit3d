
package science.unlicense.p.editvisual.v3d;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.phase.PhaseSequence;
import science.unlicense.engine.opengl.phase.effect.DirectPhase;
import science.unlicense.engine.opengl.technique.GLBlinnPhongTechnique;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.p.editvisual.v3d.io.XMLSceneReader;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class Project extends AbstractEventSource{

    public static final Chars PROPERTY_ACTIVEOBJECT = new Chars("ActiveObject");
    public static final Chars PROPERTY_PROJECTPATH = new Chars("projectpath");


    private static final NodeVisitor DISABLE_LIGHT = new NodeVisitor() {

        public Object visit(Node node, Object context) {
            if(node instanceof Model){
                final Sequence techniques = ((Model) node).getTechniques();
                for(int i=0;i<techniques.getSize();i++){
                    if(techniques.get(i) instanceof GLBlinnPhongTechnique){
                        //((GLBlinnPhongTechnique)techniques.get(i)).setUseLights(false);
                        ((GLBlinnPhongTechnique)techniques.get(i)).getState().setEnable(GLC.GETSET.State.BLEND, false);
                    }
                }
            }
            return super.visit(node, context);
        }
    };

    private static final NodeVisitor BUILD_STRUCTURE = new NodeVisitor() {

        public Object visit(Node node, Object context) {
            if(node instanceof MotionModel){
//                final Skeleton skeleton = ((MultipartMesh)node).getSkeleton();
//                final Node n = new DefaultNamedNode(new Chars("Skeleton"), node, true);
//                if(skeleton!=null){
//
//                    //joint nodes hierarchy
//                    final Node joints = new DefaultNamedNode(new Chars("Joints"), node, true);
//                    final Iterator itejoints = skeleton.children().createIterator();
//                    while(itejoints.hasNext()){
//                        final Joint jt = (Joint) itejoints.next();
//                        jt.accept(BUILD_SETSKELETIONPROP, skeleton);
//                        joints.children().add(jt);
//                    }
//                    n.children().add(joints);
//
//                    //inverse kinematics
//                    final Node iks = new DefaultNamedNode(new Chars("IKs"), node, true);
//                    final Iterator iteIks = skeleton.getIks().createIterator();
//                    while(iteIks.hasNext()){
//                        final InverseKinematic ik = (InverseKinematic) iteIks.next();
//                        final Node nik = new DefaultNamedNode(new Chars("IK"), ik, true);
//                        for(Joint jt : ik.getChain()){
//                            nik.children().add(jt);
//                        }
//                        final Joint effector = ik.getEffector();
//                        final Joint target = ik.getTarget();
//                        nik.children().add(effector);
//                        nik.children().add(target);
//                        iks.children().add(nik);
//                    }
//                    n.children().add(iks);
//                }
//                node.children().add(0, n);
            }else if(node instanceof Model){
//                ((Mesh)node).getMaterial().setCellShading(5);
//                ((Mesh)node).getRenderers().add(new SilhouetteRenderer((Mesh)node, Color.WHITE, 0.003f));
//                ((Mesh)node).getRenderers().add(new DebugFaceNormalRenderer());
            }
            return super.visit(node, context);
        }
    };

//    private static final NodeVisitor BUILD_PICKABLE = new DefaultNodeVisitor() {
//
//        public Object visit(Node node, Object context) {
//            if(node instanceof Mesh){
//                final Mesh mesh = (Mesh) node;
//                final PickResetPhase phase = Platform.get().getPickPhase();
//                mesh.getExtShaderActors().add(new PickActor(phase, mesh));
//            }
//            return super.visit(node, context);
//        }
//    };


    private final PhaseSequence graphicPhases = new PhaseSequence();
    private final PhaseSequence aggregationPhases = new PhaseSequence();
    private final Sequence graphicLayers = new ArraySequence();

    private GLProcessContext context;
    private MonoCamera camera;
    private final GraphicNode root = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private Object activeObject = null;
    private Path projectPath = null;

    public GLProcessContext getContext() {
        return context;
    }

    void setContext(GLProcessContext context) {
        this.context = context;
        context.getPhases().add(graphicPhases);
        context.getPhases().add(aggregationPhases);
        root.setId(new Chars("root"));
    }

    public MonoCamera getCamera() {
        return camera;
    }

    void setCamera(MonoCamera camera) {
        this.camera = camera;
    }

    public GraphicNode getRoot() {
        return root;
    }

    public Object getActiveObject() {
        return activeObject;
    }

    public void setActiveObject(Object activeNode) {
        if(CObjects.equals(this.activeObject,activeNode)) return;
        Object old = this.activeObject;
        this.activeObject = activeNode;
        getEventManager().sendPropertyEvent(this, PROPERTY_ACTIVEOBJECT, old, activeNode);
    }

    public Property varActiveObject(){
        return getProperty(PROPERTY_ACTIVEOBJECT);
    }

    public Sequence getGraphicLayers(){
        return new ArraySequence(graphicLayers);
    }

    public void addGraphicLayer(Layer layer, int index){
        graphicLayers.add(index,layer);

        final DirectPhase aggregatePhase = new DirectPhase(layer.result);
        graphicPhases.getPhases().add(layer.phase);
        aggregationPhases.getPhases().add(aggregatePhase);
    }

    public void removeGraphicLayer(Layer layer){

    }

    public Path getProjectPath() {
        return projectPath;
    }

    private void setProjectPath(Path newPath){
        if(CObjects.equals(projectPath, newPath)){
            return;
        }
        final Path old = projectPath;
        projectPath = newPath;
        getEventManager().sendPropertyEvent(this, PROPERTY_PROJECTPATH, old, newPath);
    }

    public void newScene(final Path file){
        getRoot().getChildren().removeAll();
        setProjectPath(file);
    }

    public void openScene(final Path file){
        try{
            final XMLSceneReader reader = new XMLSceneReader();
            reader.setInput(file);
            final GraphicNode node = reader.read();
            prepareNode(node);
            getRoot().getChildren().removeAll();
            getRoot().getChildren().add(node);
            setProjectPath(file);
        }catch(IOException ex){
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }catch(StoreException ex){
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }
    }

    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

    public static class Layer{

        public Phase phase;
        public Texture2D result;

        public Layer(Phase phase, Texture2D result) {
            this.phase = phase;
            this.result = result;
        }

    }

    public static void prepareNode(Node candidate){
        BUILD_STRUCTURE.visit(candidate, null);
        DISABLE_LIGHT.visit(candidate, null);
        //candidate.accept(BUILD_PICKABLE, null);
    }
}
