

package science.unlicense.p.editvisual.v3d.subframe.skeleton;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.Phase;
import science.unlicense.engine.opengl.phase.PhaseSequence;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.impl.opengl.GLC;
import science.unlicense.gpu.impl.opengl.resource.FBO;
import science.unlicense.gpu.impl.opengl.resource.Texture2D;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.p.editvisual.v3d.Loadable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.physics.api.skeleton.Joint;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonDebugLayer implements Loadable{

    public static final class DebugState{
        public final MotionModel mesh;
        public final SceneNode debugNode;

        private boolean skeletonVisible = true;
        private boolean physicVisible = true;
        private boolean constraintVisible = true;

        public DebugState(MotionModel mesh, float size) {
            this.mesh = mesh;
            this.debugNode = SkeletonUtils.viewSkeleton(this,size);
        }

        public boolean isSkeletonVisible() {
            return skeletonVisible;
        }

        public void setSkeletonVisible(boolean state){
            SkeletonUtils.SHOW_JOINT.visit(debugNode, state);
            skeletonVisible = state;
        }

        public boolean isPhysicVisible() {
            return physicVisible;
        }

        public void setPhysicVisible(boolean state){
            SkeletonUtils.SHOW_PHYSIC.visit(debugNode, state);
            physicVisible = state;
        }

        public void setConstraintsVisible(boolean state){
            //TODO
//            debugNode.accept(SkeletonUtils.SHOW_PHYSIC, state);
//            viewPhysics = state;
        }

        public boolean isConstraintVisible() {
            return constraintVisible;
        }

    }


    public static final SkeletonDebugLayer INSTANCE = new SkeletonDebugLayer();

    //debug view layer
    private Phase debugPhase;
    private PickResetPhase pickingPhase;
    private Texture2D debugTexture;
    private SceneNode debugScene;

    //list of current meshes in debug view
    private final Dictionary debugs = new HashDictionary();

    //selected joint
    private Joint selectedJoint;
    //node in debug view
    private SceneNode debugSelectedJoint;

    private SkeletonDebugLayer(){
    }

    private void init(Platform platform){
        // Insert the layer in the application
        debugScene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);

        final Project project = platform.getProject();
        final GLProcessContext context = project.getContext();
        final int width = (int) context.getViewRectangle().getWidth();
        final int height = (int) context.getViewRectangle().getHeight();
        //configure the texture we will generate
        final Texture2D diffuse       = new Texture2D(width, height, Texture2D.COLOR_RGBA_CLAMPED());
        final Texture2D pickingMesh   = new Texture2D(width, height, Texture2D.VEC1_INT());
        final Texture2D pickingVert   = new Texture2D(width, height, Texture2D.VEC1_INT());
        final Texture2D depth         = new Texture2D(width, height, Texture2D.DEPTH_24());
        //configure FBO with our textures
        final FBO deferredFbo = new FBO(width, height);
        deferredFbo.addAttachment(null, GLC.FBO.Attachment.COLOR_0, diffuse);
        deferredFbo.addAttachment(null, GLC.FBO.Attachment.COLOR_1, pickingMesh);
        deferredFbo.addAttachment(null, GLC.FBO.Attachment.COLOR_2, pickingVert);
        deferredFbo.addAttachment(null, GLC.FBO.Attachment.DEPTH, depth);

//        final RenderPhase renderPhase = new RenderPhase(
//                debugScene,
//                project.getCamera(),
//                deferredFbo, new FragmentCollector[]{
//                    DeferredRenderPhase.OUT_MATERIAL_COLOR,
//                    PickActor.COLLECT_MESH_ID,
//                    PickActor.COLLECT_VERTEX_ID
//        });

        pickingPhase = new PickResetPhase(pickingMesh,null);

        final PhaseSequence phases = new PhaseSequence();
        phases.getPhases().add(new ClearPhase());
        phases.getPhases().add(new ClearPhase(deferredFbo));
        phases.getPhases().add(new UpdatePhase(debugScene));
//        phases.getPhases().add(renderPhase);
        phases.getPhases().add(pickingPhase);

        debugTexture = diffuse;
        debugPhase = phases;

        //add layer in the stack
        project.addGraphicLayer(new Project.Layer(debugPhase,debugTexture),
                project.getGraphicLayers().getSize());
    }

    public PickResetPhase getPickingPhase() {
        return pickingPhase;
    }

    public SceneNode getDebugScene() {
        return debugScene;
    }

    public DebugState getState(MotionModel mesh){
        return (DebugState) debugs.getValue(mesh);
    }

    public void putState(MotionModel mesh, DebugState state){
        debugs.add(mesh, state);
    }


    public Joint getSelectedJoint() {
        return selectedJoint;
    }

    public void setSelectedJoint(final Joint selectedJoint) {
        if(CObjects.equals(this.selectedJoint, selectedJoint)) return;

        //remove old arcs
        if(this.selectedJoint != null){
            if(debugSelectedJoint !=null){
                debugSelectedJoint.getChildren().removeAll();
            }
        }

        this.selectedJoint = selectedJoint;

        if(selectedJoint!=null){

//            final NodeVisitor search = new Nodes.SearchVisitor(new Predicate() {
//                public Boolean evaluate(Object candidate) {
//                    if(!(candidate instanceof Mesh)) return false;
//                    if(!((Mesh)candidate).isPickable()) return false;
//                    final Joint jt = (Joint) ((Mesh)candidate).getProperties().getValue(PROPERTY_JOINT);
//                    return selectedJoint == jt;
//                }
//            });
//            GLNode selection = (GLNode) SkeletonDebugLayer.INSTANCE.getDebugScene().accept(search, null);

//            if(selection != null){
//                //add rotation and translation objects
//                final Mesh grab = createSphere(Color.WHITE, 0.3f);
//                final Mesh arc0 = createArc(Color.RED, 1f, 0.05f, 0);
//                final Mesh arc1 = createArc(Color.GREEN, 1f, 0.05f, 1);
//                final Mesh arc2 = createArc(Color.BLUE, 1f, 0.05f, 2);
//                final Mesh trs0 = SkeletonUtils.createAxe(Color.RED, 1f, 0.025f, 0);
//                final Mesh trs1 = SkeletonUtils.createAxe(Color.GREEN, 1f, 0.025f, 1);
//                final Mesh trs2 = SkeletonUtils.createAxe(Color.BLUE, 1f, 0.025f, 2);
//                final PickResetPhase pickingPhase = SkeletonDebugLayer.INSTANCE.getPickingPhase();
////                grab.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                arc0.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                arc1.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                arc2.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                trs0.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                trs1.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
////                trs2.accept(SkeletonEditor.SET_DEBUG_ACTOR,pickingPhase);
//                selection.children().add(grab);
//                selection.children().add(arc0);
//                selection.children().add(arc1);
//                selection.children().add(arc2);
//                selection.children().add(trs0);
//                selection.children().add(trs1);
//                selection.children().add(trs2);
//                grab.setName(RotateEditHandler.GRAB);
//                arc0.setName(RotateEditHandler.ARC0);
//                arc1.setName(RotateEditHandler.ARC1);
//                arc2.setName(RotateEditHandler.ARC2);
//                trs0.setName(RotateEditHandler.TRS0);
//                trs1.setName(RotateEditHandler.TRS1);
//                trs2.setName(RotateEditHandler.TRS2);
//                debugSelectedJoint = selection;
//            }
        }
    }

    public void load(final Platform platform) {
        init(platform);
        platform.getProject().varActiveObject().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                final Object activeObject = platform.getProject().getActiveObject();
                if(activeObject instanceof Joint){
                    setSelectedJoint((Joint)activeObject);
                }else{
                    setSelectedJoint(null);
                }
            }
        });
    }

}
