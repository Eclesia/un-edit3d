
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class MeshPresenter implements ObjectPresenter{

    @Override
    public Widget createWidget(Object candidate) {
        if (candidate instanceof Model) {
            final Model model = (Model) candidate;
            final WLabel lbl = new WLabel(model.getTitle());
            return lbl;
        }
        return null;
    }

}
