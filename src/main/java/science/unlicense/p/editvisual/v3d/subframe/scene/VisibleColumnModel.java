
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class VisibleColumnModel extends DefaultColumn{

    public VisibleColumnModel() {
        super(Chars.EMPTY,new VisiblePresenter());
    }

    private static class VisiblePresenter implements ObjectPresenter{

        public Widget createWidget(final Object candidate) {
            if(candidate instanceof GraphicNode){
                final WButton button = new WButton();
                button.setText(new Chars(((GraphicNode)candidate).isVisible() ? "H":"V"));
                button.addEventListener(ActionMessage.PREDICATE, new EventListener() {
                    public void receiveEvent(Event event) {
                        final GraphicNode node = (GraphicNode)candidate;
                        node.setVisible(!node.isVisible());
                        button.setText(new Chars(((GraphicNode)candidate).isVisible() ? "H":"V"));
                    }
                });
                return button;
            }else{
                return new WSpace(new Extent.Double(2));
            }
        }

    }

}
