

package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.OrderedSet;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Tuple;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.p.editvisual.v3d.Platform;

/**
 *
 * @author Johann Sorel
 */
public class WTimeSerieCell extends WLeaf {

    private final WTimeSerieHeader header;
    private final JointTimeSerie timeSerie;

    //keep mouse position, for add frame popup action
    private Tuple mousePosition;

    public WTimeSerieCell(final WTimeSerieHeader header, final JointTimeSerie timeSerie) {
        this.header = header;
        this.timeSerie = timeSerie;

        header.addEventListener(new PropertyPredicate(WTimeSerieHeader.PROPERTY_MARKERS), new EventListener() {
            public void receiveEvent(Event event) {
                WTimeSerieCell.this.setDirty();
            }
        });

        final WContainer popup = new WContainer();
        popup.setLayout(new GridLayout(1, 1));

        final WButton button = new WButton(new Chars("Add frame"));

        button.addEventListener(ActionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final double t = WTimeSerieCell.this.header.getAxis().toModel(mousePosition.get(0));
                final JointKeyFrame jf = new JointKeyFrame(WTimeSerieCell.this.timeSerie.getJoint(), (long)t);
                WTimeSerieCell.this.timeSerie.getFrames().add(jf);
                WTimeSerieCell.this.setDirty();
            }
        });
        popup.getChildren().add(button);

        setPopup(popup);

        setView(new WidgetView(this){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {

                //render markers
                final double height = getEffectiveExtent().get(1);
                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                painter.setPaint(new ColorPaint(Color.GRAY_DARK));
                final double[] markers = header.getMarkers();
                for(int i=0,n=markers.length;i<n;i++){
                    final double vx = markers[i];
                    painter.stroke(new DefaultLine(vx, 0, vx, height));
                }

                if(timeSerie==null) return;

                //render frames
                final Rectangle c = new Rectangle(0, 0, 5, 5);
                c.setY( (innerBBox.getSpan(1) / 2) -2.5 );

                final ColorPaint base = new ColorPaint(Color.GRAY_LIGHT);
                final ColorPaint selected = new ColorPaint(Color.RED);
                final ColorPaint stroke = new ColorPaint(Color.GRAY_DARK);

                final Object active = Platform.get().getProject().getActiveObject();
                final double endX = innerBBox.getMax(0);

                final OrderedSet frames = timeSerie.getFrames();
                final Iterator ite = frames.createIterator();
                final TimeAxis axis = header.getAxis();
                while (ite.hasNext()) {
                    final JointKeyFrame f = (JointKeyFrame) ite.next();
                    final double time = f.getTime();
                    c.setX(axis.toView(time));
                    if (c.getX() < 0) {
                        continue;
                    }

                    //special style for selection element
                    if(CObjects.equals(f, active)){
                        painter.setPaint(selected);
                    }else{
                        painter.setPaint(base);
                    }
                    painter.fill(c);
                    painter.setPaint(stroke);
                    painter.stroke(c);

                    if (c.getX() > endX) {
                        break;
                    }
                }
            }
        });

    }

    @Override
    public void receiveEventParent(Event event) {

        if(timeSerie!=null){
            final EventMessage message = event.getMessage();
            if(message instanceof MouseMessage){
                final MouseMessage me = ((MouseMessage)message);
                mousePosition = me.getMousePosition().copy();

                final int type = me.getType();
                final int button = me.getButton();
                if(type==MouseMessage.TYPE_TYPED){
                    if(button == MouseMessage.BUTTON_1){
                        //frame selection
                        final double t = header.getAxis().toModel(mousePosition.get(0));
                        final double tolerance = (1.0/header.getAxis().getScale()) * 4;
                        final double min = t-tolerance;
                        final double max = t+tolerance;
                        final OrderedSet frames = timeSerie.getFrames();
                        final Iterator ite = frames.createIterator();
                        while (ite.hasNext()) {
                            final JointKeyFrame f = (JointKeyFrame) ite.next();
                            final double time = f.getTime();
                            if(time>=min && time<=max){
                                Platform.get().getProject().setActiveObject(f);
                                me.consume();
                                break;
                            }
                        }
                    }
                }
            }
            //catch key arrows to move to next or previous joint frame
            else if(message instanceof KeyMessage){
                final KeyMessage ke = (KeyMessage) message;

                final Object active = Platform.get().getProject().getActiveObject();
                if(active instanceof JointKeyFrame){

                    JointKeyFrame previous = null;
                    final Iterator ite = timeSerie.getFrames().createIterator();

                    if(ke.getCode() == KeyMessage.KC_LEFT){
                        while (ite.hasNext()) {
                            final JointKeyFrame f = (JointKeyFrame) ite.next();
                            if(CObjects.equals(f, active)){
                                if(previous!=null){
                                    //activate previous frame
                                    Platform.get().getProject().setActiveObject(previous);
                                }
                                break;
                            }
                            previous = f;
                        }
                        ke.consume();
                    }else if(ke.getCode() == KeyMessage.KC_RIGHT){
                        while (ite.hasNext()) {
                            final JointKeyFrame f = (JointKeyFrame) ite.next();
                            if(CObjects.equals(previous, active)){
                                if(f!=null){
                                    //activate previous frame
                                    Platform.get().getProject().setActiveObject(f);
                                }
                                break;
                            }
                            previous = f;
                        }
                        ke.consume();
                    }
                }
            }
        }

        super.receiveEventParent(event);
    }


}
