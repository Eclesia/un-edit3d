
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.p.editgraphic.editors.WSimilarityEditor;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class PositionableEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/transform-move.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //properties common to all scene Nodes
    private final WSimilarityEditor editTransform = new WSimilarityEditor();

    private SceneNode edited;

    public PositionableEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);
        addChild(editTransform, FillConstraint.builder().coord(0,0).build());
    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof SceneNode)) return null;
        edited = (SceneNode) candidate;
        editTransform.setSimilarity(edited.getNodeTransform());
        return this;
    }

    public void uninstall(Object candidate) {

    }

}
