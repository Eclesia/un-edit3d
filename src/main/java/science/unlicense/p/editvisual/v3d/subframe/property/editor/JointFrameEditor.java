
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WTupleField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.p.editgraphic.editors.WRotationField;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JointFrameEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/player-time-2.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //properties common to all GL Nodes
    private final WLabel lblName = new WLabel(new Chars("Joint name"));
    private final WLabel lblTime = new WLabel(new Chars("Time"));
    private final WLabel editName = new WLabel();
    private final WSpinner editTime = new WSpinner(new NumberSpinnerModel(Double.class,0,null,null,1));
    private final WTupleField editTranslation = new WTupleField();
    private final WRotationField editQuaternion = new WRotationField();

    private JointKeyFrame edited;

    public JointFrameEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        addChild(lblName,           FillConstraint.builder().coord(0,0).span(1,1).build());
        addChild(editName,          FillConstraint.builder().coord(1,0).span(1,1).build());
        addChild(lblTime,           FillConstraint.builder().coord(0,1).span(1,1).build());
        addChild(editTime,          FillConstraint.builder().coord(1,1).span(1,1).build());
        addChild(editTranslation,   FillConstraint.builder().coord(0,2).span(2,1).build());
        addChild(editQuaternion,    FillConstraint.builder().coord(0,3).span(2,1).build());

        editTime.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
                edited.setTime(((Number)editTime.getValue()).doubleValue());
            }
        });

    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof JointKeyFrame)) return null;

//        edited = (JointKeyFrame) candidate;
//        if(edited.getJoint()!=null){
//            editName.setText(edited.getJoint().getName());
//        }
//        editTime.setValue(edited.getTime());
//        editTranslation.setTuple(edited.getPose().getTransform().getTranslation());
//        editQuaternion.setValue(edited.getPose().getTransform().getRotation());

        return this;
    }

    public void uninstall(Object candidate) {

    }

}
