

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.p.editvisual.v3d.MeshRootNode;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;

/**
 *
 * @author Johann Sorel
 */
public class AddMeshAction extends AbstractActionExecutable {

    private static final Chars LAST_MESH_PATH = new Chars("lastMeshPath");

    private final SceneNode parent;

    public AddMeshAction() {
        this(null);
    }

    public AddMeshAction(SceneNode parent) {
        this.parent = parent;
        setText(new Chars("mesh"));
    }

    public Object perform() {
        final Project project = Platform.get().getProject();
        new Thread(){
            @Override
            public void run() {

                final Project project = Platform.get().getProject();
                final Object active = (parent==null) ? project.getActiveObject() : parent;
                if(active instanceof SceneNode){
                    final WPathChooser chooser = new WPathChooser();
                    final Chars val = Platform.get().getConfiguration().getProperty(LAST_MESH_PATH);
                    if(val != null){
                        final Path[] selected = new Path[]{Paths.resolve(val)};
                        try {
                            if(selected[0].exists()){
                                chooser.setViewRoot(selected[0].getParent());
                                chooser.setSelectedPath(selected);
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }

                    final Path p = chooser.showOpenDialog((UIFrame)null);
                    if(p!=null){
                        Platform.get().getConfiguration().setProperty(LAST_MESH_PATH,new Chars(p.toURI()));
                        try{
                            Platform.get().getConfiguration().save();
                            if(Model3Ds.canDecode(p)){
                                final Store store = Model3Ds.open(p);
                                final MeshRootNode root = new MeshRootNode(p);
                                final Iterator ite = ((SceneResource)store).getElements().createIterator();
                                while(ite.hasNext()){
                                    final Object obj = ite.next();
                                    if(obj instanceof SceneNode){
                                        Project.prepareNode((Node)obj);
                                        root.getChildren().add((SceneNode)obj);
                                    }
                                }
                                ((SceneNode)active).getChildren().add(root);
                            }
                        }catch(IOException ex){
                            Loggers.get().log(ex, Logger.LEVEL_WARNING);
                        }catch(StoreException ex){
                            Loggers.get().log(ex, Logger.LEVEL_WARNING);
                        }
                    }
                    System.out.println(p);
                }
            }
        }.start();
        return null;
    }

}
