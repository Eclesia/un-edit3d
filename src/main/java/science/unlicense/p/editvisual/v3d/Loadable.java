

package science.unlicense.p.editvisual.v3d;


/**
 *
 * @author Johann Sorel
 */
public interface Loadable {

    void load(Platform platform);

}
