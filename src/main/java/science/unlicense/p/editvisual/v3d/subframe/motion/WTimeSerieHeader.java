

package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.font.Font;
import science.unlicense.display.api.font.FontChoice;
import science.unlicense.display.api.font.FontMetadata;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.display.api.painter2d.PlainBrush;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Tuple;
import science.unlicense.time.api.Times;

/**
 *
 * @author Johann Sorel
 */
public class WTimeSerieHeader extends WLeaf{


    public static final Chars PROPERTY_MARKERS = new Chars("markers");

    private final TimeAxis axis = new TimeAxis();
    private double[] markers = new double[0];
    private Animation animation = null;

    public WTimeSerieHeader(){
        final Extents ext = getOverrideExtents();
        ext.setBest(new Extent.Double(200, 30));
        setOverrideExtents(ext);
        axis.addEventListener(PropertyMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                updateMarkers();
            }
        });

        addEventListener(MouseMessage.PREDICATE, new TimeAxisGesture(axis));

        setView(new WidgetView(this){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                if(animation==null) return;

                final double height = getEffectiveExtent().get(1);
                final FontChoice fontChoice = new FontChoice(new Chars[]{new Chars("Tuffy")}, 10, FontChoice.WEIGHT_NONE);
                final Font font = getFrame().getPainter().getFontStore().getFont(fontChoice);
                final FontMetadata meta = font.getMetaData();

                final ColorPaint gray = new ColorPaint(Color.GRAY_DARK);
                final ColorPaint white = new ColorPaint(Color.WHITE);

                painter.setBrush(new PlainBrush(1, PlainBrush.LINECAP_BUTT));
                painter.setFont(fontChoice);
                final double[] cp = getMarkers();
                for(int i=0,n=cp.length;i<n;i++){
                    final double vx = cp[i];

                    painter.setPaint(gray);
                    painter.stroke(new DefaultLine(vx, height-10, vx, height));

                    final Chars text = Times.toCharsSeconds((int)axis.toModel(vx));
                    final BBox bbox = meta.getCharsBox(getTranslatedText(text));

                    painter.setPaint(white);
                    painter.fill(text, (float)(vx-bbox.getSpan(0)/2), (float)height-10);
                }
            }
        });

    }

    public TimeAxis getAxis() {
        return axis;
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public double[] getMarkers() {
        return markers;
    }

    private void updateMarkers(){

        //find start and end values
        final double startx = axis.toModel(0);
        final double endx = axis.toModel(getEffectiveExtent().get(0));

        //find a proper step, at least 40pixels between each bar
        int step = 100;
        while(true){
            double a = axis.toView(0);
            double b = axis.toView(step);
            if((b-a)>40) break;
            step *=2;
        }
        double x = ((int)(startx/step))*step;

        double[] newMarkers = new double[(int)Math.ceil((endx-x)/step)+2];
        //extra 2 values for text bbox
        x -= step;
        for(int i=0;i<newMarkers.length;i++,x+=step) newMarkers[i] = axis.toView(x);
        markers = newMarkers;

        getEventManager().sendPropertyEvent(this, PROPERTY_MARKERS, null, null);
        setDirty();
    }

    public static class TimeAxisGesture implements EventListener{

        private final TimeAxis axis;
        private boolean dragging = false;
        private Tuple lastMousePosition;

        public TimeAxisGesture(TimeAxis axis) {
            this.axis = axis;
        }

        @Override
        public void receiveEvent(Event event) {

            //catch mouse wheel event for scaling
            final EventMessage message = event.getMessage();
            if(message instanceof MouseMessage){
                final MouseMessage me = (MouseMessage) message;
                final Tuple currentMousePosition = me.getMousePosition();
                final int type = me.getType();
                final int button = me.getButton();
                if(type == MouseMessage.TYPE_WHEEL){
                    if(me.getWheelOffset()<0){
                        final double scale = Math.pow(0.9, -me.getWheelOffset());
                        //model.setScale(model.getScale()*scale);
                        axis.scale(scale, axis.toModel(currentMousePosition.get(0)));
                    }else if(me.getWheelOffset()>0){
                        final double scale = Math.pow(1.1, me.getWheelOffset());
                        //model.setScale(model.getScale()*scale);
                        axis.scale(scale, axis.toModel(currentMousePosition.get(0)));
                    }
                    me.consume();
                }else if(type==MouseMessage.TYPE_PRESS){
                    dragging = button == MouseMessage.BUTTON_1;
                }else if(type==MouseMessage.TYPE_RELEASE){
                    dragging = false;
                }else if(type==MouseMessage.TYPE_MOVE){
                    if(dragging){
                        final double diff = currentMousePosition.get(0)-lastMousePosition.get(0);
                        final double scale = axis.getScale();
                        final double trs = axis.getTranslation() + diff;
                        axis.setTransform(trs, scale);
                        me.consume();
                    }
                }else if(type==MouseMessage.TYPE_EXIT){
                    dragging = false;
                }

                lastMousePosition = currentMousePosition;
            }
            //catch key arrows for translation
            else if(message instanceof KeyMessage){
                final KeyMessage ke = (KeyMessage) message;
                if(ke.getCode() == KeyMessage.KC_LEFT){
                    axis.setTranslation(axis.getTranslation()-axis.getScale()*10);
                    ke.consume();
                }else if(ke.getCode() == KeyMessage.KC_RIGHT){
                    axis.setTranslation(axis.getTranslation()+axis.getScale()*10);
                    ke.consume();
                }
            }
        }

    }

}
