

package science.unlicense.p.editvisual.v3d.handler;

import science.unlicense.common.api.character.Chars;
import science.unlicense.p.editvisual.v3d.Platform;

/**
 * Is responsible to handle keyboard and mouse gestures.
 *
 * @author Johann Sorel
 */
public interface GestureHandler {

    Chars getName();

    void install(Platform platform);

    void uninstall(Platform platform);

}
