
package science.unlicense.p.editvisual.v3d.io;

import science.unlicense.binding.xml.dom.DomElement;
import science.unlicense.binding.xml.dom.DomNode;
import science.unlicense.binding.xml.dom.DomReader;
import science.unlicense.binding.xml.dom.DomUtilities;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.number.Float64;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.encoding.api.io.AbstractReader;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.impl.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class XMLSceneReader extends AbstractReader{

    private final DomReader reader = new DomReader();

    public DefaultGraphicNode read() throws IOException, StoreException{
        reader.setInput(input);
        final DomNode node = reader.read();
        return rebuild(node);
    }

    private DefaultGraphicNode rebuild(final DomNode node) throws IOException, StoreException{
        if(!(node instanceof DomElement)) return null;

        final DomElement element = (DomElement) node;

        final Chars name = element.getName().getLocalPart();

        return new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);

//        GraphicNode scenenode = null;
//        if(XMLSceneConstants.NODE.equals(name)){
//            scenenode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
//            readBaseProperties(element, scenenode);
//
//        }else if(XMLSceneConstants.MESH.equals(name)){
//            scenenode = new MeshRootNode();
//            final DomElement sourceelement = DomUtilities.getNodeForName(element, XMLSceneConstants.MESH_SOURCE);
//            if(sourceelement!=null){
//                final Path path = Paths.resolve(sourceelement.getText());
//                final Model3DStore storage = Model3Ds.read(path);
//                final SceneNode mesh = (SceneNode) storage.getElements().createIterator().next();
//                scenenode.getChildren().add(mesh);
//                ((MeshRootNode)scenenode).setBase(path);
//            }
//
//            readBaseProperties(element, scenenode);
//        }else if(XMLSceneConstants.LIGHT.equals(name)){
//            final DomElement typeelement = DomUtilities.getNodeForName(element, XMLSceneConstants.LIGHT_TYPE);
//            final DomElement colorelement = DomUtilities.getNodeForName(element, XMLSceneConstants.LIGHT_COLOR);
//            if(XMLSceneConstants.LIGHT_TYPE_DIRECTIONAL.equals(typeelement.getText())){
//                scenenode = new DirectionalLight();
//            }else if(XMLSceneConstants.LIGHT_TYPE_POINT.equals(typeelement.getText())){
//                scenenode = new PointLight();
//            }else if(XMLSceneConstants.LIGHT_TYPE_SPOT.equals(typeelement.getText())){
//                scenenode = new SpotLight();
//            }else{ //ambiant
//                scenenode = new AmbientLight();
//            }
//            if(colorelement!=null){
//                Color color = Colors.fromHexa(colorelement.getText());
//                ((Light)scenenode).setDiffuse(color);
//            }
//            readBaseProperties(element, scenenode);
//
//        }else if(XMLSceneConstants.SPEAKER.equals(name)){
//            scenenode = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
//            readBaseProperties(element, scenenode);
//        }
//
//        if(scenenode == null) return null;
//
//        //loop on children
//        final Node[] children = (Node[]) node.getChildren().toArray(Node.class);
//        for(int i=0;i<children.length;i++){
//            SceneNode candidate = rebuild((DomNode)children[i]);
//            if(candidate!=null){
//                scenenode.getChildren().add(candidate);
//            }
//        }
//
//        return scenenode;
    }

    private void readBaseProperties(DomElement element, DefaultGraphicNode node){
        final DomElement idelement = DomUtilities.getNodeForName(element, XMLSceneConstants.NODE_ID);
        final DomElement nameelement = DomUtilities.getNodeForName(element, XMLSceneConstants.NODE_NAME);
        final DomElement visibleelement = DomUtilities.getNodeForName(element, XMLSceneConstants.NODE_VISIBLE);
        final DomElement matrixelement = DomUtilities.getNodeForName(element, XMLSceneConstants.NODE_MATRIX);

        if(idelement!=null){
            node.setId(Int32.decode(idelement.getText()));
        }
        if(nameelement!=null){
            node.setTitle(nameelement.getText());
        }
        if(visibleelement!=null){
            node.setVisible(Int32.decode(visibleelement.getText()) != 0);
        }
        if(matrixelement!=null){
            final Chars[] values = matrixelement.getText().split(' ');
            final Matrix4x4 matrix = new Matrix4x4();
            final double[] array = new double[values.length];
            for(int i=0;i<array.length;i++){
                array[i] = Float64.decode(values[i]);
            }
            matrix.set(array);
            node.getNodeTransform().set(matrix);
        }

    }

    public void dispose() throws IOException {
        super.dispose();
        reader.dispose();
    }

}
