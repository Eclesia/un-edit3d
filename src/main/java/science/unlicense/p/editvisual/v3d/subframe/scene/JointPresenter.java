
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.p.editgraphic.IconStore;
import science.unlicense.p.editvisual.v3d.subframe.skeleton.SkeletonUtils;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class JointPresenter implements ObjectPresenter{

    private static final Dictionary IMAGES = new HashDictionary();

    @Override
    public Widget createWidget(Object candidate) {
        if(!(candidate instanceof Joint)){
            return null;
        }

        final Joint node = (Joint) candidate;
        final WLabel lbl = new WLabel(node.getTitle());
        lbl.setGraphicPlacement(WLabel.GRAPHIC_LEFT);

        SceneNode n;
        for(n=node;n != null && !(n instanceof Skeleton); n=n.getParent());
        final Color color = SkeletonUtils.getColor(node, (Skeleton) n);
        Image icon = (Image) IMAGES.getValue(color);
        if(icon == null){
            icon = IconStore.render(new Circle(8, 8, 4), color, 16, 16);
            IMAGES.add(color, icon);
        }
        lbl.setGraphic(new WGraphicImage(icon));

        return lbl;
    }

}
