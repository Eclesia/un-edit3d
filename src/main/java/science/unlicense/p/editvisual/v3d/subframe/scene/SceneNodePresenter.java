
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.display.api.layout.GridLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.p.editgraphic.action.AddLightAction;
import science.unlicense.p.editvisual.v3d.action.AddMeshAction;
import science.unlicense.p.editgraphic.action.AddNodeAction;
import science.unlicense.p.editgraphic.action.RemoveNodeAction;

/**
 *
 * @author Johann Sorel
 */
public class SceneNodePresenter implements ObjectPresenter{

    @Override
    public Widget createWidget(Object candidate) {
        if(!(candidate instanceof SceneNode)){
            return null;
        }

        final SceneNode node = (SceneNode) candidate;
        final WLabel lbl = new WLabel(node.getTitle());

        //actions
        final WContainer popup = new WContainer(new GridLayout(-1, 1));
        popup.getChildren().add(new WMenuButton(new AddNodeAction(node)));
        popup.getChildren().add(new WMenuButton(new AddMeshAction(node)));
        popup.getChildren().add(new WMenuButton(new AddLightAction(node)));
        popup.getChildren().add(new WMenuButton(new RemoveNodeAction(node)));
        lbl.setPopup(popup);

        return lbl;
    }

}
