
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTreeTable;

/**
 *
 * @author Johann Sorel
 */
public class WScenePane extends WContainer {

    public static final Chars PROPERTY_SCENE = new Chars("Scene");
    public static final Chars PROPERTY_SELECTION = new Chars("Selection");

    private final WTreeTable tree = new WTreeTable();

    public WScenePane() {
        setLayout(new BorderLayout());

        addChild(tree, BorderConstraint.CENTER);
        tree.setHeaderVisible(false);

        final Sequence seq = new ArraySequence();
        seq.add(new MeshPresenter());
        seq.add(new RigidBodyPresenter());
        seq.add(new JointPresenter());
        seq.add(new SceneNodePresenter());

        final DefaultTreeColumn nameCol = new DefaultTreeColumn(null,(Chars)null,new DelegatePresenter(seq));
        nameCol.setBestWidth(FormLayout.SIZE_EXPAND);
        final Column visibleCol = new VisibleColumnModel();
        visibleCol.setBestWidth(30);
        final Column removeCol = new DeleteColumnModel();
        removeCol.setBestWidth(30);
        tree.getColumns().add(nameCol);
        tree.getColumns().add(visibleCol);
        tree.getColumns().add(removeCol);

        tree.getSelection().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            public void receiveEvent(Event event) {
                final CollectionMessage ce = (CollectionMessage) event.getMessage();
                final Object[] selection = ce.getNewElements();
                if(selection!=null && selection.length!=0){
                    if(selection[0] instanceof SceneNode){
                        setSelection((SceneNode) selection[0]);
                    }
                }
            }
        });
    }

    public void setScene(SceneNode scene) {
        setPropertyValue(PROPERTY_SCENE, scene);
    }

    public SceneNode getScene() {
        return (SceneNode) getPropertyValue(PROPERTY_SCENE);
    }

    public Property varScene() {
        return getProperty(PROPERTY_SCENE);
    }

    public SceneNode getSelection() {
        return (SceneNode) getPropertyValue(PROPERTY_SELECTION);
    }

    public void setSelection(SceneNode scene) {
        setPropertyValue(PROPERTY_SELECTION, scene);
    }

    public Property varSelection() {
        return getProperty(PROPERTY_SELECTION);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_SCENE.equals(name)) {
            final SceneNode scene = (SceneNode) value;
            if (scene == null) {
                tree.setRowModel(null);
            } else {
                final TreeRowModel treeModel = new TreeRowModel(scene);
                tree.setRowModel(treeModel);
            }
        }
    }

}
