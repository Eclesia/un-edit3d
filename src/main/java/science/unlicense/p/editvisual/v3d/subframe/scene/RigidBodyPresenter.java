

package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.physics.api.body.RigidBody;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyPresenter implements ObjectPresenter{

    private static final Dictionary IMAGES = new HashDictionary();

    @Override
    public Widget createWidget(Object candidate) {
        if(!(candidate instanceof RigidBody)){
            return null;
        }

        final RigidBody node = (RigidBody) candidate;
        final Chars geomType = new Chars(node.getGeometry().getClass().getSimpleName());
        final WLabel lbl = new WLabel(new Chars("Physic : ").concat(geomType));

        return lbl;
    }

}
