
package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.FormatPredicate;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.model3d.gltf.model.v2.Animation;
import science.unlicense.model3d.impl.physic.JointKeyFrame;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;
import science.unlicense.model3d.impl.physic.SkeletonPoseResolver;
import science.unlicense.model3d.impl.physic.Skeletons;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.mmd.vmd.VMDFormat;
import science.unlicense.model3d.mmd.vpd.VPDFormat;
import science.unlicense.model3d.mmd.vpd.VPDStore;
import science.unlicense.model3d.xna.pose.XNAPoseFormat;
import science.unlicense.model3d.xna.pose.XNAPoseStore;
import science.unlicense.p.editvisual.v3d.Loadable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class WMotionPane extends WContainer implements Loadable {

    private static final Image ICON_PLAY;
    private static final Image ICON_PAUSE;
    private static final Image ICON_STOP;
    static {
        try {
            ICON_PLAY = Images.read(Paths.resolve(new Chars("mod:/icons/16/media-playback-start-5.png")));
            ICON_PAUSE = Images.read(Paths.resolve(new Chars("mod:/icons/16/media-playback-pause-5.png")));
            ICON_STOP = Images.read(Paths.resolve(new Chars("mod:/icons/16/media-playback-stop-5.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static final Predicate PREDICATE_VPD = new FormatPredicate(VPDFormat.INSTANCE);
    private static final Predicate PREDICATE_POSE = new FormatPredicate(XNAPoseFormat.INSTANCE);
    private static final Predicate PREDICATE_VMD = new FormatPredicate(VMDFormat.INSTANCE);

    private static final Chars LAST_POSE_PATH = new Chars("lastPosePath");
    private static final Chars LAST_MOTION_PATH = new Chars("lastMotionPath");

    private final WMenuButton loadPoseButton = new WMenuButton(new Chars("P"),null,new EventListener() {
        public void receiveEvent(Event event) {
            if(!(edited instanceof MotionModel)) return;
            new Thread(){
                public void run() {
                    final WPathChooser chooser = new WPathChooser();
                    final Chars val = Platform.get().getConfiguration().getProperty(LAST_POSE_PATH);
                    if(val != null){
                        final Path[] selected = new Path[]{Paths.resolve(val)};
                        chooser.setViewRoot(selected[0].getParent());
                        chooser.setSelectedPath(selected);
                    }
                    openPose(chooser.showOpenDialog(loadPoseButton.getFrame()));
                }
            }.start();
        }
    });

    private final WMenuButton loadMotionButton = new WMenuButton(new Chars("M"), null,new EventListener() {
        public void receiveEvent(Event event) {
            new Thread(){
                public void run() {
                    final WPathChooser chooser = new WPathChooser();
                    final Chars val = Platform.get().getConfiguration().getProperty(LAST_MOTION_PATH);
                    if(val != null){
                        final Path[] selected = new Path[]{Paths.resolve(val)};
                        chooser.setViewRoot(selected[0].getParent());
                        chooser.setSelectedPath(selected);
                    }
                    openMotion(chooser.showSaveDialog(loadMotionButton.getFrame()));
                }
            }.start();
        }
    });

    private final WMenuButton savePoseButton = new WMenuButton(new Chars("SP"),null, new EventListener() {
        public void receiveEvent(Event event) {
            if(skeleton== null) return;
            new Thread(){
                public void run() {
                    final WPathChooser chooser = new WPathChooser();
                    final Sequence predicates = new ArraySequence();
                    predicates.add(PREDICATE_VPD);
                    predicates.add(PREDICATE_POSE);
                    chooser.setPredicates(predicates);
                    chooser.setFilter(PREDICATE_VPD);
                    final Path path = chooser.showSaveDialog(savePoseButton.getFrame());
                    try {
                        savePose(path, chooser.getFilter());
                    } catch (IOException ex) {
                        Loggers.get().log(ex, Logger.LEVEL_WARNING);
                    } catch (StoreException ex) {
                        Loggers.get().log(ex, Logger.LEVEL_WARNING);
                    }
                }
            }.start();
        }
    });


    private final WMenuButton wPlayButton = new WMenuButton(Chars.EMPTY,null,new EventListener() {
        public void receiveEvent(Event event) {
            if(animation!=null){
//                animation.start();
            }
        }
    });
    private final WMenuButton wPauseButton = new WMenuButton(Chars.EMPTY,null,new EventListener() {
        public void receiveEvent(Event event) {
            if(animation!=null){
//                animation.stop();
            }
        }
    });
    private final WMenuButton wStopButton = new WMenuButton(Chars.EMPTY,null,new EventListener() {
        public void receiveEvent(Event event) {
            if(animation!=null){
//                animation.stop();
//                animation.reset();
            }
        }
    });


    private final WSkeletionAnimEditor wAnimEditor = new WSkeletionAnimEditor();

    //current scene node
    private SceneNode edited;
    //it's skeleton if it has one
    private Skeleton skeleton;
    private Animation animation;

    public WMotionPane() {
        setLayout(new BorderLayout());
        loadPoseButton.setEnable(false);
        loadMotionButton.setEnable(false);
        wPlayButton.setGraphic(new WGraphicImage(ICON_PLAY));
        wPauseButton.setGraphic(new WGraphicImage(ICON_PAUSE));
        wStopButton.setGraphic(new WGraphicImage(ICON_STOP));

        final WButtonBar bar = new WButtonBar();
        bar.setLayout(new FormLayout());
        bar.addChild(loadPoseButton,FillConstraint.builder().coord(0, 0).build());
        bar.addChild(loadMotionButton,FillConstraint.builder().coord(1, 0).build());
        bar.addChild(new WSpace(new Extent.Double(100, 10)), FillConstraint.builder().coord(2, 0).build());
        bar.addChild(savePoseButton,FillConstraint.builder().coord(3, 0).build());
        bar.addChild(new WSpace(new Extent.Double(100, 10)), FillConstraint.builder().coord(4, 0).build());
        bar.addChild(wPlayButton, FillConstraint.builder().coord(5, 0).build());
        bar.addChild(wPauseButton, FillConstraint.builder().coord(6, 0).build());
        bar.addChild(wStopButton, FillConstraint.builder().coord(7, 0).build());

        addChild(bar,BorderConstraint.TOP);
        addChild(wAnimEditor,BorderConstraint.CENTER);
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setEdited(Object edited) {
        if(this.edited == edited) return;
        if(!(edited instanceof SceneNode)) return;
        this.edited = (SceneNode) edited;

        if(edited instanceof MotionModel){
            final MotionModel mpm = (MotionModel) edited;
            skeleton = mpm.getSkeleton();
            final Iterator ite = mpm.getUpdaters().createIterator();
            while(ite.hasNext()){
                final Updater up = (Updater) ite.next();
                if(up instanceof Animation){
                    this.animation = (Animation)up;
//                    wAnimEditor.setAnimation((Animation)up);
                }
            }
        }else{
            wAnimEditor.setDirty();
        }

        loadPoseButton.setEnable(edited instanceof MotionModel);
        loadMotionButton.setEnable(edited instanceof MotionModel);
    }

    public SceneNode getEdited() {
        return edited;
    }

    private void openPose(Path path){
        if(path==null) return;

        Platform.get().getConfiguration().setProperty(LAST_POSE_PATH,new Chars(path.toURI()));
        try {
            Platform.get().getConfiguration().save();
        } catch (IOException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }
        try {
            Store store = Model3Ds.open(path);
            final Collection col = ((SceneResource)store).searchElements(RelativeSkeletonPose.class);
            final Iterator ite = col.createIterator();
            if(ite.hasNext()){
                final RelativeSkeletonPose pose = (RelativeSkeletonPose) ite.next();
                SkeletonPoseResolver.resolve1(skeleton, pose,null);
                skeleton.solveKinematics();
                skeleton.updateBindPose();

                //TODO
//                final SkeletonAnimation animation = new SkeletonAnimation(skeleton);
//                final Iterator jite = skeleton.getJoints().createIterator();
//                while(jite.hasNext()){
//                    final Joint joint = (Joint) jite.next();
//                    final JointTimeSerie serie = new JointTimeSerie(joint);
//                    final JointFrame frame = new JointFrame(joint, 0);
//                    frame.getPose()
//                    serie.getFrames().add(frame);
//                    animation.getSeries().add(serie);
//                }

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (StoreException ex) {
            ex.printStackTrace();
        }
    }

    private void openMotion(Path path){
        if(path==null) return;

        Platform.get().getConfiguration().setProperty(LAST_MOTION_PATH,new Chars(path.toURI()));

        try {
            Platform.get().getConfiguration().save();
        } catch (IOException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }

//        try {
//            final Model3DStore store = Model3Ds.read(path);
//            for(Iterator ite=store.getElements().createIterator();ite.hasNext();){
//                final Object candidate = ite.next();
//                if(candidate instanceof Animation){
//                    this.animation = (Animation) candidate;
//                    mapAnimation(animation, skeleton, edited,null);
//                    edited.getUpdaters().add(animation);
//                    wAnimEditor.setAnimation(animation);
//                }
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    private void savePose(Path path, Predicate format) throws IOException, StoreException{
        if(path==null) return;

        final Sequence data = new ArraySequence();
        final RelativeSkeletonPose pose = Skeletons.toRelative(skeleton, JointKeyFrame.FROM_BASE);
        data.add(pose);

        if(format == PREDICATE_VPD){
            final VPDStore store = new VPDStore(path);
            store.writeElements(data);
        }else if(format == PREDICATE_POSE){
            final XNAPoseStore store = new XNAPoseStore(path);
            store.writeElements(data);
        }
    }

    @Override
    public void load(final Platform platform) {
        platform.getProject().addEventListener(new PropertyPredicate(Project.PROPERTY_ACTIVEOBJECT), new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                setEdited(platform.getProject().getActiveObject());
            }
        });
    }

}
