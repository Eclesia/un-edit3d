

package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;

/**
 *
 * @author Johann Sorel
 */
public interface ObjectEditor {

    Image getIcon();

    Widget install(Object candidate);

    void uninstall(Object candidate);


}
