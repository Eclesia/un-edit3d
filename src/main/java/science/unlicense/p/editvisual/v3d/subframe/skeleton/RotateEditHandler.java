
package science.unlicense.p.editvisual.v3d.subframe.skeleton;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.opengl.phase.picking.PickMessage;
import science.unlicense.engine.opengl.phase.picking.PickResetPhase;
import science.unlicense.geometry.impl.Plane;
import science.unlicense.geometry.impl.Ray;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.impl.VectorNd;
import science.unlicense.math.impl.Vector3d;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.p.editvisual.v3d.handler.OrbitHandler;
import science.unlicense.p.editvisual.v3d.subframe.property.editor.SkeletonEditor;
import science.unlicense.physics.api.skeleton.Joint;

/**
 *
 * @author Johann Sorel
 */
public class RotateEditHandler extends OrbitHandler implements EventListener {

    public static final Chars GRAB = new Chars("Grab");
    public static final Chars ARC0 = new Chars("Arc0");
    public static final Chars ARC1 = new Chars("Arc1");
    public static final Chars ARC2 = new Chars("Arc2");
    public static final Chars TRS0 = new Chars("Trs0");
    public static final Chars TRS1 = new Chars("Trs1");
    public static final Chars TRS2 = new Chars("Trs2");

    //currently dragged joint
    private SkeletonDebugLayer.DebugState state;
    private Joint ej = null;
    private int axis = -1;
    private boolean translate = false;
    private Plane plane;

    //gesture informations
    private double lastMouseX = 0;
    private double lastMouseY = 0;
    private double diffX = 0;
    private double diffY = 0;
    //moving target controls
    private double dragX = 0;
    private double dragY = 0;


    public RotateEditHandler(SkeletonEditor pane) {
        super(new Chars("Joint rotation editor"));
    }

    public void install(Platform platform) {
        super.install(platform);
        //we will forward the events to the controller
        //TODO this is not done the right way
        platform.removeEventListener(MouseMessage.PREDICATE, controller);
    }

    public void uninstall(Platform platform) {
        super.uninstall(platform);
    }

    public synchronized void receiveEvent(Event event) {
        final EventMessage message = event.getMessage();
        if(!(message instanceof MouseMessage)) return;
        final MouseMessage me = (MouseMessage) message;

        calculateMouseMove(me);

        if(me.getType() == MouseMessage.TYPE_PRESS){
            handlePick(me);
            //store the original joint transform
            if(ej!=null){
                //calculate the plane passing through joint center and facing camera
                //the plane is in world space

                final Similarity jointToWorld = ej.getNodeToRootSpace();
                final Similarity cameraToWorld = camera.getNodeToRootSpace();

                final Vector3d center = new Vector3d();
                jointToWorld.transform(center, center);

                final VectorNd normal = new VectorNd(camera.getForward().copy().negate(),0);
                cameraToWorld.toMatrix().transform(normal, normal);
                normal.localNormalize();

                plane = new Plane(normal, center);
            }

        }else if(axis>=0 && me.getType() == MouseMessage.TYPE_MOVE){

            //calculate mouse camera ray
            final Ray ray = camera.calculateRayWorldCS((int)lastMouseX, (int)lastMouseY);

//            if(axis==3){
//                if(plane!=null){
//                    try {
//                        final Point pt = (Point) ray.intersection(plane);
//                        if(pt!=null){
//                            //convert intersection point in node space and move the joint
//                            final Similarity WorldToJoint = ej.getRootToNodeSpace();
//                            final TupleRW coord = pt.getCoordinate();
//                            WorldToJoint.transform(coord, coord);
//                            ej.getNodeTransform().getTranslation().localAdd(coord);
//                        }
//
//                    } catch (OperationException ex) {
//                        Loggers.get().log(ex,Logger.LEVEL_WARNING);
//                    }
//                }
//            }else{
//
//                if(translate){
//                    //dragging an axis
//                    final VectorRW v = ej.getNodeTransform().getTranslation();
//                    v.set(axis, v.get(axis)+diffY);
//                }else{
//                    //rotate around axis
//                    final MatrixRW matrix = ej.getNodeTransform().getRotation();
//                    final DefaultVector ax = new DefaultVector(3);
//                    ax.set(axis, 1);
//                    final MatrixRW m = Matrix3x3.createRotation3(Math.toRadians(diffY), ax);
//                    m.localMultiply(matrix);
//                    matrix.set(m);
//                }
//            }
            ej.getNodeTransform().notifyChanged();
            state.mesh.getSkeleton().updateBindPose();
            //update iks
            state.mesh.getSkeleton().solveKinematics();
            state.mesh.getSkeleton().updateBindPose();
            message.consume();
        }else if(me.getType() == MouseMessage.TYPE_RELEASE){
            axis = -1;
            plane = null;
        }

        controller.receiveEvent(event);
    }

    private void calculateMouseMove(MouseMessage e){
        diffX = e.getMousePosition().get(0)-lastMouseX;
        diffY = e.getMousePosition().get(1)-lastMouseY;
        lastMouseX = e.getMousePosition().get(0);
        lastMouseY = e.getMousePosition().get(1);
    }

    private void handlePick(MouseMessage me){
        final Project project = Platform.get().getProject();
        final PickResetPhase pickphase = SkeletonDebugLayer.INSTANCE.getPickingPhase();

        if(pickphase==null) return;

        pickphase.pickAt(me.getMousePosition(), new EventListener() {
            public void receiveEvent(Event event) {
                final SceneNode selection = ((PickMessage)event.getMessage()).getSelection();
                if(selection == null) return;

                final CharArray elementName = selection.getTitle();

                //check if we selected a joint sphere
                Joint joint = (Joint) selection.getUserProperties().getValue(SkeletonUtils.PROPERTY_JOINT);
                if(joint != null){
                    Platform.get().getProject().setActiveObject(joint);
                    return;
                }
                final Dictionary dico = ((SceneNode)selection.getParent()).getUserProperties();

                if(GRAB.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = true;
                    axis = 3;
                }else if(ARC0.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = false;
                    axis = 0;
                }else if(ARC1.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = false;
                    axis = 1;
                }else if(ARC2.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = false;
                    axis = 2;
                }else if(TRS0.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = true;
                    axis = 0;
                }else if(TRS1.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = true;
                    axis = 1;
                }else if(TRS2.equals(elementName)){
                    ej = (Joint)dico.getValue(SkeletonUtils.PROPERTY_JOINT);
                    state = (SkeletonDebugLayer.DebugState) dico.getValue(SkeletonUtils.PROPERTY_STATE);
                    translate = true;
                    axis = 2;
                }

            }
        });
    }

}
