

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.p.editvisual.v3d.io.XMLSceneWriter;

/**
 *
 * @author Johann Sorel
 */
public class SaveSceneAction extends AbstractActionExecutable {

    public SaveSceneAction() {
        setText(new Chars("Save"));
    }

    public Object perform() {
        try{
            final Project project = Platform.get().getProject();
            final XMLSceneWriter writer = new XMLSceneWriter();
            writer.setOutput(project.getProjectPath());
            writer.write(project.getRoot());
        }catch(IOException ex){
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }
        return null;
    }

}
