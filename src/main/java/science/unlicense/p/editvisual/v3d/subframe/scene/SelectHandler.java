
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.p.editvisual.v3d.handler.OrbitHandler;

/**
 *
 * @author Johann Sorel
 */
public class SelectHandler extends OrbitHandler implements EventListener {

    public SelectHandler() {
        super(new Chars("Mesh selection"));
    }

    public void install(Platform platform) {
        super.install(platform);
        platform.addEventListener(MouseMessage.PREDICATE, this);
    }

    public void uninstall(Platform platform) {
        super.uninstall(platform);
        platform.removeEventListener(MouseMessage.PREDICATE, this);
    }

    public void receiveEvent(Event event) {
        final MouseMessage me = (MouseMessage) event.getMessage();

        if(me.getButton() != MouseMessage.BUTTON_3) return;

        final Project project = Platform.get().getProject();
//        Platform.get().getPickPhase().pickAt(new Vector(me.getMousePosition()), new EventListener() {
//
//            @Override
//            public void receiveEvent(Event event) {
//                final GLNode selection = ((PickEvent)event).getSelection();
//                if(selection != null){
//                    //check if selected node is part of the editable scene and not
//                    //the widget plan, grid or xyz marker.
//                    boolean valid = false;
//                    SceneNode parent = selection;
//                    final SceneNode projectRoot = project.getRoot();
//                    do{
//                        if(parent == projectRoot){
//                            valid=true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }while(parent!=null);
//
//                    if(valid){
//                        project.setActiveObject(selection);
//                    }
//                }
//            }
//        });

    }

}
