
package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.DefaultNamedNode;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.anim.CompoundAnimation;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.model.Column;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultTreeColumn;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.model.TreeRowModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WTreeTable;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.model3d.impl.physic.JointTimeSerie;
import science.unlicense.model3d.impl.physic.SkeletonAnimation;
import science.unlicense.p.editvisual.v3d.subframe.scene.DelegatePresenter;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * Skeletion animation panel.
 * Displays two cut views of the skeleton and a ribbon
 * for all Joint time series.
 *
 * @author Johann Sorel
 */
public class WSkeletionAnimEditor extends WContainer {

    private final WTreeTable tree = new WTreeTable();
    private final WTimeSerieHeader timeHeader = new WTimeSerieHeader();


    private Animation animation;
    private SkeletonAnimation skeletonAnimation;

    public WSkeletionAnimEditor() {
        super(new BorderLayout());
        addChild(tree,BorderConstraint.CENTER);
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(final Animation candidate) {
        if(this.animation == candidate) return;
        this.animation = candidate;
        this.timeHeader.setAnimation(candidate);
        SkeletonAnimation skeAnim = null;
        if(animation instanceof SkeletonAnimation){
            skeAnim = (SkeletonAnimation) animation;
        }else if(animation instanceof CompoundAnimation){
            final CompoundAnimation compound = (CompoundAnimation) candidate;
            final Sequence subs = compound.getElements();
            for(int i=0;i<subs.getSize();i++){
                if(subs.get(i) instanceof SkeletonAnimation){
                    skeAnim = (SkeletonAnimation) subs.get(i);
                    break;
                }
            }
        }


        this.skeletonAnimation = skeAnim;

        final DefaultNamedNode root = new DefaultNamedNode(true);
        root.setName(new Chars("Skeleton"));

        final Skeleton skeleton = skeAnim.getSkeleton();
        root.getChildren().add(skeleton.getChildren());

        final TreeRowModel treeModel = new TreeRowModel(root);
        final Column nameCol = new DefaultTreeColumn(null,Chars.EMPTY, new DelegatePresenter(new ArraySequence()));
        final Column timecol = new DefaultColumn(timeHeader, new TimePresenter());

        tree.setRowModel(treeModel);
        tree.getColumns().add(nameCol);
        tree.getColumns().add(timecol);
        TreeRowModel.setFoldRecursive(treeModel, root, true);

//        final Sequence bands = new ArraySequence();
//        final DefaultRibbonModel model = new DefaultRibbonModel(bands);
//        ribbon.setModel(model);
//
//        if(skeAnim!=null){
//            for(int i=0,n=skeAnim.getSeries().getSize();i<n;i++){
//                final JointTimeSerie serie = (JointTimeSerie) skeAnim.getSeries().get(i);
//                bands.add(new JointTimeSerieBand(serie,model));
//            }
//            final SkeletonAnimation ani = skeAnim;
//            skeAnim.addEventListener(PropertyEvent.class, new EventListener() {
//                public void receiveEvent(Class eventClass, Event event) {
//                    ribbon.setAnimationPosition(ani.getTime());
//                }
//            });
//        }

    }

    private class TimePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
//            final TreeRowPath treePath = (TreeRowPath) candidate;
//            candidate = treePath.getLeaf();
            JointTimeSerie serie = null;
            if(skeletonAnimation!=null && candidate instanceof Joint){
                final Joint joint = (Joint) candidate;
                serie = skeletonAnimation.getJointTimeSerie(CObjects.toChars(joint.getId()));
            }
            return new WTimeSerieCell(timeHeader, serie);
        }
    }

}
