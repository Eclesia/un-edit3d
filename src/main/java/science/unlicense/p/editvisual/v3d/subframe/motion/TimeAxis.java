

package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.math.impl.Affine1;


/**
 *
 * @author Johann Sorel
 */
public class TimeAxis extends AbstractEventSource {

    public static final Chars PROPERTY_TRANSFORM = new Chars("modeltoview");

    private final Affine1 transform = new Affine1();

    public TimeAxis() {
    }

    /**
     * Model to view translation.
     *
     * @return translation
     */
    public double getTranslation(){
        return transform.get(0, 1);
    }

    public void setTranslation(double translation){
        final Affine1 old = new Affine1(transform);
        transform.set(0, 1, translation);
        getEventManager().sendPropertyEvent(this, PROPERTY_TRANSFORM, old, new Affine1(transform));
    }

    /**
     * Model to view scale.
     *
     * @return scale
     */
    public double getScale(){
        return transform.get(0, 0);
    }

    public void setScale(double scale){
        final Affine1 old = new Affine1(transform);
        transform.set(0, 0, scale);
        getEventManager().sendPropertyEvent(this, PROPERTY_TRANSFORM, old, new Affine1(transform));
    }

    public void setTransform(double translate, double scale){
        final Affine1 old = new Affine1(transform);
        transform.set(0, 0, scale);
        transform.set(0, 1, translate);
        getEventManager().sendPropertyEvent(this, PROPERTY_TRANSFORM, old, new Affine1(transform));
    }

    public void scale(double factor, double position){
        final Affine1 modelToView = transform;
        final Affine1 old = new Affine1(modelToView);
        position = toModel(position);
        modelToView.localMultiply(new Affine1(1, +position));
        modelToView.localScale(factor);
        modelToView.localMultiply(new Affine1(1, -position));
        getEventManager().sendPropertyEvent(this, PROPERTY_TRANSFORM, old, new Affine1(modelToView));
    }

    public double toModel(double view){
        return (view-getTranslation()) / getScale();
    }

    public double toView(double model){
        return getScale()*model + getTranslation();
    }

    @Override
    public Class[] getEventClasses() {
        return new Class[]{PropertyMessage.class};
    }

}
