

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.p.editvisual.v3d.Platform;

/**
 *
 * @author Johann Sorel
 */
public class NewSceneAction extends AbstractActionExecutable {

    public NewSceneAction() {
        setText(new Chars("New"));
    }

    public Object perform() {
        new Thread(){
            @Override
            public void run() {
                final Path p = WPathChooser.showSaveDialog((Predicate)null);
                Platform.get().getProject().newScene(p);
            }
        }.start();
        return null;
    }

}
