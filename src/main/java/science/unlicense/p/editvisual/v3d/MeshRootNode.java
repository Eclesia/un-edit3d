
package science.unlicense.p.editvisual.v3d;

import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class MeshRootNode extends DefaultGraphicNode {

    private Path base;

    public MeshRootNode() {
        this(null);
    }

    public MeshRootNode(Path base) {
        super(CoordinateSystems.UNDEFINED_3D);
        this.base = base;
    }

    public Path getBase() {
        return base;
    }

    public void setBase(Path base) {
        this.base = base;
    }

}
