
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;

/**
 *
 * @author Johann Sorel
 */
public class DeleteColumnModel extends DefaultColumn{


    public DeleteColumnModel() {
        super(Chars.EMPTY,new DeletePresenter());
    }

    private static class DeletePresenter implements ObjectPresenter{

        public Widget createWidget(final Object candidate) {
            if(candidate instanceof SceneNode){
                final WButton button = new WButton();
                button.setText(new Chars(" - "));
                button.addEventListener(ActionMessage.PREDICATE, new EventListener() {
                    public void receiveEvent(Event event) {
                        final SceneNode node = (SceneNode)candidate;
                        node.getParent().getChildren().remove(node);
                    }
                });
                return button;
            }else{
                return new WSpace(new Extent.Double(2));
            }
        }

    }

}
