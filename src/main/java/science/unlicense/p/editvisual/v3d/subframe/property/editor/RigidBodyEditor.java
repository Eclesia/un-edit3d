
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.physics.api.body.RigidBody;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class RigidBodyEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/code-block.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private final WLabel lblMass = new WLabel(new Chars("mass"));
    private final WSpinner editMass = new WSpinner(new NumberSpinnerModel(Double.class, 1.0, null, null, 1.0));

    private RigidBody edited;

    public RigidBodyEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        //build common panel ---------------------------------------------------
        addChild(lblMass,         FillConstraint.builder().coord(0,0).build());
        addChild(editMass,        FillConstraint.builder().coord(1,0).span(2,1).build());

        editMass.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                try{
                    double mass = (Double)pe.getNewValue();
                    edited.setMass(mass);
                }catch(Exception ex){
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
            }
        });

    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof RigidBody)) return null;

        edited = (RigidBody) candidate;
        editMass.setValue(edited.getMass());

        return this;
    }

    public void uninstall(Object candidate) {

    }

}
