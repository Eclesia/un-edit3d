
package science.unlicense.p.editvisual.v3d.io;

import science.unlicense.common.api.character.Chars;


/**
 *
 * @author Johann Sorel
 */
public class XMLSceneConstants {

    public static final Chars NODE = new Chars("node");
    public static final Chars NODE_ID = new Chars("id");
    public static final Chars NODE_NAME = new Chars("name");
    public static final Chars NODE_VISIBLE = new Chars("visible");
    public static final Chars NODE_MATRIX = new Chars("matrix");

    public static final Chars MESH = new Chars("mesh");
    public static final Chars MESH_SOURCE = new Chars("source");

    public static final Chars LIGHT = new Chars("light");
    public static final Chars LIGHT_COLOR = new Chars("color");
    public static final Chars LIGHT_TYPE = new Chars("type");
    public static final Chars LIGHT_TYPE_AMBIANT = new Chars("ambiant");
    public static final Chars LIGHT_TYPE_DIRECTIONAL = new Chars("directional");
    public static final Chars LIGHT_TYPE_POINT = new Chars("point");
    public static final Chars LIGHT_TYPE_SPOT = new Chars("spot");

    public static final Chars SPEAKER = new Chars("speaker");

    private XMLSceneConstants(){}

}
