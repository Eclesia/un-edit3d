
package science.unlicense.p.editvisual.v3d.subframe.skeleton;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonUtils {

    public static final NodeVisitor RESIZE_JOINT = new NodeVisitor() {
        public Object visit(Node node, Object context) {
//            if(node instanceof GeometryMesh && ((GeometryMesh)node).getProperties().getValue(PROPERTY_JOINT) != null){
//                final double sphereSize = (Double)context;
//                final GeometryMesh glNode = ((GeometryMesh)node);
//                if(glNode.getGeometry() instanceof Sphere){
//                    //resize only spheres, not lines
//                    final NodeTransform trs = glNode.getNodeTransform();
//                    trs.getScale().setXYZ(sphereSize, sphereSize, sphereSize);
//                    trs.notifyChanged();
//                }
//            }
            return super.visit(node, context);
        }
    };
    public static final NodeVisitor SHOW_JOINT = new NodeVisitor() {
        public Object visit(Node node, Object context) {
//            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_JOINT) != null){
//                final GLNode glNode = ((GLNode)node);
//                glNode.setVisible((Boolean)context);
//            }
            return super.visit(node, context);
        }
    };
//    public static final NodeVisitor SHOW_CONSTRAINTS = new DefaultNodeVisitor() {
//        public Object visit(Node node, Object context) {
//            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_JOINT) != null){
//                final GLNode glNode = ((GLNode)node);
//                glNode.setVisible((Boolean)context);
//            }
//            return super.visit(node, context);
//        }
//    };
    public static final NodeVisitor SHOW_PHYSIC = new NodeVisitor() {
        public Object visit(Node node, Object context) {
//            if(node instanceof GLNode && ((GLNode)node).getProperties().getValue(PROPERTY_RIGIDBODY) != null){
//                final GLNode glNode = ((GLNode)node);
//                glNode.setVisible((Boolean)context);
//            }
            return super.visit(node, context);
        }
    };

    public static final Chars PROPERTY_JOINT = new Chars("joint");
    public static final Chars PROPERTY_STATE = new Chars("state");
    public static final Chars PROPERTY_RIGIDBODY = new Chars("rigidbody");

    public static SceneNode viewSkeleton(SkeletonDebugLayer.DebugState state, float sphereSize){

        final Skeleton skeleton = state.mesh.getSkeleton();

        final Sequence workroots = skeleton.getChildren();
        SceneNode skeletonDebugNode = null;
//        skeletonDebugNode.setName(new Chars("SKELETON DEBUG"));
//        for(int i=0,n=workroots.getSize();i<n;i++){
//            final GLNode gljoint = debugJoint(state, (Joint)workroots.get(i), skeleton, sphereSize);
//            skeletonDebugNode.getChildren().add(gljoint);
//        }

        return skeletonDebugNode;
    }

//    public static SceneNode debugJoint(SkeletonDebugLayer.DebugState state, final Joint joint, final Skeleton skeleton, float shpereSize) {
//        final GLNode node = new GLNode();

//        node.getUpdaters().add(new ConstraintUpdater(new CopyTransformConstraint(node, joint, 1)));
//
//        final Color color = getColor(joint, skeleton);
//
//        final Sequence jc = joint.getChildren();
//        for(int i=jc.getSize()-1;i>=0;i--){
//            final Node nc = (Node) jc.get(i);
//
//            if(nc instanceof Joint){
//                final Joint child = (Joint) nc;
//
//                //add a line toward the child
//                final Matrix childmatrix = child.getNodeTransform().asMatrix();
//                final Tuple t = childmatrix.transform(new DefaultVector(0, 0, 0));
//                float[] end = t.toFloat();
//                final Mesh line = new Mesh();
//                line.getProperties().add(PROPERTY_JOINT, joint);
//                line.getProperties().add(PROPERTY_STATE, state);
//                line.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(color)));
//                line.setPickable(false);
//                final Shell shell = new Shell();
//                final FloatSequence fl = new FloatSequence();
//                fl.put(0).put(0).put(0);
//                fl.put(end);
//                shell.setVertices(new VBO(fl.toArrayFloat(),3));
//                shell.setNormals(new VBO(fl.toArrayFloat(),3));
//                shell.setIndexes(new IBO(new int[]{0,1}, 2), IndexRange.LINES(0, 2));
//                line.setShape(shell);
//                node.getChildren().add(line);
//
//                //build the child tree
//                final GLNode childdj = debugJoint(state, child,skeleton, shpereSize);
//                node.getChildren().add(childdj);
//            }else if(nc instanceof RigidBody){
//                final RigidBody body = (RigidBody) nc;
//                final Geometry geom = body.getGeometry();
//
//                final Mesh ele;
//                if(GeometryMesh.canHandle(geom.getClass())){
//                    ele = new GeometryMesh(geom, 10, 10);
//                }else{
//                    ele = null;
//                    System.out.println("Can not display geometry of type : "+geom.getClass());
//                }
//
//                if(ele != null){
//                    ele.getProperties().add(PROPERTY_RIGIDBODY, body);
//                    ele.getProperties().add(PROPERTY_STATE, state);
//                    //ele.setWireframe(true);
//                    ele.getNodeTransform().set(body.getNodeTransform());
//                    ele.getNodeTransform().notifyChanged();
//                    ele.setPickable(false);
//                    final Color rbColor = body.isFree() ? Color.BLUE : Color.RED;
//                    ele.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(rbColor)));
//                    node.getChildren().add(ele);
//                }
//
//            }
//        }

        //small sphere at joint root
//        final Mesh sphere = new GeometryMesh(new Sphere(1), 10, 10);
//        sphere.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(color)));
//        sphere.setPickable(true);
//        sphere.getProperties().add(PROPERTY_JOINT, joint);
//        sphere.getProperties().add(PROPERTY_STATE, state);
//        sphere.getNodeTransform().getScale().setXYZ(shpereSize, shpereSize, shpereSize);
//        sphere.getNodeTransform().notifyChanged();
//        node.getChildren().add(sphere);

        //constraints
//        final Sequence csts = joint.getConstraints();
//        for(int i=0,n=csts.getSize();i<n;i++){
//            final Constraint cst = (Constraint) csts.get(i);
//            if(cst instanceof AngleLimitConstraint){
//                final AngleLimitConstraint alc = (AngleLimitConstraint) cst;
//                final double min = alc.getLimits().getMin(0);
//                final double max = alc.getLimits().getMax(0);
//                final Mesh cone = new GeometryMesh(new Cone(1,0.5), 10, 10);
//                cone.getMaterial().putLayer(new Layer(new ColorMapping(color)));
//                cone.getMaterial().setLightVulnerable(false);
//                cone.setPickable(false);
//                //cone.getProperties().add(PROPERTY_JOINT, joint);
//                //cone.getModelTransform().getScale().setXYZ(shpereSize, shpereSize, shpereSize);
//                cone.getModelTransform().notifyChanged();
//                node.addChild(cone);
//            }else{
//                System.out.println("can not display joint constraint of type : "+cst.getClass().getSimpleName());
//            }
//        }

//        return node;
//    }

    public static Color getColor(Joint jt,Skeleton skeleton){
        final Color color;
        if(jt.isKinematicTarget(skeleton)){
            color = Color.RED;
        }else if(jt.isKinematicEffector(skeleton)){
            color = Color.GREEN;
        }else if(jt.isKinematic(skeleton)){
            color = Color.BLUE;
        }else{
            color = Color.GRAY_LIGHT;
        }
        return color;
    }

    public static Model createArc(Color color, float distance, float width, int axis){
        final Model model = new DefaultModel();
//        final Shell shell = new Shell();
//
//        final FloatSequence buffer = new FloatSequence();
//        final IntSequence index = new IntSequence();
//        for(int i=0;i<360;i++){
//            double angle = Angles.degreeToRadian(i);
//            float x = (float) Math.cos(angle) * distance;
//            float y = (float) Math.sin(angle) * distance;
//            if(axis==0){
//                buffer.put(+width).put(x).put(y);
//                buffer.put(-width).put(x).put(y);
//            }else if(axis==1){
//                buffer.put(x).put(+width).put(y);
//                buffer.put(x).put(-width).put(y);
//            }else if(axis==2){
//                buffer.put(x).put(y).put(+width);
//                buffer.put(x).put(y).put(-width);
//            }
//
//            index.put(i*2);
//            index.put(i*2+1);
//        }
//
//        shell.setVertices(new VBO(buffer.toArrayFloat(), 3));
//        shell.setNormals(new VBO(buffer.toArrayFloat(), 3));
//        shell.setIndexes(new IBO(index.toArrayInt(), 3), IndexRange.TRIANGLE_STRIP(0, 360*2));
//        mesh.setShape(shell);
//        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(color)));
//        mesh.getNodeTransform().getScale().setXYZ(10, 10, 10);
//        mesh.getNodeTransform().notifyChanged();
//        //mesh.setCullFace(-1);

        return model;
    }

//    public static Mesh createSphere(Color color, float radius){
//
//        final Mesh mesh = new GeometryMesh(new Sphere(radius));
//        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(color)));
//        mesh.getNodeTransform().getScale().setXYZ(10, 10, 10);
//        mesh.getNodeTransform().notifyChanged();
//        //mesh.setCullFace(-1);
//        return mesh;
//    }
//
//    public static Mesh createAxe(Color color, float distance, float width, int axis){
//
//        final BBox bbox = new BBox(3);
//        for(int i=0;i<3;i++){
//            if(axis==i){
//                bbox.setRange(i, 0, distance);
//            }else{
//                bbox.setRange(i, -width, +width);
//            }
//        }
//
//        final Mesh mesh = new GeometryMesh(bbox);
//        mesh.getMaterial().putOrReplaceLayer(new Layer(new PlainColorMapping(color)));
//        mesh.getNodeTransform().getScale().setXYZ(10, 10, 10);
//        mesh.getNodeTransform().notifyChanged();
//        //mesh.setCullFace(-1);
//        return mesh;
//    }
}
