
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.display.api.layout.Extents;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.model.CheckGroup;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.api.color.Colors;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class LightEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/light_bulb.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

     //properties for lights
    private final WLabel lblAmbiant = new WLabel(new Chars("Ambiant"));
    private final WCheckBox editAmbiant = new WCheckBox(null,new EventListener() {
        public void receiveEvent(Event event) {
            if(edited==null) return;
            final EventMessage message = event.getMessage();
            if(((PropertyMessage)message).getNewValue() != Boolean.TRUE) return;
            final SceneNode parent = edited.getParent();
            parent.getChildren().remove(edited);
            final Light light = new AmbientLight();
            parent.getChildren().add(light);
            install(light);
        }
    });
    private final WLabel lblDirectional = new WLabel(new Chars("Directional"));
    private final WCheckBox editDirectional = new WCheckBox(null,new EventListener() {
        public void receiveEvent(Event event) {
            if(edited==null) return;
            final EventMessage message = event.getMessage();
            if(((PropertyMessage)message).getNewValue() != Boolean.TRUE) return;
            final SceneNode parent = edited.getParent();
            parent.getChildren().remove(edited);
            final Light light = new DirectionalLight();
            parent.getChildren().add(light);
            install(light);
        }
    });
    private final WLabel lblPoint = new WLabel(new Chars("Point"));
    private final WCheckBox editPoint = new WCheckBox(null,new EventListener() {
        public void receiveEvent(Event event) {
            if(edited==null) return;
            final EventMessage message = event.getMessage();
            if(((PropertyMessage)message).getNewValue() != Boolean.TRUE) return;
            final SceneNode parent = edited.getParent();
            parent.getChildren().remove(edited);
            final Light light = new PointLight();
            parent.getChildren().add(light);
            install(light);
        }
    });
    private final WLabel lblSpot = new WLabel(new Chars("Spot"));
    private final WCheckBox editSpot = new WCheckBox(null,new EventListener() {
        public void receiveEvent(Event event) {
            if(edited==null) return;
            final EventMessage message = event.getMessage();
            if(((PropertyMessage)message).getNewValue() != Boolean.TRUE) return;
            final SceneNode parent = edited.getParent();
            parent.getChildren().remove(edited);
            final Light light = new SpotLight();
            parent.getChildren().add(light);
            install(light);
        }
    });
    private final WTextField editColor = new WTextField();


    private Light edited;

    public LightEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));

        //light pane -----------------------------------------------------------
        final Extents ext = editColor.getOverrideExtents();
        ext.setBest(new Extent.Double(200, 25));
        editColor.setOverrideExtents(ext);
        editColor.varText().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                try{
                    final Color rgba = Colors.fromHexa((Chars)pe.getNewValue());
                    ((Light)edited).setDiffuse(rgba);
                }catch(Exception ex){
                    Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                }
            }
        });

        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);
        final CheckGroup group = new CheckGroup();
        addChild(lblAmbiant,      FillConstraint.builder().coord(0,0).span(1,1).build());
        addChild(editAmbiant,     FillConstraint.builder().coord(1,0).span(1,1).build());
        addChild(lblDirectional,  FillConstraint.builder().coord(0,1).span(1,1).build());
        addChild(editDirectional, FillConstraint.builder().coord(1,1).span(1,1).build());
        addChild(lblPoint,        FillConstraint.builder().coord(0,2).span(1,1).build());
        addChild(editPoint,       FillConstraint.builder().coord(1,2).span(1,1).build());
        addChild(lblSpot,         FillConstraint.builder().coord(0,3).span(1,1).build());
        addChild(editSpot,        FillConstraint.builder().coord(1,3).span(1,1).build());
        addChild(editColor,       FillConstraint.builder().coord(0,4).span(1,1).build());
        group.add(editAmbiant);
        group.add(editDirectional);
        group.add(editPoint);
        group.add(editSpot);

    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof Light)) return null;
        edited = (Light) candidate;
        return this;
    }

    public void uninstall(Object candidate) {

    }

}
