

package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.model.NumberSliderModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.geometry.impl.MorphTarget;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class MorphEditor extends WContainer {

    private Sequence morphSet = null;

    public MorphEditor(){
        setLayout(new FormLayout());
    }

    public Sequence getMorphSet() {
        return morphSet;
    }

    public void setMorphSet(Sequence morphSet) {
        this.morphSet = morphSet;

        getChildren().removeAll();
        if (morphSet == null) return;

        //rebuild pane
        for (int i=0,n=morphSet.getSize();i<n;i++) {
            final MorphTarget target = (MorphTarget) morphSet.get(i);
            final WLabel lbl = new WLabel(target.getName());
            final Sequence seq = new ArraySequence();
            seq.add(target);
            final WMorphRatioEditor editor = new WMorphRatioEditor(seq);
            addChild(lbl, FillConstraint.builder().coord(0, i).build());
            addChild(editor, FillConstraint.builder().coord(1, i).build());
        }

    }

    public void setRoot(SceneNode root) {
        this.morphSet = null;

        getChildren().removeAll();
        if (root == null) return;

        final Dictionary dico = new HashDictionary();
        final NodeVisitor visitor = new NodeVisitor(){
            @Override
            public Object visit(Node node, Object context) {
                if (node instanceof Model) {
                    final Sequence ms = ((Mesh)((Model)node).getShape()).getMorphs();
                    if (ms != null) {
                        for(int i=0,n=ms.getSize();i<n;i++){
                            final MorphTarget target = (MorphTarget) ms.get(i);
                            Sequence seq = (Sequence) dico.getValue(target.getName());
                            if(seq==null){
                                seq = new ArraySequence();
                                dico.add(target.getName(), seq);
                            }
                            seq.add(target);
                        }
                    }
                }
                return super.visit(node, context);
            }
        };
        visitor.visit(root, null);

        //rebuild pane
        int i=0;
        for (Iterator ite=dico.getPairs().createIterator(); ite.hasNext();) {
            final Pair pair = (Pair) ite.next();
            final Chars name = (Chars) pair.getValue1();
            final Sequence seq = (Sequence) pair.getValue2();
            final WLabel lbl = new WLabel(name);
            final WMorphRatioEditor editor = new WMorphRatioEditor(seq);
            addChild(lbl, FillConstraint.builder().coord(0, i).build());
            addChild(editor, FillConstraint.builder().coord(1, i).build());
            i++;
        }

    }

    private static class WMorphRatioEditor extends WSlider{

        public WMorphRatioEditor(final Sequence targets) {
            super(new NumberSliderModel(Float.class,0, 1, 0));
            varValue().addListener(new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    for(int i=0,n=targets.getSize();i<n;i++){
                        ((MorphTarget)targets.get(i)).setFactor( ((Number)getValue()).floatValue() );
                    }
                }
            });
        }

    }


}
