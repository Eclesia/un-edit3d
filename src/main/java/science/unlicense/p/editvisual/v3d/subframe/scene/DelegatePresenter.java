
package science.unlicense.p.editvisual.v3d.subframe.scene;

import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.engine.ui.model.DefaultObjectPresenter;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.color.Color;

/**
 *
 * @author Johann Sorel
 */
public class DelegatePresenter extends DefaultObjectPresenter{

    public static final Color ICON_COLOR = Color.BLUE;

    private final Sequence presenters = new ArraySequence();

    public DelegatePresenter(Sequence presenters) {
        this.presenters.addAll(presenters);
    }



    public Widget createWidget(Object candidate) {
//        if(candidate instanceof TreeRowPath){
//            candidate = ((TreeRowPath)candidate).getLeaf();
//        }

        Widget presenter = null;
        for(int i=0,n=presenters.getSize();i<n && presenter==null;i++){
            presenter = ((ObjectPresenter)presenters.get(i)).createWidget(candidate);
        }
        if(presenter == null){
            presenter = super.createWidget(candidate);
        }
        return presenter;
    }

}
