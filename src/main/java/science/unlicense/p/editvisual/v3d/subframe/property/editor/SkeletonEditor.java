
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.ievent.ActionMessage;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.widget.WButton;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.menu.WMenuSwitch;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.Color;
import science.unlicense.p.editgraphic.editors.WSimilarityEditor;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.subframe.skeleton.RotateEditHandler;
import science.unlicense.p.editvisual.v3d.subframe.skeleton.SkeletonDebugLayer;
import science.unlicense.p.editvisual.v3d.subframe.skeleton.SkeletonDebugLayer.DebugState;
import science.unlicense.p.editvisual.v3d.subframe.skeleton.SkeletonUtils;
import science.unlicense.physics.api.skeleton.Skeleton;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonEditor extends WContainer implements ObjectEditor {

    private static final Image ICON_SELECT;
    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/format-text-italic-4.png")));
            ICON_SELECT = Images.read(Paths.resolve(new Chars("mod:/icons/16/edit-node.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

//    public static final NodeVisitor SET_DEBUG_ACTOR = new DefaultNodeVisitor() {
//        public Object visit(Node node, Object context) {
//            if(node instanceof Mesh){
//                final Mesh glNode = ((Mesh)node);
//                glNode.getExtShaderActors().add(new PickActor((PickResetPhase) context, glNode));
//            }
//            return super.visit(node, context);
//        }
//    };

    private static final Color ICON_COLOR = SystemStyle.getSystemColor(SystemStyle.COLOR_MAIN);

    private final WSpinner slider = new WSpinner(new NumberSpinnerModel(Double.class,0,null,null,1),0.1);

    private final WMenuSwitch viewSkeletonButton = new WMenuSwitch(new Chars("S"));
    private final WMenuSwitch viewPhysicButton = new WMenuSwitch(new Chars("P"));
    private final EventListener stateListener = new EventListener() {
            public void receiveEvent(Event event) {
                if(edited == null) return;
//                DebugState state = (DebugState) SkeletonDebugLayer.INSTANCE.getState(edited);
//                if(state == null){
//                    state = new DebugState(edited,((Number)slider.getValue()).floatValue());
//                    SkeletonDebugLayer.INSTANCE.putState(edited, state);
//
//                    final SceneNode node = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
//                    node.getUpdaters().add(new ConstraintUpdater(new CopyTransformConstraint(node, edited.getParent(), 1f)));
//                    node.getChildren().add(state.debugNode);
//
////                    node.accept(SET_DEBUG_ACTOR, SkeletonDebugLayer.INSTANCE.getPickingPhase());
//                    SkeletonDebugLayer.INSTANCE.getDebugScene().getChildren().add(node);
//                }
//
//                state.setSkeletonVisible(viewSkeletonButton.isCheck());
//                state.setPhysicVisible(viewPhysicButton.isCheck());
            }
        };

    private final WButton rotateButton = new WMenuSwitch(null,new WGraphicImage(ICON_SELECT),new EventListener() {
            public void receiveEvent(Event event) {
                Platform.get().setGestureHandler(new RotateEditHandler(SkeletonEditor.this));
            }
        });

    private final WSimilarityEditor trsEditor = new WSimilarityEditor();

    //current scene node
    private MotionModel edited;
    //it's skeleton if it has one
    private Skeleton skeleton;

    public SkeletonEditor() {
        super(new BorderLayout());

        final WButtonBar bar = new WButtonBar();
        bar.addChild(viewSkeletonButton,FillConstraint.builder().coord(0, 0).build());
        bar.addChild(viewPhysicButton,  FillConstraint.builder().coord(1, 0).build());
        bar.addChild(rotateButton,      FillConstraint.builder().coord(2, 0).build());
        bar.addChild(slider,            FillConstraint.builder().coord(3, 0).build());

        addChild(bar,       BorderConstraint.TOP);
        addChild(trsEditor, BorderConstraint.CENTER);

        trsEditor.varSimilarity().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(skeleton!=null){
                    skeleton.updateBindPose();
                }
            }
        });

        slider.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                SkeletonUtils.RESIZE_JOINT.visit(
                        SkeletonDebugLayer.INSTANCE.getDebugScene(),
                        ((Number)slider.getValue()).doubleValue());
            }
        });
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public void setEdited(MotionModel edited) {
        if(this.edited == edited) return;
        this.edited = null;

        trsEditor.setSimilarity(null);

        //remove state listener
        viewSkeletonButton.removeEventListener(ActionMessage.PREDICATE, stateListener);
        viewPhysicButton.removeEventListener(ActionMessage.PREDICATE, stateListener);

        if(edited!= null){
            final MotionModel mpm = (MotionModel) edited;
            skeleton = mpm.getSkeleton();

            final DebugState state = (DebugState) SkeletonDebugLayer.INSTANCE.getState(mpm);
            if(state!=null){
                viewSkeletonButton.setCheck(state.isSkeletonVisible());
                viewPhysicButton.setCheck(state.isPhysicVisible());
            }else{
                viewSkeletonButton.setCheck(false);
                viewPhysicButton.setCheck(false);
            }
        }

        //add state listener
        viewSkeletonButton.addEventListener(ActionMessage.PREDICATE, stateListener);
        viewPhysicButton.addEventListener(ActionMessage.PREDICATE, stateListener);

        this.edited = edited;
    }

    public SceneNode getEdited() {
        return edited;
    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof NamedNode)) return null;
        final Object value = ((NamedNode)candidate).getValue();
        if(!(value instanceof MotionModel)) return null;

        setEdited((MotionModel) value);
        return this;
    }

    public void uninstall(Object candidate) {
    }

}
