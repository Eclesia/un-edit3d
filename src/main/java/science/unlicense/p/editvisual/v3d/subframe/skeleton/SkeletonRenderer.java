

package science.unlicense.p.editvisual.v3d.subframe.skeleton;

import science.unlicense.model3d.impl.technique.AbstractTechnique;

/**
 *
 * @author Johann Sorel
 */
public class SkeletonRenderer extends AbstractTechnique {

//    public void renderInternal(RenderContext context, CameraMono camera, SceneNode node) {
//        final MeshGroup mesh = (MeshGroup) node;
//        final Skeleton skeleton = mesh.getSkeleton();
//
//        final Matrix matrix = node.calculateMVP(camera);
//
//        final GL gl = context.getGL();
//
////        gl.glDisable(GL4bc.GL_DEPTH_TEST);
////        gl.glDepthMask(false);
//        gl.asGL1().glColor3f(1.0f, 0.0f, 0.0f);
//        gl.asGL1().glLineWidth(3);
//
//        gl.asGL1().glBegin(GLC.GL_LINES);
//        final Sequence roots = skeleton.getChildren();
//        for(int i=0,n=roots.getSize();i<n;i++){
//            renderBone(gl, matrix, (Joint) roots.get(i));
//        }
//        gl.asGL1().glEnd();
//
////        gl.glDepthMask(true);
////        gl.glEnable(GL4bc.GL_DEPTH_TEST);
//    }
//
//    private void renderBone(GL gl, Matrix mvp, Joint bone){
//
//        final DefaultVector pv = toVector(bone, mvp);
//
//        final Node[] children = (Node[]) bone.getChildren().toArray(Node.class);;
//        for(int i=0;i<children.length;i++){
//            final Joint child = (Joint) children[i];
//            final DefaultVector cv = toVector(child, mvp);
//
//            gl.asGL1().glVertex3f((float)pv.getX(),(float)pv.getY(),(float)pv.getZ());
//            gl.asGL1().glVertex3f((float)cv.getX(),(float)cv.getY(),(float)cv.getZ());
//
//            System.out.println(pv);
//            System.out.println(cv);
//
//            renderBone(gl, mvp, child);
//        }
//
//
//    }
//
//    private static DefaultVector toVector(Joint joint, Matrix mvp){
//        DefaultVector v = new DefaultVector(0, 0, 0);
//        Affine m = joint.getRootToNodeSpace();
//        v = (DefaultVector) m.transform(v,v);
//        //v.setW(1);
//        v = (DefaultVector) mvp.transform(v);
//        return v;
//    }
//
//    public void dispose(GLProcessContext context) {
//    }

}
