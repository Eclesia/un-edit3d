

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;

/**
 *
 * @author Johann Sorel
 */
public class ExitAction extends AbstractActionExecutable {

    public ExitAction() {
        setText(new Chars("Exit"));
    }

    public Object perform() {
        System.exit(0);
        return null;
    }

}
