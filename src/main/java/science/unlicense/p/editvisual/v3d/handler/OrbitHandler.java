
package science.unlicense.p.editvisual.v3d.handler;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Vector;
import science.unlicense.math.impl.Vector3d;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;

/**
 * Orbit controler.
 *
 * @author Johann Sorel
 */
public class OrbitHandler implements GestureHandler, EventListener{

    private static float DISTANCE = 20;
    private static double ANGLE_HOR = Angles.degreeToRadian(30);
    private static double ANGLE_VER = Angles.degreeToRadian(25);
    private static Vector TARGET = new Vector3d(0, 0, 0);

    protected final Chars name;
    protected MonoCamera camera;
    protected SceneNode cameraTarget;
    protected OrbitUpdater controller;


    public OrbitHandler() {
        this(new Chars("Orbit navigation"));
    }

    public OrbitHandler(Chars name) {
        this.name = name;
    }

    public Chars getName() {
        return name;
    }

    public void install(Platform platform) {
        final Project project = platform.getProject();
        final SceneNode scene = project.getRoot();
        camera = project.getCamera();

        //restore position
        cameraTarget = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        cameraTarget.getNodeTransform().getTranslation().set(TARGET);
        cameraTarget.getNodeTransform().notifyChanged();
        scene.getChildren().add(cameraTarget);
        controller = new OrbitUpdater(platform,camera,cameraTarget).configureDefault();
        controller.setDistance(DISTANCE);
        controller.setHorizontalAngle(ANGLE_HOR);
        controller.setVerticalAngle(ANGLE_VER);
        cameraTarget.getChildren().add(camera);
        camera.getUpdaters().add(controller);

        //listen to mouse and keyboard events.
        platform.addEventListener(MouseMessage.PREDICATE, this);
        platform.addEventListener(KeyMessage.PREDICATE, this);
    }

    public void uninstall(Platform platform) {
        final Project project = platform.getProject();
        final SceneNode scene = project.getRoot();
        final MonoCamera camera = project.getCamera();

        camera.getUpdaters().remove(controller);
        platform.removeEventListener(MouseMessage.PREDICATE, controller);
        platform.removeEventListener(KeyMessage.PREDICATE, controller);
        scene.getChildren().remove(cameraTarget);

        //save parameters
        DISTANCE = controller.getDistance();
        ANGLE_HOR = controller.getHorizontalAngle();
        ANGLE_VER = controller.getVerticalAngle();
        TARGET = cameraTarget.getNodeTransform().getTranslation().copy();

        platform.removeEventListener(MouseMessage.PREDICATE, this);
        platform.removeEventListener(KeyMessage.PREDICATE, this);
    }

    public void receiveEvent(Event event) {

    }

}
