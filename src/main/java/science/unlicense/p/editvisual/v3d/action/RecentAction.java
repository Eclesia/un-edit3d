
package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.event.PropertyPredicate;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.widget.WPopup;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.p.editvisual.v3d.Loadable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class RecentAction extends WMenuDropDown implements Loadable {

    private static final Chars PROP_RECENTS = new Chars("recents");


    public RecentAction() {
        setText(new Chars("Recent"));
        setDropLocation(POPUP_RIGHT);
    }

    public void load(Platform platform) {
        //listen to projects opened
        platform.getProject().addEventListener(new PropertyPredicate(Project.PROPERTY_PROJECTPATH), new EventListener() {
            public void receiveEvent(Event event) {
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                saveRecents( new Chars(((Path)pe.getNewValue()).toURI()) );
            }
        });
    }

    public WPopup getDropdown() {
        final WPopup popup = super.getDropdown();
        popup.getChildren().removeAll();

        //update submenus
        final Sequence recents = getRecents();
        final Iterator ite = recents.createIterator();
        while(ite.hasNext()){
            final Chars v = (Chars) ite.next();
            final Path p = Paths.resolve(v);

            final WMenuButton button = new WMenuButton(v, null,new EventListener() {
                public void receiveEvent(Event event) {
                    Platform.get().getProject().openScene(p);
                }
            });
            popup.getChildren().add(button);
        }

        return popup;
    }

    private void saveRecents(Chars newRecent){
        final Sequence recents = getRecents();
        if(recents.contains(newRecent)){
            //just change the order
            recents.remove(newRecent);
            recents.add(newRecent);
        }else{
            recents.add(newRecent);
            if(recents.getSize()>10){
                //remove oldest
                recents.remove(0);
            }
        }
        final CharBuffer cb = new CharBuffer(CharEncodings.UTF_8);
        final Iterator ite = recents.createIterator();
        while(ite.hasNext()){
            final Chars v = (Chars) ite.next();
            cb.append(v).append('|');
        }
        Platform.get().getConfiguration().setProperty(PROP_RECENTS, cb.toChars());
        try {
            Platform.get().getConfiguration().save();
        } catch (IOException ex) {
            Loggers.get().log(ex, Logger.LEVEL_WARNING);
        }
    }

    /**
     * Get the last opened files.
     * @return Sequence of Path
     */
    private Sequence getRecents(){
        final Sequence recents = new ArraySequence();
        final Chars val = Platform.get().getConfiguration().getProperty(PROP_RECENTS);
        if(val!=null){
            final Chars[] parts = val.split('|');
            for(int i=0;i<parts.length;i++){
                final Chars p = parts[i].trim();
                if(p.isEmpty()) continue;
                recents.add(p);
            }
        }
        return recents;
    }


}
