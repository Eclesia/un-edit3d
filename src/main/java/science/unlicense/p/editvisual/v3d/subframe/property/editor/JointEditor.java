
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.CharArray;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTextField;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class JointEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/format-text-italic-4.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //properties common to all GL Nodes
    private final WLabel lblName = new WLabel(new Chars("name"));
    private final WTextField editName = new WTextField();

    private Joint edited;

    public JointEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        addChild(lblName,       FillConstraint.builder().coord(0,0).span(1,1).build());
        addChild(editName,      FillConstraint.builder().coord(1,0).span(2,1).build());

        editName.varText().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
                final PropertyMessage pe = (PropertyMessage) event.getMessage();
                edited.setTitle((CharArray)pe.getNewValue());
            }
        });

    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof Joint)) return null;
        edited = (Joint) candidate;
        editName.setText(edited.getTitle());
        return this;
    }

    public void uninstall(Object candidate) {

    }

}
