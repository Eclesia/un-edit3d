

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;

/**
 *
 * @author Johann Sorel
 */
public class SnapshotAction extends AbstractActionExecutable {

    public SnapshotAction() {
        setText(new Chars("Snapshot"));
    }

    public Object perform() {
        final Project project = Platform.get().getProject();

        //TODO

        return null;
    }

}
