
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class MultiPartMeshEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/im-user-offline.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //properties common to all GL Nodes
    private final WLabel lblLights = new WLabel(new Chars("lights off"));
    private final WCheckBox editLights = new WCheckBox();
    private final MorphEditor editMorph = new MorphEditor();
    private final WLabel lblDebugRigidBody = new WLabel(new Chars("Debug RigidBody"));
    private final WCheckBox editRigidBody = new WCheckBox();
    private final WLabel lblDebugSkeleton = new WLabel(new Chars("Debug Skeleton"));
    private final WCheckBox editSkeleton = new WCheckBox();
    private MotionModel edited;

    public MultiPartMeshEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        addChild(lblLights,             FillConstraint.builder().coord(0, 0).build());
        addChild(editLights,            FillConstraint.builder().coord(1, 0).build());
        addChild(lblDebugRigidBody,     FillConstraint.builder().coord(0, 1).build());
        addChild(editRigidBody,         FillConstraint.builder().coord(1, 1).build());
        addChild(lblDebugSkeleton,      FillConstraint.builder().coord(0, 2).build());
        addChild(editSkeleton,          FillConstraint.builder().coord(1, 2).build());
        addChild(editMorph,             FillConstraint.builder().coord(0, 3).span(3,1).build());

        editRigidBody.varCheck().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof GL2ES2DebugRigidBodyRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new GL2ES2DebugRigidBodyRenderer());
//                }
            }
        });

        editSkeleton.varCheck().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof GL2ES2DebugSkeletonRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new GL2ES2DebugSkeletonRenderer());
//                }
            }
        });

    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(!(candidate instanceof MotionModel)) return null;

        edited = (MotionModel) candidate;
        editMorph.setRoot(edited);

        return this;
    }

    public void uninstall(Object candidate) {

    }

}
