
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.component.bean.WBeanTable;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.concurrent.api.Paths;

/**
 *
 * @author Johann Sorel
 */
public class BeanEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/format-text-italic-4.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private final WBeanTable beanTable = new WBeanTable();

    public BeanEditor() {
        super(new BorderLayout());
        addChild(beanTable, BorderConstraint.CENTER);
    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if(candidate instanceof EventSource){
            beanTable.setBean(candidate);
        }

        return this;
    }

    public void uninstall(Object candidate) {

    }

}
