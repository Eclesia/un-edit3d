
package science.unlicense.p.editvisual.v3d.io;

import science.unlicense.binding.xml.FName;
import science.unlicense.binding.xml.XMLOutputStream;
import science.unlicense.binding.xml.dom.DomElement;
import science.unlicense.binding.xml.dom.DomNode;
import science.unlicense.binding.xml.dom.DomWriter;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.number.Int32;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.light.Light;
import science.unlicense.display.impl.light.PointLight;
import science.unlicense.display.impl.light.SpotLight;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.AbstractWriter;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.image.api.color.Colors;
import science.unlicense.p.editvisual.v3d.MeshRootNode;

/**
 *
 * @author Johann Sorel
 */
public class XMLSceneWriter extends AbstractWriter{

    private final DomWriter writer = new DomWriter();

    public void write(GraphicNode scene) throws IOException{

        final XMLOutputStream stream = new XMLOutputStream();
        stream.setOutput(output);
        stream.setEncoding(CharEncodings.UTF_8);
        stream.setIndent(new Chars("  "));
        writer.setOutput(stream);
        DomNode node = toDom(scene);
        writer.write(node);
        writer.dispose();
        stream.dispose();

    }

    private DomElement toDom(GraphicNode node){

        DomElement ele;
        if(node instanceof MeshRootNode){
            ele = new DomElement(new FName(null, XMLSceneConstants.MESH));
            fillBaseProperties(node, ele);
            final MeshRootNode mesh = (MeshRootNode) node;
            final DomElement domSource = new DomElement(new FName(null, XMLSceneConstants.MESH_SOURCE));
            domSource.setText(new Chars(mesh.getBase().toURI()));
            ele.getChildren().add(domSource);

            //do not encode children
            return ele;
        }else if(node instanceof Light){
            ele = new DomElement(new FName(null, XMLSceneConstants.LIGHT));
            fillBaseProperties(node, ele);

            final DomElement domColor = new DomElement(new FName(null, XMLSceneConstants.LIGHT_COLOR));
            domColor.setText(Colors.toHexa(((Light)node).getDiffuse()));

            final DomElement domType = new DomElement(new FName(null, XMLSceneConstants.LIGHT_TYPE));
            if(node instanceof DirectionalLight){
                domType.setText(XMLSceneConstants.LIGHT_TYPE_DIRECTIONAL);
            }else if(node instanceof PointLight){
                domType.setText(XMLSceneConstants.LIGHT_TYPE_POINT);
            }else if(node instanceof SpotLight){
                domType.setText(XMLSceneConstants.LIGHT_TYPE_SPOT);
            }else{
                domType.setText(XMLSceneConstants.LIGHT_TYPE_AMBIANT);
            }
            ele.getChildren().add(domColor);
            ele.getChildren().add(domType);

        }else{
            ele = new DomElement(new FName(null, XMLSceneConstants.NODE));
            fillBaseProperties(node, ele);
        }

        final Node[] children = (Node[]) node.getChildren().toArray(Node.class);
        for(int i=0;i<children.length;i++){
            DomElement candidate = toDom((GraphicNode)children[i]);
            if(candidate!=null){
                ele.getChildren().add(candidate);
            }
        }

        return ele;
    }

    private void fillBaseProperties(GraphicNode node, DomElement element){
        final DomElement domId = new DomElement(new FName(null, XMLSceneConstants.NODE_ID));
        final DomElement domName = new DomElement(new FName(null, XMLSceneConstants.NODE_NAME));
        final DomElement domVisible = new DomElement(new FName(null, XMLSceneConstants.NODE_VISIBLE));
        final DomElement domMatrix = new DomElement(new FName(null, XMLSceneConstants.NODE_MATRIX));

        domId.setText(Int32.encode((Integer)node.getId()));
        domName.setText(node.getTitle().toChars());
        domVisible.setText((node.isVisible())? new Chars("1") : new Chars("0"));

        final double[] array = node.getNodeTransform().toMatrix().toArrayDouble();
        final CharBuffer cb = new CharBuffer();
        for(double d : array){
            cb.append(new Chars(Double.toString(d)));
            cb.append(' ');
        }
        domMatrix.setText(cb.toChars().trim());

        element.getChildren().add(domId);
        element.getChildren().add(domName);
        element.getChildren().add(domVisible);
        element.getChildren().add(domMatrix);
    }

}
