
package science.unlicense.p.editvisual.v3d.subframe.property;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Pair;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSlider;
import science.unlicense.engine.ui.widget.WTabContainer;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.p.editvisual.v3d.Loadable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.subframe.property.editor.ObjectEditor;

/**
 *
 * @author Johann Sorel
 */
public class WPropertyPane extends WContainer implements Loadable{

    public static final Chars PROPERTY_VALUE = WSlider.PROPERTY_VALUE;

    private final WTabContainer tabs = new WTabContainer();
    private Sequence editors;

    //currently active editors
    private final Sequence pairs = new ArraySequence();

    public WPropertyPane() {
        super(new BorderLayout());
        tabs.setTabPosition(WTabContainer.TAB_POSITION_LEFT);
        addChild(tabs, BorderConstraint.CENTER);
    }

    public Object getValue(){
        return getPropertyValue(PROPERTY_VALUE);
    }

    public void setValue(Object value){
        Object old = getValue();
        if(setPropertyValue(PROPERTY_VALUE, value)){
            for(int i=pairs.getSize()-1;i>=0;i--){
                final Pair p = (Pair) pairs.get(i);
                ((ObjectEditor)p.getValue1()).uninstall(old);
            }
            pairs.removeAll();
            tabs.removeTabs();

            getEditors();
            for(int i=0,n=editors.getSize();i<n;i++){
                final ObjectEditor editor = ((ObjectEditor)editors.get(i));
                final Widget w = editor.install(value);
                if(w!=null){
                    pairs.add(new Pair(editor, w));
                    final WLabel lbl = new WLabel(null,new WGraphicImage(editor.getIcon()));
                    tabs.addTab(w, lbl);
                }
            }
        }
    }

    public Variable varValue() {
        return getProperty(PROPERTY_VALUE);
    }

    private Sequence getEditors(){
        if(editors!=null) return editors;


        editors = new ArraySequence();
        final NamedNode root = science.unlicense.runtime.Runtime.system().getModuleManager()
                .getMetadataRoot("platform").search(new Chars("unp/platform/subframe/properties/editors"));
        if(root!=null){
            final Node[] children = (Node[]) root.getChildren().toArray(Node.class);
            for(int i=0;i<children.length;i++){
                editors.add( ((NamedNode)children[i]).getValue());
            }
        }
        return editors;
    }


    public void load(final Platform platform) {
        varValue().sync(platform.getProject().varActiveObject());
    }

}
