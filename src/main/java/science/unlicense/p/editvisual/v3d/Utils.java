
package science.unlicense.p.editvisual.v3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.p.editgraphic.IconStore;

/**
 *
 * @author Johann Sorel
 */
public class Utils {

    public static final IconStore ICONS;
    static {
        try {
            ICONS = new IconStore(Paths.resolve(new Chars("mod:/style/Unlicense-Shapes.ttf")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Build a grid mesh. standard ground for demos.
     * @return
     */
    public static Model buildGround(){
        final FloatSequence vertices = new FloatSequence();
        final FloatSequence normals = new FloatSequence();
        final IntSequence indexes = new IntSequence();

        final float[] normal = new float[]{0,0,1};
        int i=0;
        for(int x=-50;x<50;x++){
            vertices.put(x).put(0).put(-50);
            vertices.put(x).put(0).put(+50);
            normals.put(normal);
            normals.put(normal);
            indexes.put(i++);
            indexes.put(i++);
        }
        for(int z=-50;z<50;z++){
            vertices.put(-50).put(0).put(z);
            vertices.put(+50).put(0).put(z);
            normals.put(normal);
            normals.put(normal);
            indexes.put(i++);
            indexes.put(i++);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices.toArrayFloat(), 3));
        shell.setNormals(new VBO(normals.toArrayFloat(), 3));
        shell.setIndex(new IBO(indexes.toArrayInt()));
        shell.setRanges(new IndexedRange[]{IndexedRange.LINES(0, indexes.getSize())});

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.GRAY_DARK);

        final DefaultModel mesh = new DefaultModel();
        mesh.getTechniques().add(new SimpleBlinnPhong());
        mesh.setShape(shell);
        mesh.getMaterials().add(material);

        return mesh;
    }

}
