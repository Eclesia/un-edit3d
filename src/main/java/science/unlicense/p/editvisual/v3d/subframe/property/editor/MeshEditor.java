
package science.unlicense.p.editvisual.v3d.subframe.property.editor;

import science.unlicense.common.api.character.Chars;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.geometry.impl.Mesh;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class MeshEditor extends WContainer implements ObjectEditor {

    private static final Image ICON;
    static {
        try {
            ICON = Images.read(Paths.resolve(new Chars("mod:/icons/im-user-offline.png")));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    //properties common to all GL Nodes
    private final WLabel lblLights = new WLabel(new Chars("lights off"));
    private final WCheckBox editLights = new WCheckBox();
    private final WLabel lblDebugNormal = new WLabel(new Chars("Debug normal"));
    private final WLabel lblDebugTangent = new WLabel(new Chars("Debug tangent"));
    private final WLabel lblDebugFace = new WLabel(new Chars("Debug face"));
    private final WLabel lblDebugWireframe = new WLabel(new Chars("Debug wireframe"));
    private final WLabel lblDebugBbox = new WLabel(new Chars("Debug bbox"));
    private final WLabel lblDebugAdjency = new WLabel(new Chars("Debug adjency"));
    private final WLabel lblDebugRigidBody = new WLabel(new Chars("Debug RigidBody"));
    private final WLabel lblOutline = new WLabel(new Chars("Outline"));
    private final WCheckBox editDebugNormal = new WCheckBox();
    private final WCheckBox editDebugTangent = new WCheckBox();
    private final WSpinner editNormalLength = new WSpinner(new NumberSpinnerModel(Double.class,0,null,null,1),1.0);
    private final WCheckBox editDebugFace = new WCheckBox();
    private final WCheckBox editDebugWireframe = new WCheckBox();
    private final WCheckBox editDebugBbox = new WCheckBox();
    private final WCheckBox editDebugAdjency = new WCheckBox();
    private final WCheckBox editRigidBody = new WCheckBox();
    private final WCheckBox editOutline = new WCheckBox();
    private final MorphEditor editMorph = new MorphEditor();

    private Model edited;

    public MeshEditor() {
        super(new FormLayout());
        getStyle().getSelfRule().setProperty(new Chars("margin"), new Chars("5"));
        ((FormLayout)getLayout()).setColumnSize(2, FormLayout.SIZE_EXPAND);

        //build common panel ---------------------------------------------------
        int y=0;
        addChild(lblLights,             FillConstraint.builder().coord(0,   y).build());
        addChild(editLights,            FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugNormal,        FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugNormal,       FillConstraint.builder().coord(1,   y).build());
        addChild(editNormalLength,      FillConstraint.builder().coord(2,   y).build());
        addChild(lblDebugTangent,       FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugTangent,      FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugFace,          FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugFace,         FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugWireframe,     FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugWireframe,    FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugBbox,          FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugBbox,         FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugAdjency,       FillConstraint.builder().coord(0, ++y).build());
        addChild(editDebugAdjency,      FillConstraint.builder().coord(1,   y).build());
        addChild(lblDebugRigidBody,     FillConstraint.builder().coord(0, ++y).build());
        addChild(editRigidBody,         FillConstraint.builder().coord(1,   y).build());
        addChild(lblOutline,            FillConstraint.builder().coord(0, ++y).build());
        addChild(editOutline,           FillConstraint.builder().coord(1,   y).build());
        addChild(editMorph,             FillConstraint.builder().coord(0, ++y).span(3,1).build());

//        editLights.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                //edited.getMaterial().setLightVulnerable((Boolean)pe.getNewValue());
//            }
//        });
//        editDebugNormal.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof DebugNormalRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new DebugNormalRenderer( ((Number)editNormalLength.getValue()).floatValue() ));
//                }
//            }
//        });
//        editDebugTangent.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof DebugTangentRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new DebugTangentRenderer( ((Number)editNormalLength.getValue()).floatValue() ));
//                }
//            }
//        });
//        editDebugFace.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof DebugFaceNormalRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new DebugFaceNormalRenderer( ((Number)editNormalLength.getValue()).floatValue() ));
//                }
//            }
//        });
//        editDebugWireframe.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                if((Boolean)pe.getNewValue()){
//                    ((MeshRenderer)edited.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.FILL);
//                }else{
//                    ((MeshRenderer)edited.getRenderers().get(0)).getState().setPolygonMode(GLC.POLYGON_MODE.LINE);
//                }
//            }
//        });
//        editDebugBbox.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof DebugBBoxRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new DebugBBoxRenderer());
//                }
//            }
//        });
//        editDebugAdjency.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof DebugAdjencyRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new DebugAdjencyRenderer());
//                }
//            }
//        });
//        editRigidBody.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof GL2DebugRigidBodyRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new GL2DebugRigidBodyRenderer());
//                }
//            }
//        });
//        editOutline.varCheck().addListener(new EventListener() {
//            public void receiveEvent(Event event) {
//                if(edited==null) return;
//                final PropertyMessage pe = (PropertyMessage) event.getMessage();
//                final boolean selected = (Boolean)pe.getNewValue();
//                final Sequence seq = edited.getRenderers();
//                boolean found = false;
//                for(int i=seq.getSize()-1;i>=0;i--){
//                    if(seq.get(i) instanceof SilhouetteRenderer){
//                        found = true;
//                        if(!selected){
//                            seq.remove(i);
//                        }
//                    }
//                }
//                if(!found && selected){
//                    seq.add(new SilhouetteRenderer(edited, Color.BLACK, 1));
//                }
//            }
//        });
    }

    public Image getIcon() {
        return ICON;
    }

    public Widget install(Object candidate) {
        if (!(candidate instanceof Model)) return null;

        edited = (Model) candidate;
        editMorph.setMorphSet(((Mesh)edited.getShape()).getMorphs());

        return this;
    }

    public void uninstall(Object candidate) {

    }

}
