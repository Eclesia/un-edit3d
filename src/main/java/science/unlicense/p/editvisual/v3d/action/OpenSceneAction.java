

package science.unlicense.p.editvisual.v3d.action;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.p.editvisual.v3d.Platform;
import science.unlicense.p.editvisual.v3d.Project;

/**
 *
 * @author Johann Sorel
 */
public class OpenSceneAction extends AbstractActionExecutable {

    public OpenSceneAction() {
        setText(new Chars("Open"));
    }

    public Object perform() {
        new Thread(){
            @Override
            public void run() {
                final Path p = WPathChooser.showOpenDialog((Predicate)null);
                if(p!=null){
                    final Project project = Platform.get().getProject();
                    project.openScene(p);
                }
            }
        }.start();
        return null;
    }

}
