

package science.unlicense.p.editvisual.v3d;

import javax.swing.JOptionPane;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.number.Float64;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.impl.io.ur.URLUtilities;
import science.unlicense.engine.ui.component.path.PathPresenters;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;
import science.unlicense.image.api.color.ColorRGB;
import science.unlicense.math.api.Angles;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Vector3d;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;
import science.unlicense.model3d.impl.physic.SkeletonPoseResolver;
import science.unlicense.model3d.impl.physic.Skeletons;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class Runner {

    private static final Chars ARG_MODEL = new Chars("-model");
    private static final Chars ARG_THUMB_OUT = new Chars("-thumb-out");
    private static final Chars ARG_THUMB_SIZE = new Chars("-thumb-size");
    private static final Chars ARG_POSE = new Chars("-pose");
    private static final Chars ARG_ANIM = new Chars("-anim");

    public static void main(String[] args) throws IOException {

        //change global application theme
        WStyle style = RSReader.readStyle(Paths.resolve(SystemStyle.THEME_DARK));
        WidgetStyles.mergeDoc(
                SystemStyle.INSTANCE.getRule(SystemStyle.RULE_SYSTEM),
                style.getRule(SystemStyle.RULE_SYSTEM), false);
        SystemStyle.INSTANCE.notifyChanged();


        //interpret commands
        final Dictionary dico = new HashDictionary();
        if(args.length>1){
            for(int i=0;i<args.length;i+=2){
                final String key = args[i];
                final String value = args[i+1];
                dico.add(new Chars(key), new Chars(value));
            }
        }else if(args.length==1){
            //handle it as a single model
            dico.add(ARG_MODEL,new Chars(args[0]));
        }

        Chars path = (Chars) dico.getValue(ARG_MODEL);
        if(path!=null && path.startsWith(new Chars("file://"))){
            //some desktop managers add the prefix as urls
            path = path.truncate(7, path.getCharLength());
            path = URLUtilities.decode(path);
            dico.add(ARG_MODEL, path);
        }

        final Chars thumbOut = (Chars) dico.getValue(ARG_THUMB_OUT);
        final Chars thumbSize = (Chars) dico.getValue(ARG_THUMB_SIZE);
        if(thumbOut!=null){
            //do not run application, generate a thumbnail
            final Chars modelPath = (Chars) dico.getValue(ARG_MODEL);
            final Path p = Paths.resolve(modelPath);
            final Path thumbPath = Paths.resolve(thumbOut);
            double size = 256;
            if(thumbSize!=null) size = Float64.decode(new Chars(thumbSize));

            final Image thumb = PathPresenters.generateImage(p, new Extent.Double(size, size));
            Images.write(thumb, new Chars("bmp"), thumbPath);
            System.exit(0);
        }

        final Platform platform = Platform.get();

        //load model
        final Chars modelPath = (Chars) dico.getValue(ARG_MODEL);
        final Project project = platform.getProject();
        MeshRootNode meshRootNode = null;
        if(modelPath!=null){
            final Path p = Paths.resolve(modelPath);
            try{
                if(Model3Ds.canDecode(p)){
                    final Store store = Model3Ds.open(p);
                    meshRootNode = new MeshRootNode(p);
                    final Iterator ite = ((SceneResource)store).getElements().createIterator();
                    while(ite.hasNext()){
                        final Object obj = ite.next();
                        if(obj instanceof SceneNode){
                            Project.prepareNode((Node)obj);
                            meshRootNode.getChildren().add(obj);
                        }
                    }
                    final GraphicNode sceneRoot = project.getRoot();

                    final AmbientLight ambiantLight = new AmbientLight();
                    ambiantLight.setDiffuse(new ColorRGB(0.2f, 0.2f, 0.2f));
                    sceneRoot.getChildren().add(ambiantLight);

                    final DirectionalLight pl = new DirectionalLight();
                    pl.getNodeTransform().getRotation().set(Matrix3x3.createRotation3(Angles.degreeToRadian(-70), new Vector3d(1, 0, 0)));
                    pl.getNodeTransform().getTranslation().setXYZ(0, 50, 0);
                    pl.getNodeTransform().notifyChanged();
                    sceneRoot.getChildren().add(pl);

                    Project.prepareNode((Node)meshRootNode);
                    sceneRoot.getChildren().add(meshRootNode);
                }
            }catch(Exception ex){
                ex.printStackTrace();
                JOptionPane.showInputDialog(null, "ERROR"+ex.getMessage());
            }
        }

        //load pose
        final Chars posePath = (Chars) dico.getValue(ARG_POSE);
        if(meshRootNode!=null && posePath!=null){
            final Path p = Paths.resolve(posePath);
            try{
                final MotionModel mpm = (MotionModel) meshRootNode.getChildren().get(0);
                final Store motionStore = Model3Ds.open(p);
                final RelativeSkeletonPose pose = (RelativeSkeletonPose) ((SceneResource)motionStore).getElements().createIterator().next();
                SkeletonPoseResolver.resolve1(mpm.getSkeleton(), pose,null);
                mpm.getSkeleton().solveKinematics();
                mpm.getSkeleton().updateBindPose();
                project.setActiveObject(mpm);
            }catch(Exception ex){
                ex.printStackTrace();
                JOptionPane.showInputDialog(null, ex.getMessage());
            }
        }

        //load animation
        final Chars animPath = (Chars) dico.getValue(ARG_ANIM);
        if(meshRootNode!=null && animPath!=null){
            final Path p = Paths.resolve(animPath);
            try{
                final MotionModel mpm = (MotionModel) meshRootNode.getChildren().get(0);
                final Store motionStore = Model3Ds.open(p);
                final Animation animation = (Animation) ((SceneResource)motionStore).getElements().createIterator().next();
                Skeletons.mapAnimation(animation, mpm.getSkeleton(), mpm,null);
                mpm.getUpdaters().add(animation);
                animation.start();
                project.setActiveObject(mpm);
            }catch(Exception ex){
                ex.printStackTrace();
                JOptionPane.showInputDialog(null, ex.getMessage());
            }
        }

        //generate thumbnail

    }

}
