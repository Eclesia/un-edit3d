
package science.unlicense.p.editvisual.v3d.subframe.motion;

import science.unlicense.common.api.collection.HashSet;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.collection.Set;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.Painter2D;
import science.unlicense.engine.ui.visual.WidgetView;
import science.unlicense.engine.ui.widget.WLeaf;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.Line;
import science.unlicense.geometry.impl.Circle;
import science.unlicense.geometry.impl.DefaultLine;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.Matrix4x4;
import science.unlicense.math.impl.Vector4d;
import science.unlicense.physics.api.skeleton.Joint;
import science.unlicense.physics.api.skeleton.Skeleton;

/**
 * 2D Skeleton view. can be used for debugging.
 *
 * @author Johann Sorel
 */
public class WSkeletonView2D extends WLeaf{

    public static final int CUT_XY = 0;
    public static final int CUT_XZ = 1;
    public static final int CUT_YZ = 2;

    private Skeleton skeleton;
    private int cut = 0;
    private double scale = 1;

    //for rendering
    private final Vector4d tuple = new Vector4d();
    private final Circle c = new Circle(0, 0, 5);
    private final Matrix4x4 adjust = new Matrix4x4();

    /**
     *
     * @param skeleton skeleton to render.
     * @param cut Cut plan, on of CUT_X.
     */
    public WSkeletonView2D(int cut) {
        this.cut = cut;
        setView(new WidgetView(this){
            protected void renderSelf(Painter2D painter, BBox innerBBox) {
                super.renderSelf(painter, innerBBox);
                WSkeletonView2D.this.renderSelf(painter, innerBBox);
            }
        });
    }

    public void setSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    private void renderSelf(Painter2D painter, BBox innerBBox) {
        final Extent extent = getEffectiveExtent();
        final double width = extent.get(0);
        final double height = extent.get(1);

        adjust.set(1, 3, height);
        adjust.set(0, 0, scale);
        adjust.set(1, 1, -1*scale);
        adjust.set(2, 2, scale);

        if(cut == CUT_XY){
            adjust.set(0, 3, width/2);
        }else{
            adjust.set(2, 3, width/2);
        }



        c.setCenter(tuple);
        painter.setPaint(new ColorPaint(Color.RED));

        if(skeleton!=null){
            final Sequence joints = skeleton.getAllJoints();
            final Set visited = new HashSet();
            for(int i=0,n=joints.getSize(); i<n; i++){
                final Joint joint = (Joint) joints.get(i);
                renderJoint(painter, joint, null, visited);
            }
        }
    }

    private void renderJoint(Painter2D painter, Joint joint, TupleRW parent, Set visited){

        final Matrix m = joint.getNodeTransform().viewMatrix();
        tuple.setAll(0);
        tuple.set(3, 1);
        m.transform(tuple, tuple);
        tuple.set(3, 1);
        adjust.transform(tuple, tuple);

        if(cut == CUT_YZ){
            tuple.setX(tuple.getZ());
        }else if(cut == CUT_XZ){
            tuple.setY(tuple.getZ());
        }

        //render bone line segment
        if(parent!=null){
            final Line line = new DefaultLine(parent, tuple);
            painter.stroke(line);
        }

        if(visited.contains(joint)){
            //we test this after rendering the parent relation since we don't
            //know in which order joints will come
            return;
        }

        painter.fill(c);
        visited.add(joint);

        //render children
        final Sequence jc = joint.getChildren();
        for(int i=0,n=jc.getSize(); i<n; i++){
            final Joint child = (Joint) jc.get(i);
            renderJoint(painter, child, tuple.copy(), visited);
        }
    }


    public void receiveEventParent(Event event) {

        //catch mouse wheel event for scaling
        EventMessage message = event.getMessage();
        if(message instanceof MouseMessage){
            final MouseMessage me = (MouseMessage) message;
            if(me.getType() == MouseMessage.TYPE_WHEEL){
                if(me.getWheelOffset()<0){
                    final double scale = Math.pow(0.9, -me.getWheelOffset());
                    this.scale *= scale;
                }else if(me.getWheelOffset()>0){
                    final double scale = Math.pow(1.1, me.getWheelOffset());
                    this.scale *= scale;
                }
                setDirty();
                me.consume();
                return;

            }
        }

        //forward mouse and key event to listeners if any.
        super.receiveEventParent(event);
    }

}
