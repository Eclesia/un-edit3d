

package science.unlicense.p.editvisual.v3d;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.country.Country;
import science.unlicense.common.api.model.tree.NamedNode;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.impl.store.keyvalue.KeyValueStore;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.game.phase.GamePhases;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.ievent.ActionExecutable;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.engine.ui.widget.frame.WDesktop;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.gpu.api.opengl.GLBindings;
import science.unlicense.math.impl.Vector2d;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.p.editvisual.v3d.handler.GestureHandler;
import science.unlicense.p.editvisual.v3d.handler.OrbitHandler;

/**
 *
 * @author Johann Sorel
 */
public final class Platform extends GLUIFrame{

    public static final Chars FRAME_X = new Chars("x");
    public static final Chars FRAME_Y = new Chars("y");
    public static final Chars FRAME_WIDTH = new Chars("width");
    public static final Chars FRAME_HEIGHT = new Chars("height");
    public static final Chars FRAME_TITLE = new Chars("title");

    private static  final Platform INSTANCE = new Platform();

    public static Platform get() {
        return INSTANCE;
    }


    private final KeyValueStore configProperties;
    private final Project project = new Project();
    private final WContainer mainPane;
    private final WButtonBar menuPane = new WButtonBar();
    private final WDesktop framePane = new WDesktop();

    //gesture handler
    private final WLabel handlerLabel = new WLabel();
    private GestureHandler handler;

    //picking
    private final GamePhases phases = new GamePhases();

    private Platform() {
        super(null,GLBindings.getBindings()[0].createFrame(true,null));
        final int width = 1400;
        final int height = 1000;
        setSize(width, height);
        setTitle(new Chars("Scene editor"));
        setVisible(true);

        mainPane = getContainer();
        mainPane.setLanguage(Country.GBR.asLanguage(), true);
        mainPane.setLayout(new BorderLayout());
        mainPane.addChild(menuPane, BorderConstraint.TOP);
        mainPane.addChild(framePane, BorderConstraint.CENTER);
        mainPane.getStyle().getSelfRule().setProperties(new Chars("background:none"));

        //load the application properties
        try{
            final NamedNode userHome = science.unlicense.runtime.Runtime.system().getProperties()
                    .getSystemTree().search(new Chars("system/user/home"));
            Path path = Paths.resolve((Chars)userHome.getValue());
            path = path.resolve(new Chars(".unedit3d"));
            path.createContainer();
            path = path.resolve(new Chars("config.properties"));
            configProperties = new KeyValueStore(path).load();
        }catch(IOException ex){
            throw new RuntimeException(ex);
        }

        //build menu bar
        int index = buildMenu();
        menuPane.addChild(new WLabel(new Chars("Tool")), FillConstraint.builder().coord(index++, 0).fill(false, true).build());
        menuPane.addChild(handlerLabel, FillConstraint.builder().coord(index++, 0).fill(false, true).build());

        //build frames panel
        buildFrames();


        //initialize an empty scene and project
        final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.CARTESIAN3D_METRIC_RIGH_HANDED);
        final Model ground = Utils.buildGround();
        final MotionModel marker = DefaultMotionModel.createCrosshair3D();
        final MonoCamera camera = new MonoCamera();
        camera.setFarPlane(700);

        scene.getChildren().add(ground);
        scene.getChildren().add(marker);
        scene.getChildren().add(camera);

        project.setContext(getContext());
        project.setCamera(camera);
        scene.getChildren().add(project.getRoot());

        phases.setDeferredLight(false);
        phases.setBloomEnable(false);
        phases.setSsaoEnable(false);
        phases.setDofEnable(false);
        phases.setCamera(camera);
        phases.setScene(scene);
        phases.getPhysicPhase().setEnable(false);
        getContext().getPhases().add(phases);

        initModules();

        //activate default gesture handler
        setGestureHandler(new OrbitHandler());


//        new WContextDebugFrame(getContext());
    }

    public KeyValueStore getConfiguration() {
        return configProperties;
    }

    public Project getProject() {
        return project;
    }

    public void setGestureHandler(GestureHandler handler){
        if(this.handler!=null){
            this.handler.uninstall(this);
        }
        this.handler = handler;
        if(this.handler!=null){
            handlerLabel.setText(handler.getName());
            this.handler.install(this);
        }else{
            handlerLabel.setText(Chars.EMPTY);
        }
    }

    private int buildMenu(){
        final NamedNode root = science.unlicense.runtime.Runtime.system().getModuleManager()
                .getMetadataRoot("platform").search(new Chars("unp/platform/menu"));
        if(root==null) return 0;

        int index = 0;
        final Node[] children = (Node[]) root.getChildren().toArray(Node.class);
        for(int i=0;i<children.length;i++){
            final Widget w = buildMenu(children[i]);
            if(w!=null){
                menuPane.addChild(w,  FillConstraint.builder().coord(index++, 0).fill(false, true).build());
            }
        }

        return index;
    }

    private Widget buildMenu(final Node candidate){
        if(!(candidate instanceof NamedNode)){
            return null;
        }

        final NamedNode nnode = (NamedNode)candidate;

        if(nnode.getValue() instanceof Chars){
            final WMenuDropDown dropdown = new WMenuDropDown();
            dropdown.setText((Chars)nnode.getValue());

            final Node[] children = (Node[]) candidate.getChildren().toArray(Node.class);
            for(int i=0;i<children.length;i++){
                final Widget w = buildMenu(children[i]);
                if(w!=null){
                    dropdown.getDropdown().getChildren().add(w);
                }
            }

            return dropdown;
        }else if(nnode.getValue() instanceof ActionExecutable){
            final ActionExecutable exec = (ActionExecutable) nnode.getValue();
            init(exec);
            return new WMenuButton(exec);
        }else if(nnode.getValue() instanceof WMenuButton){
            final WMenuButton button = (WMenuButton) nnode.getValue();
            init(button);
            return button;
        }else{
            return null;
        }
    }

    private void buildFrames(){
        final NamedNode root = science.unlicense.runtime.Runtime.system().getModuleManager()
                .getMetadataRoot("platform").search(new Chars("unp/platform/subframe"));
        if(root==null) return;

        final Node[] children = (Node[]) root.getChildren().toArray(Node.class);
        for(int i=0;i<children.length;i++){
            if(!(children[i] instanceof NamedNode)) continue;
            final NamedNode nnode = (NamedNode) children[i];

            if(nnode.getValue() instanceof Widget){
                final Widget widget = (Widget) nnode.getValue();
                init(widget);
                final UIFrame s1 = framePane.addFrame();
                s1.getContainer().setLayout(new BorderLayout());
                s1.getContainer().addChild(widget,BorderConstraint.CENTER);

                Chars title = Chars.EMPTY;
                double x = 5;
                double y = 20;
                double width = 250;
                double height = 300;
                for(Object n : nnode.getChildren().toArray()){
                    if(((NamedNode)n).getName().equals(FRAME_X))x = (Double)((NamedNode)n).getValue();
                    if(((NamedNode)n).getName().equals(FRAME_Y))y = (Double)((NamedNode)n).getValue();
                    if(((NamedNode)n).getName().equals(FRAME_WIDTH))width = (Double)((NamedNode)n).getValue();
                    if(((NamedNode)n).getName().equals(FRAME_HEIGHT))height = (Double)((NamedNode)n).getValue();
                    if(((NamedNode)n).getName().equals(FRAME_TITLE))title = (Chars)((NamedNode)n).getValue();
                }
                s1.setTitle(title);
                s1.setSize((int)width, (int)height);
                s1.setOnScreenLocation(new Vector2d(x, y));
            }
        }
    }

    private void initModules(){
        final NamedNode root = science.unlicense.runtime.Runtime.system().getModuleManager()
                .getMetadataRoot("platform").search(new Chars("unp/platform/listener"));
        if(root==null) return;

        final Node[] children = (Node[]) root.getChildren().toArray(Node.class);;
        for(int i=0;i<children.length;i++){
            if(!(children[i] instanceof NamedNode)) continue;
            final NamedNode nnode = (NamedNode) children[i];
            init(nnode.getValue());
        }
    }

    private void init(Object candidate){
        if(candidate instanceof Loadable){
            ((Loadable)candidate).load(this);
        }
    }

}
