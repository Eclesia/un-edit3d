
package science.unlicense.p.editvisual.v2d;

import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.geometry.api.system.CoordinateSystems;


/**
 *
 * @author eclesia
 */
public class WCanvas {

    private final SceneNode scene = new DefaultSceneNode(CoordinateSystems.undefined(2));

}
