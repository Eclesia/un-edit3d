

package science.unlicense.p.editvisual.v2d;

import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.display.api.desktop.FrameManagers;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.component.path.WPathChooser;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WGraphicImage;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;


/**
 * Tool to edit images.
 *
 * TODO
 *
 * @author Johann Sorel
 */
public class ImageEditor {

    public static void main(String[] args) throws IOException {



        final UIFrame frame = (UIFrame) FrameManagers.getFrameManager().createFrame(false);
        frame.setSize(1024, 768);
        final WContainer container = frame.getContainer();
        container.setLayout(new BorderLayout());

        final WButtonBar bar = new WButtonBar();
        final WLabel lbl = new WLabel();

        new Thread(){
            public void run() {
                try {
                    final Path p = WPathChooser.showOpenDialog((Predicate)null);
                    final Image image = Images.read(p);
                    lbl.setGraphic(new WGraphicImage(image));
                } catch (IOException ex) {
                    Loggers.get().warning(ex);
                }
            }

        }.start();


        container.addChild(lbl,BorderConstraint.CENTER);

        frame.setVisible(true);


    }

}
