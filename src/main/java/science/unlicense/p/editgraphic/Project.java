
package science.unlicense.p.editgraphic;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.AbstractEventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class Project extends AbstractEventSource{

    private static final Project INSTANCE = new Project();
    public static final Chars PROPERTY_ACTIVEOBJECT = new Chars("ActiveObject");

    private final GraphicNode root = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private Object activeObject = null;

    public static Project get() {
        return INSTANCE;
    }

    public GraphicNode getRoot() {
        return root;
    }

    public Object getActiveObject() {
        return activeObject;
    }

    public void setActiveObject(Object activeNode) {
        if(CObjects.equals(this.activeObject,activeNode)) return;
        Object old = this.activeObject;
        this.activeObject = activeNode;
        getEventManager().sendPropertyEvent(this, PROPERTY_ACTIVEOBJECT, old, activeNode);
    }

    public Property varActiveObject(){
        return getProperty(PROPERTY_ACTIVEOBJECT);
    }

}
