
package science.unlicense.p.editgraphic;

import science.unlicense.display.api.painter2d.ColorPaint;
import science.unlicense.display.api.painter2d.ImagePainter2D;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.engine.software.CPUPainter2D;
import science.unlicense.format.ttf.ttf.TTFFontMetadata;
import science.unlicense.format.ttf.ttf.TTFReader;
import science.unlicense.format.ttf.ttf.TrueTypeFont;
import science.unlicense.geometry.api.PlanarGeometry;
import science.unlicense.geometry.impl.TransformedGeometry;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.color.Color;
import science.unlicense.image.impl.process.geometric.FlipVerticalOperator;
import science.unlicense.math.impl.Matrix4x4;

/**
 *
 * @author Johann Sorel
 */
public class IconStore {

    private final Path path;
    private final TrueTypeFont font;
    private final TTFFontMetadata meta;

    public IconStore(Path path) throws IOException {
        this.path = path;
        final TTFReader reader = new TTFReader();
        reader.setInput(path);
        font = reader.read();
        meta = new TTFFontMetadata(font);
    }

    public Image render(int unicode, Color color, int width, int height){
        return render(new int[]{unicode}, color, width, height);
    }

    public Image render(int[] unicode, Color color, int width, int height){
        final ImagePainter2D painter = new CPUPainter2D(width, height);

        final double scale = ((double)height)/meta.getAscent();
        final Matrix4x4 trs = new Matrix4x4();
        trs.set(0, 0, scale);
        trs.set(1, 1, scale);

        painter.setPaint(new ColorPaint(color));

        for(int i=0;i<unicode.length;i++){
            PlanarGeometry geom = font.getGlyph(unicode[i]);
            geom = new TransformedGeometry.T2D(geom, trs);
            painter.fill(geom);
        }

        final Image image = painter.getImage();
        new FlipVerticalOperator().execute(image);

        //release opengl painter
        painter.dispose();

        return image;
    }

    public static Image render(PlanarGeometry geom, Color color, int width, int height){
        final ImagePainter2D painter = new CPUPainter2D(width, height);
        painter.setPaint(new ColorPaint(color));
        painter.fill(geom);

        final Image image = painter.getImage();
        new FlipVerticalOperator().execute(image);

        //release opengl painter
        painter.dispose();

        return image;
    }

}
