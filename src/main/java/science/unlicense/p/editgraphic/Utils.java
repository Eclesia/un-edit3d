
package science.unlicense.p.editgraphic;

import science.unlicense.common.api.collection.primitive.FloatSequence;
import science.unlicense.common.api.collection.primitive.IntSequence;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.IndexedRange;
import science.unlicense.gpu.impl.opengl.resource.IBO;
import science.unlicense.gpu.impl.opengl.resource.VBO;
import science.unlicense.image.api.color.Color;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;

/**
 *
 * @author eclesia
 */
public class Utils {


    /**
     * Build a grid mesh. standard ground for demos.
     * @return
     */
    public static Model buildGround(){
        final FloatSequence vertices = new FloatSequence();
        final FloatSequence normals = new FloatSequence();
        final IntSequence indexes = new IntSequence();

        final int range = 10;

        final float[] normal = new float[]{0,0,1};
        int i=0;
        for(int x=-range;x<=range;x++){
            vertices.put(x).put(0).put(-range);
            vertices.put(x).put(0).put(+range);
            normals.put(normal);
            normals.put(normal);
            indexes.put(i++);
            indexes.put(i++);
        }
        for(int z=-range;z<=range;z++){
            vertices.put(-range).put(0).put(z);
            vertices.put(+range).put(0).put(z);
            normals.put(normal);
            normals.put(normal);
            indexes.put(i++);
            indexes.put(i++);
        }

        final DefaultMesh shell = new DefaultMesh(CoordinateSystems.UNDEFINED_3D);
        shell.setPositions(new VBO(vertices.toArrayFloat(), 3));
        shell.setNormals(new VBO(normals.toArrayFloat(), 3));
        shell.setIndex(new IBO(indexes.toArrayInt()));
        shell.setRanges(new IndexedRange[]{IndexedRange.LINES(0, indexes.getSize())});

        final SimpleBlinnPhong.Material material = SimpleBlinnPhong.newMaterial();
        material.setDiffuse(Color.GREEN);

        final DefaultModel mesh = new DefaultModel();
        mesh.getTechniques().add(new SimpleBlinnPhong());
        mesh.setShape(shell);
        mesh.getMaterials().add(material);

        return mesh;
    }

}
