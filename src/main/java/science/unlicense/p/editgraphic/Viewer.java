
package science.unlicense.p.editgraphic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.DefaultDisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.engine.opengl.painter.GLPainter3D;
import science.unlicense.engine.ui.widget.WCheckBox;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WScene3D;
import static science.unlicense.engine.ui.widget.WScene3D.PROPERTY_SCENE;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3d;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;

/**
 *
 * @author Johann Sorel
 */
public class Viewer extends WContainer {

    private final WScene wscene = new WScene();
    private final OrbitUpdater controller;
    private final DefaultDisplayTimerState state = new DefaultDisplayTimerState();
    private final WCheckBox lights = new WCheckBox(new Chars("Lights"));

    public Viewer() {
        setLayout(new BorderLayout());
        addChild(wscene, BorderConstraint.CENTER);
        addChild(lights, BorderConstraint.BOTTOM);
        getStyle().getSelfRule().setProperties(new Chars("background:{fill-paint:rotateHSL(@color-background 0 1 0.3)}"));

        final DefaultSceneNode scene = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        final GraphicNode base = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        scene.getChildren().add(base);

        final BBox bbox = new BBox(3);
        bbox.setRange(0, 0, 10);
        bbox.setRange(1, 0, 10);
        bbox.setRange(2, 0, 10);

        //create a target node at the center of the bbox
        final MotionModel target = DefaultMotionModel.createCrosshair3D(true);
//        final DefaultGraphicNode target = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        target.getNodeTransform().getTranslation().set(bbox.getMiddle());
        target.getNodeTransform().notifyChanged();
        scene.getChildren().add(target);

        //build the scene
        final GraphicNode camNode = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        final MonoCamera camera = new MonoCamera();
        camera.setVerticalFlip(true);
        target.getChildren().add(camera);

        //calculate camera position from object bbox
        controller = new OrbitUpdater((EventSource) null, new Vector3d(0, 1, 0), new Vector3d(1, 0, 0), camera, target);
        controller.setMinDistance(0.000001);
        camNode.getUpdaters().add(controller);

        //set camera at good distance
        final double fov = camera.getFieldOfView();
        final double spanX = bbox.getSpan(0);
        final double distX = spanX / Math.tan(fov);
        final double spanY = bbox.getSpan(1);
        final double distY = spanY / Math.tan(fov);
        // x2 because screen space is [-1...+1]
        // x1.2 to compensate perspective effect
        final float dist = (float) (Maths.max(distX,distY) * 2.0 * 1.2);

        controller.setDistance(dist);
        controller.setVerticalAngle(Angles.degreeToRadian(15));
        controller.setHorizontalAngle(Angles.degreeToRadian(15));
        controller.configureDefault();
        controller.update(null);

        wscene.setScene(scene);
        wscene.setCamera(camera);
        wscene.setLightEnable(false);

        lights.varCheck().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                wscene.setLightEnable(lights.isCheck());
            }
        });

        wscene.addEventListener(MouseMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                wscene.requestFocus();
                controller.receiveEvent(event);
            }
        });

        varScene().sync(wscene.varScene());
    }

    public void setScene(SceneNode scene) {
        setPropertyValue(PROPERTY_SCENE, scene);
    }

    public SceneNode getScene() {
        return (SceneNode) getPropertyValue(PROPERTY_SCENE);
    }

    public Property varScene() {
        return getProperty(PROPERTY_SCENE);
    }

    private class WScene extends WScene3D {

        public WScene() {
            setPainter(new GLPainter3D());
        }



        @Override
        protected boolean preUpdate(long time) {
            state.pulse();
            controller.update(state);
//            boolean n = needUpdate;
//            if (n) {
//                stage.controller.update(null);
//            }
//            needUpdate = false;
//            return n;
            return true;
        }

    }

}
