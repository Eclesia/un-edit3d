
package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSelect;
import science.unlicense.model3d.impl.technique.RenderState;

/**
 *
 * @author Johann Sorel
 */
public class WRenderState extends WContainer {

    public static final Chars PROPERTY_STATE = Chars.constant("State");

    private final WSelect polygonMode = new WSelect(new DefaultRowModel(Collections.staticSequence(new Object[]{new Chars("0 - FILL"), new Chars("1 - LINE"), new Chars("2 - POINT")})));

    public WRenderState() {
        setLayout(new FormLayout());

        final EventListener lst = new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                updateState();
            }
        };

        polygonMode.varSelection().addListener(lst);


        addChild(new WLabel(new Chars("Polygon mode")), FillConstraint.builder().coord(0, 0).build());
        addChild(polygonMode, FillConstraint.builder().coord(1, 0).build());

    }

    private void updateState() {
        final RenderState state = getState();
        if (state == null) return;

        int pm = polygonMode.getModel().asSequence().search(polygonMode.getSelection());
        state.setPolygonMode(pm);
    }


    public void setState(RenderState mesh) {
        setPropertyValue(PROPERTY_STATE, mesh);
    }

    public RenderState getState() {
        return (RenderState) getPropertyValue(PROPERTY_STATE);
    }

    public Property varState() {
        return getProperty(PROPERTY_STATE);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_STATE.equals(name)) {
            RenderState state = (RenderState) value;

            if (state == null) {
                polygonMode.setSelection(polygonMode.getModel().asSequence().get(0));
            } else {
                polygonMode.setSelection(polygonMode.getModel().asSequence().get(state.getPolygonMode()));
            }
        }

    }

}
