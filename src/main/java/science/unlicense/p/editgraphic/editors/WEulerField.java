

package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import static science.unlicense.engine.ui.widget.AbstractControlWidget.PROPERTY_VALUE;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.TupleRW;

/**
 * Edit euler angles in radians. displayed values are in degrees.
 *
 * @author Johann Sorel
 */
public class WEulerField extends WContainer implements WValueWidget {

    public WEulerField() {
        super(new FormLayout());
    }

    public void setValue(Object e){
        if (setPropertyValue(PROPERTY_VALUE, e)) {
            final TupleRW euler = (TupleRW) e;
            removeChildren();
            if(euler==null) return;

            // Heading (yaw)      [-180°...+180°]
            // Elevation (pitch)  [ -90°... +90°]
            // Bank (roll)        [-180°...+180°]
            final WSpinner spinnerX = new WCellEditor(euler,0,-180,+180);
            final WSpinner spinnerY = new WCellEditor(euler,1,-90,+90);
            final WSpinner spinnerZ = new WCellEditor(euler,2,-180,+180);

            addChild(new WLabel(new Chars("X(Heading/Yaw)")), FillConstraint.builder().coord(0, 0).build());
            addChild(spinnerX, FillConstraint.builder().coord(1, 0).build());
            addChild(new WLabel(new Chars("Y(Elevation/Pitch)")), FillConstraint.builder().coord(0, 1).build());
            addChild(spinnerY, FillConstraint.builder().coord(1, 1).build());
            addChild(new WLabel(new Chars("Z(Bank/roll)")), FillConstraint.builder().coord(0, 2).build());
            addChild(spinnerZ, FillConstraint.builder().coord(1, 2).build());
        }
    }

    public TupleRW getValue() {
        return (TupleRW) getPropertyValue(PROPERTY_VALUE);
    }

    public Variable varValue() {
        return getProperty(PROPERTY_VALUE);
    }

    @Override
    public Chars getValuePropertyName() {
        return PROPERTY_VALUE;
    }

    private final class WCellEditor extends WSpinner{

        public WCellEditor(final TupleRW euler, final int idx, double min, double max) {
            super(new NumberSpinnerModel(Double.class,0,min,max,1));
            setValue(Angles.radianToDegree(euler.get(idx)));
            varValue().addListener(new EventListener() {
                public void receiveEvent(Event event) {
                    try{
                        final double val = ((Number)getValue()).doubleValue();
                        euler.set(idx, Angles.degreeToRadian(val));
                        WEulerField.this.getEventManager().sendPropertyEvent(WEulerField.this, PROPERTY_VALUE, euler, euler);
                    }catch(RuntimeException ex){
                        Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                    }
                }
            });
        }

    }

}
