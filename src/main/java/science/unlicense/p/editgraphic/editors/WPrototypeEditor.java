
package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.model.proto.Prototype;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.component.bean.BooleanEditor;
import science.unlicense.engine.ui.component.bean.CharArrayEditor;
import science.unlicense.engine.ui.component.bean.DoubleEditor;
import science.unlicense.engine.ui.component.bean.IntegerEditor;
import science.unlicense.engine.ui.component.bean.PropertyEditor;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WColorField;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpace;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.WTupleField;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Tuple;
import science.unlicense.model3d.impl.material.TextureMapping;

/**
 *
 * @author Johann Sorel
 */
public class WPrototypeEditor extends WContainer {

    public static final Chars PROPERTY_PROTOTYPE = new Chars("Prototype");

    private final Sequence editors = new ArraySequence();
    private final WTable table = new WTable();

    public WPrototypeEditor() {
        setLayout(new BorderLayout());

        final DefaultColumn nameCol = new DefaultColumn(new Chars("Attribute"), new ObjectPresenter() {
            @Override
            public Widget createWidget(Object candidate) {
                final Property p = (Property) candidate;
                return new WLabel(p.getName());
            }
        });
        final DefaultColumn valueCol = new DefaultColumn(new Chars("Value"), new PropertyValuePresenter() );
        nameCol.setBestWidth(150);
        valueCol.setBestWidth(FormLayout.SIZE_EXPAND);

        table.getColumns().add(nameCol);
        table.getColumns().add(valueCol);

        //default editors
        editors.add(CharArrayEditor.INSTANCE);
        editors.add(BooleanEditor.INSTANCE);
        editors.add(DoubleEditor.INSTANCE);
        editors.add(IntegerEditor.INSTANCE);
        editors.add(ColorEditor.INSTANCE);
        editors.add(TupleEditor.INSTANCE);

        addChild(table, BorderConstraint.CENTER);
    }

    public void setPrototype(Prototype proto) {
        setPropertyValue(PROPERTY_PROTOTYPE, proto);
    }

    public Prototype getPrototype() {
        return (Prototype) getPropertyValue(PROPERTY_PROTOTYPE);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_PROTOTYPE.equals(name)) {
            final Prototype proto = (Prototype) value;
            if (proto == null) {
                table.setRowModel(new DefaultRowModel());
            } else {
                final Sequence properties = new ArraySequence();
                final Iterator keys = proto.asDictionary().getKeys().createIterator();
                while (keys.hasNext()) {
                    properties.add(proto.getProperty((Chars) keys.next()));
                }
                table.setRowModel(new DefaultRowModel(properties));
            }
        }
    }

    private final class PropertyValuePresenter implements ObjectPresenter{
        public Widget createWidget(Object candidate) {
            final Property property = (Property) candidate;

            for (int k=0,kn=editors.getSize();k<kn;k++){
                final WValueWidget editor = ((PropertyEditor) editors.get(k)).create(property);
                if (editor != null) {
                    editor.getProperty(editor.getValuePropertyName()).sync(property);
                    return editor;
                }
            }

            return new WSpace();
        }
    }

    private static final class ColorEditor implements PropertyEditor {

        public static final ColorEditor INSTANCE = new ColorEditor();

        @Override
        public WValueWidget create(Property property) {
            final Class valueClass = property.getType().getValueClass();
            if (!Color.class.isAssignableFrom(valueClass)) {
                return null;
            }
            return new WColorField();
        }
    }

    private static final class TupleEditor implements PropertyEditor {

        public static final TupleEditor INSTANCE = new TupleEditor();

        @Override
        public WValueWidget create(Property property) {
            final Class valueClass = property.getType().getValueClass();
            if (!Tuple.class.isAssignableFrom(valueClass)) {
                return null;
            }
            return new WTupleField();
        }
    }

    private static final class TextureEditor implements PropertyEditor {

        public static final TextureEditor INSTANCE = new TextureEditor();

        @Override
        public WValueWidget create(Property property) {
            final Class valueClass = property.getType().getValueClass();
            if (!TextureMapping.class.isAssignableFrom(valueClass)) {
                return null;
            }
            return new WTupleField();
        }
    }

    private static final class WTextureMappingEditor extends WContainer {

        public WTextureMappingEditor() {

        }

    }

}
