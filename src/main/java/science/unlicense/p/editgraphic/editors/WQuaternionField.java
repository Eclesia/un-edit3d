

package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.logging.Loggers;
import science.unlicense.common.api.predicate.Variable;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.NumberSpinnerModel;
import static science.unlicense.engine.ui.widget.AbstractControlWidget.PROPERTY_VALUE;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WSpinner;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.math.impl.Quaternion;

/**
 *
 * @author Johann Sorel
 */
public class WQuaternionField extends WContainer implements WValueWidget {

    public WQuaternionField() {
        super(new FormLayout());
    }

    public void setValue(Object m){
        if (setPropertyValue(PROPERTY_VALUE, m)) {
            final Quaternion quaternion = (Quaternion) m;
            getChildren().removeAll();
            if(m==null) return;

            final WSpinner spinnerX = new WCellEditor(quaternion,0);
            final WSpinner spinnerY = new WCellEditor(quaternion,1);
            final WSpinner spinnerZ = new WCellEditor(quaternion,2);
            final WSpinner spinnerW = new WCellEditor(quaternion,3);

            addChild(new WLabel(new Chars("X")), FillConstraint.builder().coord(0, 0).build());
            addChild(spinnerX, FillConstraint.builder().coord(1, 0).build());
            addChild(new WLabel(new Chars("Y")), FillConstraint.builder().coord(0, 1).build());
            addChild(spinnerY, FillConstraint.builder().coord(1, 1).build());
            addChild(new WLabel(new Chars("Z")), FillConstraint.builder().coord(0, 2).build());
            addChild(spinnerZ, FillConstraint.builder().coord(1, 2).build());
            addChild(new WLabel(new Chars("W")), FillConstraint.builder().coord(0, 3).build());
            addChild(spinnerW, FillConstraint.builder().coord(1, 3).build());
        }
    }

    public Quaternion getValue() {
        return (Quaternion) getPropertyValue(PROPERTY_VALUE);
    }

    public Variable varValue() {
        return getProperty(PROPERTY_VALUE);
    }

    @Override
    public Chars getValuePropertyName() {
        return PROPERTY_VALUE;
    }

    private final class WCellEditor extends WSpinner{

        public WCellEditor(final Quaternion quaternion, final int idx) {
            super(new NumberSpinnerModel(Double.class,0,null,null,1));
            setValue(quaternion.get(idx));
            varValue().addListener(new EventListener() {
                public void receiveEvent(Event event) {
                    try{
                        final double val = ((Number)getValue()).doubleValue();
                        quaternion.set(idx, val);
                        WQuaternionField.this.getEventManager().sendPropertyEvent(WQuaternionField.this, PROPERTY_VALUE, quaternion, quaternion);
                    }catch(RuntimeException ex){
                        Loggers.get().log(ex, Logger.LEVEL_INFORMATION);
                    }
                }
            });
        }

    }

}
