
package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTupleField;
import science.unlicense.engine.ui.widget.WValueWidget;
import science.unlicense.math.api.Similarity;
import science.unlicense.math.api.SimilarityRW;

/**
 * Widget to edit a Similarity object.
 *
 * @author Johann Sorel
 */
public class WSimilarityEditor extends WContainer implements WValueWidget {

    public static final Chars PROPERTY_SIMILARITY = new Chars("Similarity");

    private final WRotationField wrotation = new WRotationField();
    private final WTupleField wtranslation = new WTupleField();
    private final WTupleField wscale = new WTupleField();

    public WSimilarityEditor() {
        setLayout(new FormLayout());

        final WLabel lblRot = new WLabel(new Chars("Rotation"));
        final WLabel lblSca = new WLabel(new Chars("Scale"));
        final WLabel lblTrs = new WLabel(new Chars("Translation"));

        addChild(lblTrs,        FillConstraint.builder().coord(0, 0).build());
        addChild(wtranslation,  FillConstraint.builder().coord(0, 1).build());
        addChild(lblSca,        FillConstraint.builder().coord(0, 2).build());
        addChild(wscale,        FillConstraint.builder().coord(0, 3).build());
        addChild(lblRot,        FillConstraint.builder().coord(0, 4).build());
        addChild(wrotation,     FillConstraint.builder().coord(0, 5).build());

        final EventListener valueListener = new EventListener() {
            public void receiveEvent(Event event) {
                final Similarity transform = getSimilarity();
                if (transform == null) return;
                if (transform instanceof SimilarityRW) {
                    ((SimilarityRW) transform).notifyChanged();
                }
                WSimilarityEditor.this.getEventManager().sendPropertyEvent(WSimilarityEditor.this, PROPERTY_SIMILARITY, transform, transform);
            }
        };
        wtranslation.varTuple().addListener(valueListener);
        wscale.varTuple().addListener(valueListener);
        wrotation.addEventListener(PropertyMessage.PREDICATE, valueListener);
    }

    @Override
    public Chars getValuePropertyName() {
        return PROPERTY_SIMILARITY;
    }

    public void setSimilarity(Similarity trs) {
        setPropertyValue(PROPERTY_SIMILARITY, trs);
    }

    public Similarity getSimilarity() {
        return (Similarity) getPropertyValue(PROPERTY_SIMILARITY);
    }

    public Property varSimilarity() {
        return getProperty(PROPERTY_SIMILARITY);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_SIMILARITY.equals(name)) {
            Similarity sim = (Similarity) value;
            if (sim != null) {
                wrotation.setValue(sim.getRotation());
                wtranslation.setTuple(sim.getTranslation());
                wscale.setTuple(sim.getScale());
            } else {
                wrotation.setValue(null);
                wtranslation.setTuple(null);
                wscale.setTuple(null);
            }
        }

    }

}
