
package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.ArraySequence;
import science.unlicense.common.api.collection.CollectionMessage;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.Property;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.engine.ui.model.DefaultColumn;
import science.unlicense.engine.ui.model.DefaultRowModel;
import science.unlicense.engine.ui.model.ObjectPresenter;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WLabel;
import science.unlicense.engine.ui.widget.WTable;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.model3d.impl.scene.Model;
import science.unlicense.model3d.impl.technique.Technique;

/**
 *
 * @author Johann Sorel
 */
public class WTechniquesEditor extends WContainer {

    public static final Chars PROPERTY_MODEL = Chars.constant("Model");

    private final WTable table = new WTable();
    private final WPrototypeEditor proto = new WPrototypeEditor();
    private final WRenderState state = new WRenderState();

    public WTechniquesEditor() {
        setLayout(new BorderLayout());
        addChild(table, BorderConstraint.TOP);
        addChild(proto, BorderConstraint.CENTER);
        addChild(state, BorderConstraint.BOTTOM);

        table.getSelection().addEventListener(CollectionMessage.PREDICATE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final Object[] selection = ((CollectionMessage) event.getMessage()).getNewElements();
                if (selection.length == 1) {
                    final Technique technique = (Technique) selection[0];
                    proto.setPrototype(technique.properties());
                    state.setState(technique.getState());
                }
            }
        });
        table.getColumns().add(new DefaultColumn(Chars.EMPTY, new NamePresenter()));
        table.setHeaderVisible(false);
    }

    public void setModel(Model model) {
        setPropertyValue(PROPERTY_MODEL, model);
    }

    public Model getModel() {
        return (Model) getPropertyValue(PROPERTY_MODEL);
    }

    public Property varModel() {
        return getProperty(PROPERTY_MODEL);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_MODEL.equals(name)) {
            final Model model = (Model) value;
            proto.setPrototype(null);
            state.setState(null);
            if (model == null) {
                table.setRowModel(new DefaultRowModel(new ArraySequence()));
            } else {
                table.setRowModel(new DefaultRowModel(model.getTechniques()));
            }
        }
    }

    private static class NamePresenter implements ObjectPresenter {

        @Override
        public Widget createWidget(Object candidate) {
            final WLabel lbl = new WLabel();
            lbl.setText(((Technique) candidate).getMaterialName());
            return lbl;
        }

    }

}
