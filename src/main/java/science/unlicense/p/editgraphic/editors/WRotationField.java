
package science.unlicense.p.editgraphic.editors;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.FormLayout;
import science.unlicense.engine.ui.model.CheckGroup;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WMatrixField;
import science.unlicense.engine.ui.widget.WSwitch;
import science.unlicense.engine.ui.widget.Widget;
import science.unlicense.math.api.Matrix;
import science.unlicense.math.api.MatrixRW;
import science.unlicense.math.api.Tuple;
import science.unlicense.math.api.TupleRW;
import science.unlicense.math.impl.VectorNd;
import science.unlicense.math.impl.Matrix3x3;
import science.unlicense.math.impl.Quaternion;


/**
 *
 * @author Johann Sorel
 */
public class WRotationField extends WContainer{

    public static final Chars PROPERTY_VALUE = Chars.constant("Value");

    private final CheckGroup group = new CheckGroup();
    private final WSwitch switchMatrix = new WSwitch(new Chars("M"));
    private final WSwitch switchQuaternion = new WSwitch(new Chars("Q"));
    private final WSwitch switchEuler = new WSwitch(new Chars("E"));

    private final WMatrixField matrixEditor = new WMatrixField();
    private final WQuaternionField quaternionEditor = new WQuaternionField();
    private final WEulerField eulerEditor = new WEulerField();

    public WRotationField() {
        super(new BorderLayout());

        final WContainer top = new WContainer(new FormLayout());
        top.addChild(switchMatrix, FillConstraint.builder().coord(0, 0).build());
        top.addChild(switchQuaternion, FillConstraint.builder().coord(1, 0).build());
        top.addChild(switchEuler, FillConstraint.builder().coord(2, 0).build());
        addChild(top, BorderConstraint.TOP);

        group.add(switchMatrix);
        group.add(switchEuler);
        group.add(switchQuaternion);

        matrixEditor.varMatrix().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                Object value = getValue();
                if(value instanceof Matrix){
                    ((MatrixRW)value).set(matrixEditor.getMatrix());
                }else if(value instanceof Quaternion){
                    ((Quaternion)value).fromMatrix(matrixEditor.getMatrix());
                }else if(value instanceof TupleRW){
                    ((TupleRW)value).set(new Matrix3x3(matrixEditor.getMatrix()).toEuler());
                }
                WRotationField.this.getEventManager().sendPropertyEvent(WRotationField.this, PROPERTY_VALUE, value, value);
            }
        });
        quaternionEditor.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                Object value = getValue();
                if(value instanceof Matrix){
                    ((MatrixRW) value).set(quaternionEditor.getValue().toMatrix3());
                }else if(value instanceof Quaternion){
                    ((Quaternion)value).set(quaternionEditor.getValue());
                }else if(value instanceof TupleRW){
                    ((TupleRW)value).set(quaternionEditor.getValue().toEuler());
                }
                WRotationField.this.getEventManager().sendPropertyEvent(WRotationField.this, PROPERTY_VALUE, value, value);
            }
        });
        eulerEditor.varValue().addListener(new EventListener() {
            public void receiveEvent(Event event) {
                Object value = getValue();
                if(value instanceof Matrix){
                    ((MatrixRW)value).set(Matrix3x3.createRotationEuler(eulerEditor.getValue()));
                }else if(value instanceof Quaternion){
                    ((Quaternion)value).fromEuler(new VectorNd(eulerEditor.getValue()));
                }else if(value instanceof TupleRW){
                    ((TupleRW)value).set(eulerEditor.getValue());
                }
                WRotationField.this.getEventManager().sendPropertyEvent(WRotationField.this, PROPERTY_VALUE, value, value);
            }
        });

        final EventListener typeListener = new EventListener() {
            public void receiveEvent(Event event) {
                Widget current = null;
                Object value = getValue();
                if (switchMatrix.isCheck()) {
                    current = matrixEditor;
                    if(value instanceof Matrix) matrixEditor.setMatrix(((Matrix)value).copy() );
                    else if(value instanceof Quaternion) matrixEditor.setMatrix( ((Quaternion)value).toMatrix3() );
                    else if(value instanceof Tuple) matrixEditor.setMatrix(Matrix3x3.createRotationEuler((Tuple)value));
                } else if(switchQuaternion.isCheck()) {
                    current = quaternionEditor;
                    if(value instanceof Matrix) quaternionEditor.setValue(new Matrix3x3((Matrix)value).toQuaternion() );
                    else if(value instanceof Quaternion) quaternionEditor.setValue( ((Quaternion)value).copy() );
                    else if(value instanceof Tuple) quaternionEditor.setValue(new Quaternion().fromEuler(new VectorNd((Tuple)value)));
                } else if(switchEuler.isCheck()) {
                    current = eulerEditor;
                    if(value instanceof Matrix) eulerEditor.setValue(new Matrix3x3((Matrix)value).toEuler() );
                    else if(value instanceof Quaternion) eulerEditor.setValue( ((Quaternion)value).toEuler() );
                    else if(value instanceof Tuple) eulerEditor.setValue( ((Tuple)value).copy() );
                }
                getChildren().remove(matrixEditor);
                getChildren().remove(quaternionEditor);
                getChildren().remove(eulerEditor);
                if(current!=null){
                    addChild(current, BorderConstraint.CENTER);
                }
            }
        };
        switchMatrix.varCheck().addListener(typeListener);
        switchQuaternion.varCheck().addListener(typeListener);
        switchEuler.varCheck().addListener(typeListener);

    }

    public void setValue(Object m){
        setPropertyValue(PROPERTY_VALUE, m);
    }

    public Object getValue() {
        return getPropertyValue(PROPERTY_VALUE);
    }

    @Override
    protected void valueChanged(Chars name, Object oldValue, Object value) {
        super.valueChanged(name, oldValue, value);

        if (PROPERTY_VALUE.equals(name)) {
            Object m = value;
            if (m == null) return;

            switchMatrix.setCheck(false);
            switchQuaternion.setCheck(false);
            switchEuler.setCheck(false);

            if (m instanceof Matrix) {
                switchMatrix.setCheck(true);
            } else if (m instanceof Quaternion) {
                switchQuaternion.setCheck(true);
            } else if (m instanceof Tuple) {
                switchEuler.setCheck(true);
            }
        }
    }

}
