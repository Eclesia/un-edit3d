
package science.unlicense.p.editgraphic;

import science.unlicense.archive.api.Archive;
import science.unlicense.common.api.Arrays;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collection;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.EventSource;
import science.unlicense.common.api.model.tree.Node;
import science.unlicense.common.api.model.tree.NodeVisitor;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.display.api.anim.Animation;
import science.unlicense.display.api.anim.CompoundAnimation;
import science.unlicense.display.api.desktop.DragAndDropMessage;
import science.unlicense.display.api.desktop.KeyMessage;
import science.unlicense.display.api.desktop.MouseMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.SplitConstraint;
import science.unlicense.display.api.layout.StackConstraint;
import science.unlicense.display.api.layout.StackLayout;
import science.unlicense.display.api.scene.DefaultSceneNode;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.api.scene.SceneUtils;
import science.unlicense.display.impl.DefaultDisplayTimerState;
import science.unlicense.display.impl.DisplayTimerState;
import science.unlicense.display.impl.camera.MonoCamera;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.display.impl.light.DirectionalLight;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.display.impl.scene.GraphicNode;
import science.unlicense.display.impl.updater.AnimationUpdater;
import science.unlicense.display.impl.updater.OrbitUpdater;
import science.unlicense.display.impl.updater.Updater;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Resources;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.encoding.api.store.StoreException;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.opengl.GLProcessContext;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrame;
import science.unlicense.engine.opengl.desktop.opengl.GLUIFrameManager;
import science.unlicense.engine.opengl.painter.GLPainter3D;
import science.unlicense.engine.opengl.phase.ClearPhase;
import science.unlicense.engine.opengl.phase.RenderPhase;
import science.unlicense.engine.opengl.phase.UpdatePhase;
import science.unlicense.engine.software.SoftwarePainter3D;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.component.WQueryDialog;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WScene3D;
import science.unlicense.engine.ui.widget.WSplitContainer;
import science.unlicense.geometry.api.BBox;
import science.unlicense.geometry.api.Extent;
import science.unlicense.geometry.api.system.CoordinateSystems;
import science.unlicense.geometry.impl.DefaultMesh;
import science.unlicense.geometry.impl.Rectangle;
import science.unlicense.image.api.color.Color;
import science.unlicense.math.api.Angles;
import science.unlicense.math.api.Maths;
import science.unlicense.math.impl.Vector3d;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.model3d.impl.effect.SSAOTask;
import science.unlicense.model3d.impl.physic.RelativeSkeletonPose;
import science.unlicense.model3d.impl.physic.SkeletonPoseResolver;
import science.unlicense.model3d.impl.physic.Skeletons;
import science.unlicense.model3d.impl.scene.DefaultModel;
import science.unlicense.model3d.impl.scene.DefaultMotionModel;
import science.unlicense.model3d.impl.scene.MotionModel;
import science.unlicense.model3d.impl.technique.SimpleBlinnPhong;
import science.unlicense.p.editvisual.v3d.subframe.scene.WScenePane;
import science.unlicense.system.api.desktop.Attachment;

/**
 *
 * @author Johann Sorel
 */
public class QuickViewerFrame {

    public static final int MODE_WIDGET_CPU = 0;
    public static final int MODE_WIDGET_GPU = 1;
    public static final int MODE_GPU = 2;

    private static final NodeVisitor NODE_VISITOR = new NodeVisitor(){
        public Object visit(final Node node, final Object context) {
            if (node instanceof GraphicNode){
                updateNode((DisplayTimerState) context, (GraphicNode) node);
            }
            return super.visit(node, context);
        }
    };


    protected static void updateNode(final DisplayTimerState context, final GraphicNode node){
        final Iterator iteup = node.getUpdaters().createIterator();
        while (iteup.hasNext()){
            final Updater up = (Updater) iteup.next();
            if (up==null) continue;
            up.update(context);
        }
    }

    public static void main(String[] args) {
        final Chars[] array = new Chars[]{
            new Chars("Widget CPU"),
            new Chars("Widget GPU"),
            new Chars("GPU")};
        final Chars result = WQueryDialog.showInput(SwingFrameManager.INSTANCE,
                new Chars("Select rendering engine"), array);

        if (result == null) return;

        new QuickViewerFrame(Arrays.getFirstOccurence(array, result));
    }


    private final GraphicNode scene = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
    private final MonoCamera camera = new MonoCamera();
    private final MotionModel crosshair = DefaultMotionModel.createCrosshair3D(true);
    private final OrbitUpdater controller = new OrbitUpdater((EventSource) null, camera, crosshair);
    private GLProcessContext context;
    private MotionModel currentModel;
    private AnimationUpdater animationUpdater;
//    private ALSource currentAudio;

    private boolean glCompatible;

    private QuickViewerFrame(int mode) {
        glCompatible = mode == MODE_WIDGET_GPU || mode == MODE_GPU;

        final DefaultModel plan = new DefaultModel();
        plan.setShape(DefaultMesh.createPlan(
                new Vector3d(-10, 0, -10),
                new Vector3d(-10, 0, 10),
                new Vector3d(10, 0, 10),
                new Vector3d(10, 0,-10)));
        GLEngineUtils.makeCompatible(plan);
        SimpleBlinnPhong.Material planMaterial = SimpleBlinnPhong.newMaterial();
        planMaterial.setDiffuse(Color.GRAY_NORMAL);
        plan.getMaterials().add(planMaterial);
        plan.getTechniques().add(new SimpleBlinnPhong());

//        base.getChildren().add(model);
        scene.getChildren().add(plan);
        scene.getChildren().add(crosshair);

        final BBox bbox = SceneUtils.calculateBBox(scene, null);
        focusOn(bbox);

        camera.setNearPlane(1);
        camera.setFarPlane(10000);
        crosshair.getChildren().add(camera);

        controller.setMinDistance(0.1);
        controller.setDistance(10);
        controller.setVerticalAngle(Angles.degreeToRadian(15));
        controller.setHorizontalAngle(Angles.degreeToRadian(15));
        controller.configureDefault();
        camera.getUpdaters().add(controller);


        final WContainer cnt = new WContainer();
        cnt.setLayout(new StackLayout());
        cnt.addEventListener(Predicate.TRUE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {

                if (event.getMessage() instanceof DragAndDropMessage) {
                    final DragAndDropMessage dd = (DragAndDropMessage) event.getMessage();
                    final int type = dd.getType();
                    System.out.println(dd);

                    final Attachment att = dd.getBag().getAttachment(new Chars("application/x-java-file-list; class=java.util.List"), null);
                    if (att == null) return;

                    if (DragAndDropMessage.TYPE_ENTER == type) {
                        dd.consume();
                        return;
                    } else if (DragAndDropMessage.TYPE_DROP == type) {

                        Object value = att.getObject();
                        if (value instanceof Sequence) {
                            final Sequence seq = (Sequence) value;
                            final Path p = (Path) seq.get(0);

                            try {
                                load(p, false);
                                final BBox bbox = SceneUtils.calculateBBox(scene, null);
                                focusOn(bbox);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }

            }
        });

        final WScenePane wscenetree = new WScenePane();
        wscenetree.setScene(scene);
        final WSplitContainer above = new WSplitContainer();
        above.addChild(wscenetree, SplitConstraint.RIGHT);

        if (mode == MODE_WIDGET_CPU || mode == MODE_WIDGET_GPU) {
            camera.setVerticalFlip(true);

            final DefaultDisplayTimerState state = new DefaultDisplayTimerState();

            final WScene3D widget = new WScene3D(scene, camera) {
                @Override
                protected boolean preUpdate(long time) {
                    state.pulse();
                    controller.update(state);
                    NODE_VISITOR.visit(scene, state);
                    return true;
                }
            };
            widget.addEventListener(MouseMessage.PREDICATE, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    widget.requestFocus();
                    controller.receiveEvent(event);
                }
            });
            widget.addEventListener(KeyMessage.PREDICATE, new EventListener() {
                @Override
                public void receiveEvent(Event event) {
                    controller.receiveEvent(event);
                }
            });
            if (mode == MODE_WIDGET_GPU) {
                GLPainter3D painter = new GLPainter3D();
                widget.setPainter(painter);
            } else {
                final SoftwarePainter3D painter = new SoftwarePainter3D(new Extent.Long(10, 10));
                painter.getAfterEffects().add(new SSAOTask());
                widget.setPainter(painter);
            }
            widget.setLightEnable(true);

            final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);
            final WContainer root = frame.getContainer();
            root.setLayout(new BorderLayout());
            root.addChild(cnt, BorderConstraint.CENTER);
            cnt.addChild(widget, new StackConstraint(0));
            cnt.addChild(above, new StackConstraint(1));

            frame.setSize(800, 600);
            frame.setVisible(true);
        } else {
            GLEngineUtils.makeCompatible(scene);
            camera.setVerticalFlip(false);

            final GLUIFrame frame = GLUIFrameManager.INSTANCE.createFrame(false, true);
            final GLProcessContext context = frame.getContext();
            frame.addEventListener(MouseMessage.PREDICATE, controller);

            context.getPhases().add(new ClearPhase());
            context.getPhases().add(new UpdatePhase(scene));
            context.getPhases().add(new RenderPhase(scene,camera));


            final WContainer root = frame.getContainer();
            root.setLayout(new BorderLayout());
//            root.getStyle().getSelfRule().setProperty(Chars.EMPTY, Chars.EMPTY);
            root.getStyle().getSelfRule().setProperties(new Chars("background:none"));
            root.addChild(cnt, BorderConstraint.CENTER);
            cnt.addChild(above, new StackConstraint(1));

            //show the frame
            frame.setSize(800, 600);
            frame.setVisible(true);
        }

    }

    private SceneNode load(Path path, boolean recursive) throws IOException, StoreException {

        //clean scene
        if (currentModel != null) {
            scene.getChildren().remove(currentModel);
            if (context != null) {
                GLEngineUtils.dispose(currentModel, context);
            }
        }

        if (path.isContainer()) {
            if (recursive) {
                final Iterator ite = path.getChildren().createIterator();
                while (ite.hasNext()) {
                    final Path child = (Path) ite.next();
                    SceneNode sn = null;
                    try {
                        sn = load(child, recursive);
                    } catch (IOException | StoreException ex) {
                        System.out.println(ex.getMessage());
                    }
                    if (sn != null) {
                        return sn;
                    }
                }
            }
            return null;
        }


        //search content of path
        final Store store = Formats.open(path);
        final SceneResource sceneRes = (SceneResource) Resources.findFirst(store, SceneResource.class);

        MotionModel sn = null;
        CompoundAnimation animation = null;

        if (sceneRes != null) {
            final Collection col = sceneRes.getElements();
            final Iterator ite = col.createIterator();
            while (ite.hasNext()){
                final Object obj = ite.next();
                if (obj instanceof MotionModel){
                    sn = (MotionModel) obj;
                } else if (obj instanceof GraphicNode){
                    final MotionModel mg = new DefaultMotionModel(CoordinateSystems.UNDEFINED_3D);
                    mg.getChildren().add(obj);
                    sn = mg;
                }
                if (obj instanceof Animation){
                    animation = new CompoundAnimation();
                    animation.getElements().add(obj);
                } else if (obj instanceof RelativeSkeletonPose) {
                    RelativeSkeletonPose rsp = (RelativeSkeletonPose) obj;

                    if (currentModel != null && rsp!=null){
                        SkeletonPoseResolver.resolve1(currentModel.getSkeleton(), rsp, null);
                        currentModel.getSkeleton().solveKinematics();
                        currentModel.getSkeleton().updateBindPose();
                    }
                }
            }

            if (sn != null) {

                //new scene
                if (glCompatible) GLEngineUtils.makeCompatible(sn);
                currentModel = sn;
                BBox bbox = SceneUtils.calculateBBox(sn, null);

                if (bbox.getSpan(0) < 1) {
                    sn.getNodeTransform().getScale().localScale(5);
                    sn.getNodeTransform().notifyChanged();
                    bbox = SceneUtils.calculateBBox(sn, null);
                }

//                sn.getTechniques().add(new DebugNodeBoxTechnique());
                focusOn(bbox);
                scene.getChildren().add(sn);
            }
        } else if (store instanceof Archive) {
            //open archive and search for a model
            final Archive archive = (Archive) store;
            return load(archive, true);
        }

        if (animation != null && currentModel != null) {

            //remove any previous animation
            if (animationUpdater != null) {
                currentModel.getUpdaters().remove(animationUpdater);
                animationUpdater.getAnimation().stop();
                animationUpdater = null;
            }

            //add new animation
            animationUpdater = new AnimationUpdater(animation);
            Skeletons.mapAnimation(animation, currentModel.getSkeleton(), currentModel, new HashDictionary());
            animation.setRepeatCount(0);
            animation.setSpeed(1.0f);
            currentModel.getUpdaters().add(animationUpdater);

//            try {
//                loadAudio(path);
//            } catch(Exception ex) {
//                ex.printStackTrace();
//            }
            animation.start();
        }

        return null;
    }

    private void focusOn(BBox bbox) {
        if (bbox == null) return;

        //create a target node at the center of the bbox
        crosshair.getNodeTransform().getTranslation().set(bbox.getMiddle());
        crosshair.getNodeTransform().notifyChanged();


        //set camera at good distance
        final double fov = 45;
        final double spanX = bbox.getSpan(0);
        final double distX = spanX / Math.tan(fov);
        final double spanY = bbox.getSpan(1);
        final double distY = spanY / Math.tan(fov);
        // x2 because screen space is [-1...+1]
        // x1.2 to compensate perspective effect
        final float dist = (float) (Maths.max(distX,distY) * 2.0 * 1.2);

        controller.setDistance(dist);

        //some lightning
        scene.getChildren().add(new AmbientLight(Color.GRAY_LIGHT,Color.GRAY_LIGHT));

        final DirectionalLight dl = new DirectionalLight(Color.WHITE, Color.GRAY_DARK);
        scene.getChildren().add(dl);

//        final SpotLight spot = new SpotLight();
//        spot.setAttenuation(new Attenuation(1, 0.2f, 0.2f));
//        spot.getNodeTransform().getRotation().set(Matrix3x3.createFromAngles(-Math.PI/2.0, 0, 0));
//        spot.getNodeTransform().notifyChanged();
//        spot.getAttenuation().setConstant(1);
//        spot.getAttenuation().setLinear(0);
//        spot.getAttenuation().setQuadratic(0);
//        spot.getTechniques().add(new DebugLightBoxTechnique());

        final MonoCamera otherCam = new MonoCamera();
        otherCam.setRenderArea(new Rectangle(0, 0, 512, 512));
//        otherCam.getTechniques().add(new DebugCameraFrustrumTechnique());


        final DefaultSceneNode lightBase = new DefaultSceneNode(CoordinateSystems.UNDEFINED_3D);
        lightBase.getNodeTransform().getTranslation().setXYZ(0, 16, 0);
        lightBase.getNodeTransform().notifyChanged();

//        lightBase.getChildren().add(spot);
        lightBase.getChildren().add(otherCam);

        scene.getChildren().add(lightBase);

    }

//    private void loadAudio(Path base) throws IOException, StoreException {
//
//        if (currentAudio != null) {
//            currentAudio.stop();
//            currentAudio.unload();
//            currentAudio = null;
//        }
//
//        if (!base.isContainer()) {
//            base = base.getParent();
//        }
//
//        Path path = null;
//        Iterator ite = base.getChildren().createIterator();
//        while (ite.hasNext()) {
//            Object cdt = ite.next();
//            if (Medias.canDecode(cdt)) {
//                path = (Path) cdt;
//                break;
//            }
//        }
//
//        if (path == null) return;
//
//
//        final Store store = Medias.open(path);
//        final Media media = (Media) Resources.findFirst(store, Media.class);
//
//        //tranform audio in a supported byte buffer
//        final AudioTrack meta = (AudioTrack) media.getTracks()[0];
//        System.out.println(Arrays.toChars(meta.getChannels()));
//        final MediaReadStream reader = media.createReader(null);
//
//        //recode stream in a stereo 16 bits per sample.
//        final ArrayOutputStream out = new ArrayOutputStream();
//        final DataOutputStream ds = new DataOutputStream(out, Endianness.LITTLE_ENDIAN);
//
//        for(MediaPacket pack=reader.next();pack!=null;pack=reader.next()){
//            final AudioSamples samples = ((AudioPacket) pack).getSamples();
//            final int[] audiosamples = samples.asPCM(null, 16);
//            ds.writeUShort(audiosamples[0]);
//            ds.writeUShort(audiosamples[1]);
//        }
//
//        final byte[] all = out.getBuffer().toArrayByte();
//
//        //buffer which contains audio datas
//        final ALBuffer data = new ALBuffer();
//        data.setFormat(AL.AL_FORMAT_STEREO16);
//        data.setFrequency((int) meta.getSampleRate());
//        data.setSize(all.length);
//        data.setData(ByteBuffer.wrap(all));
//        data.load();
//
//        //the audio source
//        currentAudio = new ALSource();
//        currentAudio.setBuffer(data);
//        currentAudio.load();
//        currentAudio.play();
//    }

}
