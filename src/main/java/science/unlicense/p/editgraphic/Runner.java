
package science.unlicense.p.editgraphic;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Iterator;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.event.Event;
import science.unlicense.common.api.event.EventListener;
import science.unlicense.common.api.event.PropertyMessage;
import science.unlicense.common.api.predicate.Predicate;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.display.api.desktop.DragAndDropMessage;
import science.unlicense.display.api.layout.BorderConstraint;
import science.unlicense.display.api.layout.BorderLayout;
import science.unlicense.display.api.layout.FillConstraint;
import science.unlicense.display.api.layout.LineLayout;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Store;
import science.unlicense.engine.opengl.GLEngineUtils;
import science.unlicense.engine.swing.SwingFrameManager;
import science.unlicense.engine.ui.component.WButtonBar;
import science.unlicense.engine.ui.desktop.UIFrame;
import science.unlicense.engine.ui.io.RSReader;
import science.unlicense.engine.ui.style.StyleDocument;
import science.unlicense.engine.ui.style.SystemStyle;
import science.unlicense.engine.ui.style.WStyle;
import science.unlicense.engine.ui.style.WidgetStyles;
import science.unlicense.engine.ui.widget.WContainer;
import science.unlicense.engine.ui.widget.WPlaform;
import science.unlicense.engine.ui.widget.menu.WMenuButton;
import science.unlicense.engine.ui.widget.menu.WMenuDropDown;
import science.unlicense.model3d.api.Model3Ds;
import science.unlicense.model3d.api.SceneResource;
import science.unlicense.p.editgraphic.editors.WMaterialsEditor;
import science.unlicense.p.editgraphic.editors.WSimilarityEditor;
import science.unlicense.p.editgraphic.editors.WTechniquesEditor;
import science.unlicense.p.editvisual.v3d.subframe.scene.WScenePane;
import science.unlicense.system.api.desktop.Attachment;
import science.unlicense.model3d.impl.scene.Model;

/**
 *
 * @author Johann Sorel
 */
public class Runner {

    public static void main(String[] args) {

        final UIFrame frame = SwingFrameManager.INSTANCE.createFrame(false);



        final WButtonBar bar = new WButtonBar();
        bar.setLayout(new LineLayout());
        final WMenuDropDown menu = new WMenuDropDown();
        menu.setText(new Chars("File"));
        final WMenuButton exit = new WMenuButton(new Chars("Exit"),null, new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                System.exit(0);
            }
        });
        menu.getDropdown().addChild(exit,null);
        bar.addChild(menu,null);
        bar.addChild(createLightButton(new Chars(" Light ")), FillConstraint.builder().coord(1, 0).build());
        bar.addChild(createDarkButton(new Chars(" Dark ")), FillConstraint.builder().coord(2, 0).build());
//        createThemeButton(SystemStyle.THEME_LIGHT, new Chars(" Light ")).doClick();
//        createThemeButton(SystemStyle.THEME_DARK, new Chars(" Dark ")).doClick();


        final WContainer container = frame.getContainer();
        container.setLayout(new BorderLayout());

        final Viewer viewer = new Viewer();
        final WScenePane wscenetree = new WScenePane();
        final WMaterialsEditor wmaterialeditor = new WMaterialsEditor();
        final WTechniquesEditor wtechniqueeditor = new WTechniquesEditor();
        final WSimilarityEditor wsimilarityEditor = new WSimilarityEditor();

        final Model ground = Utils.buildGround();
        ground.setId(new Chars("ground"));
        viewer.getScene().getChildren().add(ground);
        GLEngineUtils.makeCompatible(viewer.getScene());
        wscenetree.varScene().sync(viewer.varScene(), true);

        final WPlaform.Block viewBlock = new WPlaform.Block(new Chars("View"), WPlaform.HORIZONTAL_CENTER, WPlaform.VERTICAL_CENTER);
        viewBlock.addChild(viewer, BorderConstraint.CENTER);
        final WPlaform.Block sceneBlock = new WPlaform.Block(new Chars("Scene"), WPlaform.HORIZONTAL_LEFT, WPlaform.VERTICAL_CENTER);
        sceneBlock.addChild(wscenetree, BorderConstraint.CENTER);
        final WPlaform.Block materialBlock = new WPlaform.Block(new Chars("Materials"), WPlaform.HORIZONTAL_RIGHT, WPlaform.VERTICAL_CENTER);
        materialBlock.addChild(wmaterialeditor, BorderConstraint.CENTER);
        final WPlaform.Block techniqueBlock = new WPlaform.Block(new Chars("Techniques"), WPlaform.HORIZONTAL_RIGHT, WPlaform.VERTICAL_BOTTOM);
        techniqueBlock.addChild(wtechniqueeditor, BorderConstraint.CENTER);
        final WPlaform.Block transformBlock = new WPlaform.Block(new Chars("Node"), WPlaform.HORIZONTAL_LEFT, WPlaform.VERTICAL_BOTTOM);
        transformBlock.addChild(wsimilarityEditor, BorderConstraint.CENTER);


        final WPlaform platform = new WPlaform();
        platform.addBlock(viewBlock);
        platform.addBlock(sceneBlock);
        platform.addBlock(materialBlock);
        platform.addBlock(techniqueBlock);
        platform.addBlock(transformBlock);


        container.addChild(bar, BorderConstraint.TOP);
        container.addChild(platform, BorderConstraint.CENTER);


        wscenetree.varSelection().addListener(new EventListener() {
            @Override
            public void receiveEvent(Event event) {
                final Object value = ((PropertyMessage) event.getMessage()).getNewValue();
                if (value instanceof SceneNode) {
                    wsimilarityEditor.setSimilarity(((SceneNode)value).getNodeTransform());
                } else {
                    wsimilarityEditor.setSimilarity(null);
                }

                if (value instanceof Model) {
                    wmaterialeditor.setModel((Model) value);
                    wtechniqueeditor.setModel((Model) value);
                } else {
                    wmaterialeditor.setModel(null);
                    wtechniqueeditor.setModel(null);
                }
            }
        });

        container.addEventListener(Predicate.TRUE, new EventListener() {
            @Override
            public void receiveEvent(Event event) {

                if (event.getMessage() instanceof DragAndDropMessage) {
                    final DragAndDropMessage dd = (DragAndDropMessage) event.getMessage();
                    final int type = dd.getType();
                    System.out.println(dd);

                    final Attachment att = dd.getBag().getAttachment(new Chars("application/x-java-file-list; class=java.util.List"), null);
                    if (att == null) return;

                    if (DragAndDropMessage.TYPE_ENTER == type) {
                        dd.consume();
                        return;
                    } else if (DragAndDropMessage.TYPE_DROP == type) {

                        Object value = att.getObject();
                        if (value instanceof Sequence) {
                            final Sequence seq = (Sequence) value;
                            final Path p = (Path) seq.get(0);

                            try {
                                if (Model3Ds.canDecode(p)) {
                                    final Store store = Model3Ds.open(p);
                                    final SceneNode root = viewer.getScene();
                                    final Iterator ite = ((SceneResource) store).getElements().createIterator();
                                    while (ite.hasNext()) {
                                        final Object obj = ite.next();
                                        if (obj instanceof SceneNode) {
                                            SceneNode sn = (SceneNode) obj;
                                            GLEngineUtils.makeCompatible(sn);
                                            root.getChildren().add(sn);
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


//                            try {
//                                final Image image = Images.read(path);
//                                final WGraphicImage gra = new WGraphicImage(image);
//                                l.addChild(gra, BorderConstraint.CENTER);
//
//                            } catch (IOException ex) {
//                                ex.printStackTrace();
//                            }
                        }
                    }
                }

            }
        });


        frame.setSize(1600, 900);
        frame.setVisible(true);

    }

    private static WMenuButton createLightButton(Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                    WStyle style = RSReader.readStyle(Paths.resolve(SystemStyle.THEME_LIGHT));
                    SystemStyle.INSTANCE.getRules().removeAll();
                    SystemStyle.INSTANCE.getRules().addAll(style.getRules());
                    SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }

    private static WMenuButton createDarkButton(Chars name){
        final WMenuButton button = new WMenuButton(name,null,new EventListener(){
            @Override
            public void receiveEvent(Event event) {
                    WStyle styleLight = RSReader.readStyle(Paths.resolve(SystemStyle.THEME_LIGHT));
                    SystemStyle.INSTANCE.getRules().removeAll();
                    SystemStyle.INSTANCE.getRules().addAll(styleLight.getRules());

                    WStyle style = RSReader.readStyle(Paths.resolve(SystemStyle.THEME_DARK));
                    for(int i=0;i<style.getRules().getSize();i++){
                        final StyleDocument r = (StyleDocument) style.getRules().get(i);
                        WidgetStyles.mergeDoc(
                            SystemStyle.INSTANCE.getRule(r.getName()),
                            r, false);
                    }

                    SystemStyle.INSTANCE.notifyChanged();
            }
        });
        return button;
    }
}
