

package science.unlicense.p.editgraphic.action;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.scene.DefaultGraphicNode;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;
import science.unlicense.geometry.api.system.CoordinateSystems;

/**
 *
 * @author Johann Sorel
 */
public class AddNodeAction extends AbstractActionExecutable {

    private final SceneNode parent;

    public AddNodeAction(SceneNode parent) {
        CObjects.ensureNotNull(parent);
        this.parent = parent;
        setText(new Chars("node"));
    }

    @Override
    public Object perform() {
        final DefaultGraphicNode node = new DefaultGraphicNode(CoordinateSystems.UNDEFINED_3D);
        parent.getChildren().add(node);
        return null;
    }

}
