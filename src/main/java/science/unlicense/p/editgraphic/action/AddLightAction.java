

package science.unlicense.p.editgraphic.action;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.display.impl.light.AmbientLight;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;

/**
 *
 * @author Johann Sorel
 */
public class AddLightAction extends AbstractActionExecutable {

    private final SceneNode parent;

    public AddLightAction(SceneNode parent) {
        CObjects.ensureNotNull(parent);
        this.parent = parent;
        setText(new Chars("light"));
    }

    @Override
    public Object perform() {
        final AmbientLight light = new AmbientLight();
        parent.getChildren().add(light);
        return null;
    }

}
