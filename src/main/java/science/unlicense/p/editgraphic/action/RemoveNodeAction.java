

package science.unlicense.p.editgraphic.action;

import science.unlicense.common.api.CObjects;
import science.unlicense.common.api.character.Chars;
import science.unlicense.display.api.scene.SceneNode;
import science.unlicense.engine.ui.ievent.AbstractActionExecutable;

/**
 *
 * @author Johann Sorel
 */
public class RemoveNodeAction extends AbstractActionExecutable {

    private final SceneNode candidate;

    public RemoveNodeAction(SceneNode candidate) {
        CObjects.ensureNotNull(candidate);
        this.candidate = candidate;
        setText(new Chars("remove"));
    }

    @Override
    public Object perform() {
        if (candidate.getParent() != null) {
            candidate.getParent().getChildren().remove(candidate);
        }
        return null;
    }

}
