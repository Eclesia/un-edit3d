
package science.unlicense.p.thumbnailer;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.number.Float64;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.impl.io.ur.URLUtilities;
import science.unlicense.engine.ui.component.path.PathPresenters;
import science.unlicense.geometry.api.Extent;
import science.unlicense.image.api.Image;
import science.unlicense.image.api.Images;

/**
 *
 * @author Johann Sorel
 */
public class Thumbnailer {

    private static final Chars ARG_MODEL = new Chars("-model");
    private static final Chars ARG_THUMB_OUT = new Chars("-thumb-out");
    private static final Chars ARG_THUMB_SIZE = new Chars("-thumb-size");

    public static void main(String[] args) throws IOException {

//        final StringBuilder sb = new StringBuilder();
//        for(String s : args) {
//            sb.append(s);
//            sb.append(" ");
//        }
//        JOptionPane.showInputDialog(sb.toString());

//        args = new String[]{"-model", "/home/clubelec/Documents/stl/netflix.stl", "-thumb-out", "/home/clubelec/Documents/thumb.png"};

        //interpret commands
        final Dictionary dico = new HashDictionary();
        if (args.length > 1) {
            for (int i = 0; i < args.length; i += 2) {
                final String key = args[i];
                final String value = args[i+1];
                dico.add(new Chars(key), new Chars(value));
            }
        } else if (args.length == 1) {
            //handle it as a single model
            dico.add(ARG_MODEL,new Chars(args[0]));
        }

        Chars path = (Chars) dico.getValue(ARG_MODEL);
        if (path != null && path.startsWith(new Chars("file://"))) {
            //some desktop managers add the prefix as urls
            path = path.truncate(7, path.getCharLength());
            path = URLUtilities.decode(path);
            dico.add(ARG_MODEL, path);
        }

        final Chars thumbOut = (Chars) dico.getValue(ARG_THUMB_OUT);
        final Chars thumbSize = (Chars) dico.getValue(ARG_THUMB_SIZE);
        if (thumbOut != null) {
            //do not run application, generate a thumbnail
            final Chars modelPath = (Chars) dico.getValue(ARG_MODEL);
            final Path p = Paths.resolve(modelPath);
            final Path thumbPath = Paths.resolve(thumbOut);
            double size = 256;
            if (thumbSize != null) size = Float64.decode(new Chars(thumbSize));

            create(p, thumbPath, new Extent.Double(size, size));
        }

        System.exit(0);
    }

    public static void create(Path modelPath, Path thumbPath, Extent size) throws IOException {
        final Image thumb = PathPresenters.generateImage(modelPath, size);
        Images.write(thumb, new Chars("bmp"), thumbPath);
    }

}
