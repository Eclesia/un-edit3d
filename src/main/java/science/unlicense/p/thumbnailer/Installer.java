/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package science.unlicense.p.thumbnailer;

import science.unlicense.binding.ini.INIFormat;
import science.unlicense.binding.ini.INIStore;
import science.unlicense.binding.xml.FName;
import science.unlicense.binding.xml.XMLOutputStream;
import science.unlicense.binding.xml.dom.DomElement;
import science.unlicense.binding.xml.dom.DomWriter;
import science.unlicense.common.api.character.CharBuffer;
import science.unlicense.common.api.character.CharEncodings;
import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Collections;
import science.unlicense.common.api.collection.Sequence;
import science.unlicense.common.api.model.doc.DefaultDocument;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.CharOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.model3d.api.SceneResource;

/**
 *
 * @author clubelec
 */
public class Installer {



//0 =================
//EN : start by fixing correct paths to ULib-edi3d program in following files
//FR : commencez par corriger les chemins vers ULib-edit3d dans le fichiers suivants
//
//unlicense-edit3d-thumbnailer.desktop
//unlicense-edit3d.desktop
//unlicense-edit3d.sh
//
//1 =================
//EN : Add mime type in /etc/mime.types
//FR : Ajouter les mimes dans /etc/mime.types
//
//application/x-model3d-pmx
//application/x-model3d-pmd
//application/x-model3d-xps
//application/x-model3d-mesh
//application/x-model3d-mesh-ascii
//application/x-model3d-mqo
//application/x-model3d-3ds
//application/x-model3d-obj
//application/x-model3d-mdl
//application/x-model3d-unity
//
//2 =================
//EN : copy x-model3d-....xml files in ~/.local/share/mime/packages
//FR : copier les fichiers x-model3d-....xml dans ~/.local/share/mime/packages
//
//3 =================
//EN : If you want automatic execution on double-click, add this file in ~/.local/share/applications/
//FR : Pour l'execution automatic sur double-clik, ajouter le fichier dans ~/.local/share/applications/
//
//unlicense-edit3d.desktop
//
//4 =================
//EN : For thumbnail integration with nautilus/pcmanfm, add this file in ~/.local/share/thumbnailers/
//FR : Pour pcmanfm/nautilus, afin de generer les apercus, ajouter le fichier dans ~/.local/share/thumbnailers/
//
//unlicense-edit3d.thumbnailer
//
//5 =================
//EN : update mime types database :
//FR : mettre a jour la base de mimes :
//
//update-desktop-database ~/.local/share/applications
//update-mime-database ~/.local/share/mime

    private static final Chars NAME_THUMBNAILER_DESKTOP = new Chars("unlicense-3d.thumbnailer");
    private static final Chars NAME_DESKTOP = new Chars("unlicense-3d.desktop");
    private static final Chars NAME_THUMBNAILER_SH = new Chars("unlicense-3d-thumbnailer.sh");

    private static final Chars NS_MIME = new Chars("http://www.freedesktop.org/standards/shared-mime-info");
    private static final Chars TAG_MIME_INFO = new Chars("mime-info");
    private static final Chars TAG_MIME_TYPE = new Chars("mime-type");
    private static final Chars TAG_GLOB = new Chars("glob");
    private static final Chars TAG_COMMENT = new Chars("comment");

    public static void main(String[] args) throws IOException, java.io.IOException {
        install(Paths.resolve(new Chars("/home/clubelec/dev/un-edit3d/target/unlicense-editvisual-1.0.jar")));
    }

    private static void install(Path jarPath) throws IOException, java.io.IOException {

        final Chars homeStr = (Chars) science.unlicense.runtime.Runtime.system().getProperties().getSystemTree().search(new Chars("system/user/home")).getValue();

        final Path home = Paths.resolve(homeStr);
        final Path shareMimePackages = home.resolve(new Chars(".local/share/mime/packages"));
        shareMimePackages.createContainer();
        final Path shareThumbnailers = home.resolve(new Chars(".local/share/thumbnailers"));
        shareThumbnailers.createContainer();

        final Path desktopPath = shareThumbnailers.resolve(NAME_THUMBNAILER_DESKTOP);
        final Path shPath = shareThumbnailers.resolve(NAME_THUMBNAILER_SH);

        CharOutputStream cs = new CharOutputStream(shPath.createOutputStream(), CharEncodings.UTF_8);
        cs.write(new Chars("java -jar "));
        cs.write(Paths.systemPath(jarPath));
        cs.write(new Chars(" \"$@\" "));
        cs.close();


        final Format[] formats = Formats.getFormatsForResourceType(SceneResource.class);

        final CharBuffer allmimes = new CharBuffer();
        for (Format format : formats) {

            if(!format.getClass().getName().startsWith("science.unlicense.model3d")) continue;

            final Chars name = format.getIdentifier().toLowerCase();
            final Sequence mimeTypes = format.getMimeTypes();
            final Sequence extensions = format.getExtensions();

            final Chars mimeType;
            if (mimeTypes.isEmpty()) {
                mimeType = new Chars("application/x-model3d-"+name);
            } else {
                mimeType = ((Chars) mimeTypes.get(0)).toLowerCase();
            }
            System.out.println(mimeType);

            if (!allmimes.isEmpty()) allmimes.append(';');
            allmimes.append(mimeType);

            final DomElement domMimeInfo = new DomElement(new FName(NS_MIME, TAG_MIME_INFO));
            domMimeInfo.getProperties().add(new Chars("xmlns"), NS_MIME);
            domMimeInfo.getNamespaceContext().add(new Chars(""), NS_MIME);

            final DomElement domMimeType = new DomElement(new FName(NS_MIME, TAG_MIME_TYPE));
            domMimeType.getProperties().add(new Chars("type"), mimeType);
            domMimeInfo.getChildren().add(domMimeType);

            if (!extensions.isEmpty()) {
                final DomElement domGlob = new DomElement(new FName(NS_MIME, TAG_GLOB));
                domGlob.getProperties().add(new Chars("pattern"), new Chars("*."+extensions.get(0)));
                domMimeType.getChildren().add(domGlob);
            }

            final DomElement domComment = new DomElement(new FName(NS_MIME, TAG_COMMENT));
            domComment.setText(name);
            domMimeType.getChildren().add(domComment);

            final Path mimeTypePath = shareMimePackages.resolve(name.concat(new Chars(".xml")));

            final XMLOutputStream out = new XMLOutputStream();
            out.setIndent(new Chars("  "));
            out.setOutput(mimeTypePath);
            out.writeText(new Chars("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"));
            final DomWriter writer = new DomWriter();
            writer.setOutput(out);
            writer.write(domMimeInfo);
            writer.dispose();
            out.dispose();
        }

        allmimes.append(';');

        final Document doc = new DefaultDocument(true);
        final Document thumbnailerEntry = new DefaultDocument(true);
        doc.setPropertyValue(new Chars("Thumbnailer Entry"), thumbnailerEntry);
        thumbnailerEntry.setPropertyValue(new Chars("TryExec"), new Chars(Paths.systemPath(shPath)));
        thumbnailerEntry.setPropertyValue(new Chars("Exec"), new Chars(Paths.systemPath(shPath) + " -model %i -thumb-out %o -thumb-size %s"));
        thumbnailerEntry.setPropertyValue(new Chars("MimeType"), allmimes.toChars());

//        final Document desktopEntry = new DefaultDocument(true);
//        doc.setPropertyValue(new Chars("Desktop Entry"), desktopEntry);
//        desktopEntry.setPropertyValue(new Chars("Version"), new Chars("1.0"));
//        desktopEntry.setPropertyValue(new Chars("Encoding"), new Chars("UTF-8"));
//        desktopEntry.setPropertyValue(new Chars("Type"), new Chars("X-Thumbnailer"));
//        desktopEntry.setPropertyValue(new Chars("Name"), new Chars("Unlicense-lib thumbnailer"));
//        desktopEntry.setPropertyValue(new Chars("MimeType"), allmimes.toChars());
//        desktopEntry.setPropertyValue(new Chars("X-Thumbnailer-Exec"), new Chars(Paths.systemPath(shPath) + " -model %i -thumb-out %o -thumb-size %s"));


        final INIStore store = INIFormat.INSTANCE.open(desktopPath);
        store.insert(Collections.singletonCollection(doc));
        store.dispose();

        Runtime.getRuntime().exec("chmod u+x "+Paths.systemPath(shPath));
        Runtime.getRuntime().exec("update-desktop-database " + homeStr + "/.local/share/applications");
        Runtime.getRuntime().exec("update-mime-database " + homeStr + "/.local/share/mime");

    }

}
